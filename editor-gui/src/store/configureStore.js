import { combineReducers, createStore } from 'redux';
import DevTools from '../containers/DevTools';

const rootReducer = combineReducers({
  gui
});

const enhancer = DevTools.instrument({ maxAge: 50 });

export default () => createStore(rootReducer, enhancer);
