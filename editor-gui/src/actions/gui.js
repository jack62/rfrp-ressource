export const SET_GUI_VISIBLE = 'GUI/SET_GUI_VISIBLE';
export const SET_GUI_ACTIVE = 'GUI/SET_GUI_ACTIVE';

export function setGuiVisible(visible) {
  const type = SET_GUI_VISIBLE;
  const payload = { visible };
  return { type, payload };
}

export function setGuiActive(active) {
  const type = SET_GUI_ACTIVE;
  const payload = { active };
  return { type, payload };
}
