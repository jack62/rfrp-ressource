$(function(){
	function createKeyframes(widthNews, widthPubs){
		var style = document.createElement('style');
		style.type = 'text/css';
		var keyFrames = '@keyframes slideNews{\n' +
						'    from {\n' +
						'		margin-top: 0px;\n' +
						'	}\n' +
						'    8% {\n' +
						'		margin-top: 0px;\n' +
						'	}\n' +
						'    98% {\n' +
						'		margin-top: -' + parseInt(widthNews) + 'px;\n' +
						'	}\n' +
						'    to {\n' +
						'		margin-top: 0px;\n' +
						'	}\n' +
						'}\n' +
						'@keyframes slidePubs{\n' +
						'    from {\n' +
						'		margin-top: 0px;\n' +
						'	}\n' +
						'    8% {\n' +
						'		margin-top: 0px;\n' +
						'	}\n' +
						'    98% {\n' +
						'		margin-top: -' + parseInt(widthPubs) + 'px;\n' +
						'	}\n' +
						'    to {\n' +
						'		margin-top: 0px;\n' +
						'	}\n' +
						'}';
		style.innerHTML = keyFrames; //.replace(/A_DYNAMIC_VALUE/g, "180deg");
		document.getElementsByTagName('head')[0].appendChild(style);
		$('#disclaimer .left .newsblock .innerbox:first-child').first().css('animation', parseInt(widthNews/scrollSpeed) + 's linear slideNews infinite');
		$('#disclaimer .right .newsblock .innerbox:first-child').first().css('animation', parseInt(widthPubs/scrollSpeed) + 's linear slidePubs infinite');
	}

	function checkSize(){
		if (newsFinished && pubsFinished){
			newsFinished = pubsFinished = false;
			var totalNews = 0;
			var totalPubs = 0;
			$('.contenair.left .innerbox').each(function(key, el){
			    totalNews += $(el).height()
			});
			$('.contenair.right .innerbox').each(function(key, el){
			    totalPubs += $(el).height()
			});
			createKeyframes(totalNews - marginTop, totalPubs - marginTop);
		}
	}

	window.onload = function(e){
		window.addEventListener('message', function(event){

			var item = event.data;
			if (item !== undefined && item.type === "disclaimer") {

				if (item.display === true) {
					$('#disclaimer').delay(1000).fadeIn( "slow" );
				} else if (item.display === false) {
					$('#disclaimer').fadeOut( "slow" );
				}
			}
		});


		newsFinished = false;
		pubsFinished = false;
		marginTop = 50; //px
		scrollSpeed = 45; // 5 = very slow // 15 = slow // 25 = normal // 35 = fast // 45 = very fast
		 
		$.ajax({
			url: 'http://rfrp.erimen.com/get-news.php',
			type: 'POST',
			data: {
				type: 'get_news',
			},
			dataType: 'json',
			success: function (response) {
				if(typeof response.success !== "undefined"){
					if(response.success){
						$('.left .newsblock').html('');
						for(var i = 0; i < response.news.length; i++){
							n = response.news[i];
							$('.left .newsblock').append(
								$("<div class=\"innerbox\">" +
									"<h1>" + n.titre + "</h1>" +
									"<h2>" + n.texte + "</h2>" +
									(n.image ? ("<img src=\"" + n.image + "\" alt=\"" + n.titre + "\">") : '') +
								"</div>")
							);
						}
						newsFinished = true;
						setTimeout(function(){
							checkSize();
						}, 500);
					}else{
						for(var i = 0; i < response.message.length; i++){
							var msgType = response.message[i].indexOf('ERROR') == -1 ? 'success' : 'danger'
							$('.overlay').append($('<div class="alert alert-' + msgType + ' fade in alert-dismissible show"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + response.message[i] + '</div>'));
						}
					}
				}else{
					$('.overlay').append($('<div class="alert alert-danger fade in alert-dismissible show"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Une erreur inconnue est survenue</div>'));
				}
			},
		});


		$.ajax({
			url: 'http://rfrp.erimen.com/get-pubs.php',
			type: 'POST',
			data: {
				type: 'get_pubs',
			},
			dataType: 'json',
			success: function (response) {
				if(typeof response.success !== "undefined"){
					if(response.success){
						$('.right .newsblock').html('');
						for(var i = 0; i < response.pubs.length; i++){
							n = response.pubs[i];
							$('.right .newsblock').append(
								$("<div class=\"innerbox\">" +
									"<h1>" + n.titre + "</h1>" +
									"<h2>" + n.texte + "</h2>" +
									(n.image ? ("<img src=\"" + n.image + "\" alt=\"" + n.titre + "\">") : '') +
								"</div>")
							);
						}
						pubsFinished = true;
						setTimeout(function(){
							checkSize();
						}, 500);
					}else{
						for(var i = 0; i < response.message.length; i++){
							var msgType = response.message[i].indexOf('ERROR') == -1 ? 'success' : 'danger'
							$('.overlay').append($('<div class="alert alert-' + msgType + ' fade in alert-dismissible show"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + response.message[i] + '</div>'));
						}
					}
				}else{
					$('.overlay').append($('<div class="alert alert-danger fade in alert-dismissible show"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Une erreur inconnue est survenue</div>'));
				}
			},
		});
		
	};
});


