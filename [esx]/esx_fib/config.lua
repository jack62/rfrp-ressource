Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerSizeDel              = { x = 5.0, y = 5.0, z = 1.0 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }
Config.MarkerColorDel             = { r = 204, g = 50, b = 50 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = true
Config.MaxInService               = -1
Config.Locale                     = 'fr'

Config.FibStations = {

  FIB = {

    Blip = {
      Pos     = {x = -73.211791,y = -682.311889,z = 33.628337 },
      Sprite  = 498,
      Display = 4,
      Scale   = 1.0,
      Colour  = 22,
    },

    AuthorizedWeapons = {
      {name = 'WEAPON_STUNGUN',          price = 500},  
      {name = 'WEAPON_HEAVYPISTOL',      price = 300},
      {name = 'WEAPON_FLASHLIGHT',       price = 80},
      {name = 'GADGET_PARACHUTE',        price = 300},
    },

    AuthorizedVehicles = {
    {name = 'fbi2', label = 'Véhicule de pénétration'},
      {name = 'schafter5', label = 'Limousine blindée'},
      {name = 'asterope', label = 'Camionnette Sécuritas'},   
    },
  
  AuthorizedVehicles1 = {
    {name = 'supervolito2' , label = 'Hélico'}
  },

    Cloakrooms = {
      {x = 11.470846,y = -661.503051,z = 32.458837 },
    },

    Armories = {
      {x = 8.364199,y = -658.012573,z = 32.459676 },
    },

    Vehicles = {
      {
        Spawner    = {x = -6.081164,y = -664.666503,z = 31.348085 },
        SpawnPoint = {x = -5.287468,y = -670.876831,z = 31.348077 },
        Heading    = 184.9574127
      }
    },

    Vehicles1 = {
    {
    Spawner1    = {x = 17.278272,y = -677.074279,z = 249.423604 },
    SpawnPoint1 = {x = 7.441348,y = -685.301147,z = 249.423604 },
    Heading1    = 295.497123
    }
  },

    VehicleDeleters = {
      {x = 2.663828,y = -671.536613,z = 31.348077 },
    },
  
  VehicleDeleters1 = {
    {x = 7.441348,y = -685.301147,z = 249.423604 },
  },

    BossActions = {
      {x = 2.690846,y = -659.527832,z = 32.460920 },
    },

  },

}
