Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerColor                = { r = 102, g = 0, b = 102 }
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.ReviveReward               = 0  -- revive reward, set to 0 if you don't want it enabled
Config.AntiCombatLog              = true -- enable anti-combat logging?
Config.MaxInService               = 10
local second = 1000
local minute = 60 * second

-- How much time before auto respawn at hospital
Config.RespawnDelayAfterRPDeath   = 20 * minute

-- How much time before a menu opens to ask the player if he wants to respawn at hospital now
-- The player is not obliged to select YES, but he will be auto respawn
-- at the end of RespawnDelayAfterRPDeath just above.
Config.RespawnToHospitalMenuTimer   = true
Config.MenuRespawnToHospitalDelay   = 10 * minute

Config.EnablePlayerManagement       = true
Config.EnableSocietyOwnedVehicles   = false

Config.RemoveWeaponsAfterRPDeath    = false
Config.RemoveCashAfterRPDeath       = false
Config.RemoveItemsAfterRPDeath      = false

-- Will display a timer that shows RespawnDelayAfterRPDeath time remaining
Config.ShowDeathTimer               = true

-- Will allow to respawn at any time, don't use RespawnToHospitalMenuTimer at the same time !
Config.EarlyRespawn                 = false
-- The player can have a fine (on bank account)
Config.RespawnFine                  = true
Config.RespawnFineAmount            = 1000
Config.NPCJobEarnings               = {min = 300, max = 500}

Config.Locale                       = 'fr'

Config.Blip = {
  Pos     = { x = 343.22, y = -1398.51, z = 32.51 },
  Sprite  = 61,
  Display = 4,
  Scale   = 1.2,
  Colour  = 5,
}

Config.HelicopterSpawner = {
  SpawnPoint  = { x = 299.53, y = -1453.37, z = 45.51 },
  Heading     = 139.61
}

Config.Zones = {


  HospitalInteriorEntering1 = {
    Pos  = { x = 294.6, y = -1448.01, z = 28.9 },
    Size = {x = 1.5, y = 1.5, z = 1.0},
    Type = 1
  },

  HospitalInteriorInside1 = {
    Pos  = { x = -455.9780, y = -367.885, z = -187.460 },
    Size = {x = 1.5, y = 1.5, z = 1.0},
    Type = -1
  },

  HospitalInteriorOutside1 = {
    Pos  = { x = 295.8, y = -1446.5, z = 28.9 },
    Size = {x = 1.5, y = 1.5, z = 1.0},
    Type = -1
  },

  HospitalInteriorExit1 = {
    Pos  = { x = -457.628, y = -367.8009, z = -187.4608 },
    Size = {x = 1.5, y = 1.5, z = 1.0},
    Type = 1
  },






  AmbulanceActions = {
    Pos  = { x = 269.66, y = -1362.95, z = 23.54 },
    Size = {x = 1.5, y = 1.5, z = 1.0},
    Type  = 27
  },

  VehicleSpawner = {
    Pos  = { x = 395.31, y = -1435.29, z = 28.46 },
    Size = {x = 1.5, y = 1.5, z = 1.0},
    Type  = 27
  },

  VehicleSpawnPoint = {
    Pos  = { x = 405.67, y = -1430.03, z = 28.44 },
    Size = {x = 1.5, y = 1.5, z = 1.0},
    Type  = -1
  },

  VehicleDeleter = {
    Pos  = { x = 376.65, y = -1444.41, z = 28.43 },
    Size = { x = 3.0, y = 3.0, z = 0.4 },
    Type  = 27
  },

  Pharmacy = {
    Pos  = { x = 238.2, y = -1359.43, z = 38.53 },
    Size = { x = 1.5, y = 1.5, z = 0.4 },
    Type = 1
  },

}