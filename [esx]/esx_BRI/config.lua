Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerSizeDel              = { x = 5.0, y = 5.0, z = 1.0 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }
Config.MarkerColorDel             = { r = 204, g = 50, b = 50 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = true -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = true
Config.MaxInService               = -1
Config.Locale                     = 'fr'

Config.BriStations = {

  BRI = {

    Blip = {
      Pos     = {x = -441.385,y = 6004.772,z = 40.528 },
      Sprite  = 310,
      Display = 4,
      Scale   = 1.0,
      Colour  = 10,
    },

    AuthorizedWeapons = {
      {name = 'WEAPON_STUNGUN',          price = 500 },
      {name = 'WEAPON_HEAVYPISTOL',      price = 300 },
      {name = 'WEAPON_ADVANCEDRIFLE',    price = 1050 },
      {name = 'WEAPON_BULLPUPSHOTGUN',   price = 150 },
      {name = 'WEAPON_HEAVYSNIPER',      price = 600 },
      {name = 'WEAPON_FLASHLIGHT',       price = 80 },
      {name = 'WEAPON_ASSAULTSMG', 		 price = 120 },
	  {name = 'WEAPON_BZGAS',        	 price = 500 },
      {name = 'WEAPON_FLAREGUN',         price = 60 },
      {name = 'GADGET_PARACHUTE',        price = 300 },
    },

    AuthorizedVehicles = {
	  {name = 'fbi', label = 'Dodge'},
	  {name = 'fbi2', label = 'SUV'},
	  {name = 'police4', label = 'Ford F150'},
	  {name = 'pranger', label = 'BRI SUV'},
    },

	AuthorizedVehicles1 = {
	  {name = 'frogger2' , label = 'Hélico'},
	},

    Cloakrooms = {
      {x = 1172.765,y = -3194.578,z = -40.008 },
    },

    Armories = {
      {x = -450.713,y = 6011.189,z = 30.716 },
    },

    Vehicles = {
      {
        Spawner    = {x = -452.06,y = 6005.693,z = 30.841 },
        SpawnPoint = {x = -462.33,y = 6009.607,z = 30.945 },
        Heading    = 88.212
      }
    },

    Vehicles1 = {
	  {
		Spawner1    = {x = -447.449,y = 6001.421,z = 30.686 },
		SpawnPoint1 = {x = -475.111,y = 5988.534,z = 31.337 },
		Heading1   = 90.0
	  }
	},

    VehicleDeleters = {
      {x = -451.435,y = 5998.582,z = 29.946 },
    },

	VehicleDeleters1 = {
	  {x = -474.966,y = 5989.255,z = 30.337 },
	},

    BossActions = {
      {x = 2069.39,y = 2996.39,z = -63.5 },
    },

  },

}
