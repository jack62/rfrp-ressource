ESX = nil

local arrayWeight = Config.localWeight

TriggerEvent('esx:getSharedObject', function(obj)
  ESX = obj
end)

AddEventHandler('onMySQLReady', function ()
  if Config.WeightSqlBased == true then
    MySQL.Async.fetchAll(
      'SELECT * FROM item_weight',
      {},
      function(result)
        for i=1, #result, 1 do
          arrayWeight[result[i].item] = result[i].weight
        end

      end
    )
  end
end)

-- Return the sum of all item in pPlayer inventory
function getInventoryWeight(pPlayer)
  local weight = 0
  local weapon_weight = 0
  local ammo_weight =0
  local itemWeight = 0
  if #pPlayer.inventory > 0 then
	  for i=1, #pPlayer.inventory, 1 do
	    if pPlayer.inventory[i] ~= nil then
	      itemWeight = Config.DefaultWeight
	      if arrayWeight[pPlayer.inventory[i].name] ~= nil then
	        itemWeight = arrayWeight[pPlayer.inventory[i].name]
	      end
	      weight = weight + (itemWeight * pPlayer.inventory[i].count)
	    end
	  end
  end
  if #pPlayer.loadout > 0 then
    for i=1,#pPlayer.loadout,1 do
      if pPlayer.loadout[i] ~= nil then
         itemWeight = Config.DefaultWeight
         if arrayWeight[pPlayer.loadout[i].name] ~= nil then
 	        itemWeight = arrayWeight[pPlayer.loadout[i].name]
 	      end
        if pPlayer.loadout[i].ammo > 0 then
          ammo_weight = pPlayer.loadout[i].ammo * Config.DefaultAmmoWeight
        end
        weight = weight + itemWeight + ammo_weight
      end
    end
  end
  return weight
end

-- Get user speed
-- https://runtime.fivem.net/doc/reference.html#_0x6DB47AA77FD94E09

RegisterServerEvent('esx_advanced_inventory:FUpdate')
AddEventHandler('esx_advanced_inventory:FUpdate', function(xPlayer)
  local source_ = source
  local weight = getInventoryWeight(xPlayer)
  TriggerClientEvent('esx_advanced_inventory:change',source_,weight)
end)


RegisterServerEvent('esx_advanced_inventory:Update')
AddEventHandler('esx_advanced_inventory:Update', function(source)
  local source_ = source
  local xPlayer = ESX.GetPlayerFromId(source_)
  local weight = getInventoryWeight(xPlayer)
  TriggerClientEvent('esx_advanced_inventory:change',source_,weight)
end)

RegisterServerEvent('esx:onAddInventoryItem')
AddEventHandler('esx:onAddInventoryItem', function(source, items, count)
  local source_ = source
  local xPlayer = ESX.GetPlayerFromId(source_)
  local currentInventoryWeight = getInventoryWeight(xPlayer)
  local item = {}
  if items.name == nil then
    item.name = items
  else
    item = items
  end
  TriggerEvent('esx_advanced_inventory:Update',source_)
end)

RegisterServerEvent('esx:onRemoveInventoryItem')
AddEventHandler('esx:onRemoveInventoryItem', function(source, item, count)
  if item.name ~= nil and item.name == 'sac'and item.count == 0 then
    TriggerClientEvent("esx_advanced_inventory:NoMoreSac",source)
  end
  if item.name ~= nil and item.name == 'sac2'and item.count == 0 then
    TriggerClientEvent("esx_advanced_inventory:NoMoreSac2",source)
  end
  if item.name ~= nil and item.name == 'sac3'and item.count == 0 then
    TriggerClientEvent("esx_advanced_inventory:NoMoreSac3",source)
  end
  TriggerEvent('esx_advanced_inventory:Update',source)
end)

RegisterServerEvent('esx:onRemoveWeapon')
AddEventHandler('esx:onRemoveWeapon', function(source)

  TriggerEvent('esx_advanced_inventory:Update',source)
end)

ESX.RegisterUsableItem('sac', function(source)
    TriggerClientEvent('esx_advanced_inventory:Sac', source)
end)
ESX.RegisterUsableItem('sac2', function(source)
    TriggerClientEvent('esx_advanced_inventory:Sac2', source)
end)
ESX.RegisterUsableItem('sac3', function(source)
    TriggerClientEvent('esx_advanced_inventory:Sac3', source)
end)




function dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end
