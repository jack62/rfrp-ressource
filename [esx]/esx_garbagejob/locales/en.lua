Locales['en'] = {
	['cloakroom']				= 'vestiaire',
	['job_wear']				= 'vêtements de travail',
	['citizen_wear'] 			= 'vêtements civils',
	['vehiclespawner'] 			= 'choix du camion',
	['already_have_van'] 		= 'nous avons déjà fourni un camion!',
	['delivery'] 				= 'appuyez sur ~ INPUT_PICKUP ~ pour ramasser.',
	['not_your_van'] 			= 'Ce nest pas le camion que vous avez fourni!',
	['not_your_van2'] 			= 'vous devez être dans le camion qui vous a été fourni!',
	['need_it'] 				= 'Oui, mais nous en avons besoin!',
	['ok_work'] 				= 'ok, au travail alors!',
	['scared_me'] 				= 'ok, tu mas fait peur là-bas!',
	['resume_delivery'] 		= 'ok, reprends ta livraison alors!',
	['not_delivery'] 			= 'pas de ramassage, pas dargent liquide',
	['pay_repair'] 				= 'Dun autre côté, vous payez les réparations!',
	['repair_minus'] 			= 'réparations de camions: -',
	['shipments_plus'] 			= 'expéditions: +',
	['van_state'] 				= 'pas dargent mec, vu létat du camion!',
	['no_delivery_no_van'] 		= 'pas de livraison, pas de camion! Cest une blague?',
	['van_price'] 				= 'prix du camion: -',
	['meet_ls'] 				= 'Aller à la prochaine position de ramassage',
	['meet_bc'] 				= 'Aller à la prochaine position de ramassage',
	['meet_del'] 				= 'Aller à la prochaine position de ramassage',
	['return_depot'] 			= 'retourner au dépôt.',
	['blip_job'] 				= 'walker Logistics : Compagnie des ordures',
	['blip_delivery'] 			= 'walker Logistics: Livraison', 
	['blip_goal'] 				= 'walker Logistics: point de livraison', 
	['blip_depot'] 				= 'walker Logistics: dépôt', 
	['cancel_mission'] 			= 'appuyez sur ~ INPUT_PICKUP ~ pour annuler la mission',
}