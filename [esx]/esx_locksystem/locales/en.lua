Locales['en'] = {
    ['vehicle_locked'] = 'Vehicule verouillé',
    ['vehicle_unlocked']  = 'Vehicule déverouillé',
    ['keys_not_inside'] = 'Les clefs n\'étaient pas dedans',
    ['lock_cooldown'] = 'Tu dois attendre %s secondes',
    ['vehicle_not_owned'] = 'Ce n\'est pas ton vehicule',
    ['missing_argument_first'] = 'il manque un argument : /givekey <id> <plate>',
    ['missing_argument_second'] = 'Second missing argument : /givekey <id> <plate>',
    ['player_not_found'] = 'Joueur introuvable',
    ['cannot_target_yourself'] = 'You can\'t target yourself',
    ['vehicle_not_exist'] = 'The vehicle with this plate doesn\'t exist',
    ['target_has_keys_sender'] = 'The target already has the keys of the vehicle',
    ['target_has_keys_receiver'] = '%s tried to give you his keys, but you already had them',
    ['you_gave_keys'] = "You gave the keys of vehicle %s to %s",
    ['you_received_keys'] = "You have been given the keys of vehicle %s by %s",
    ['could_not_find_plate'] = "Could not find plate!"
}
