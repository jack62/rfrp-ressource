Locales['fr'] = {
  -- Cloakroom
  ['cloakroom']                = 'Vestiaire',
  ['citizen_wear']             = 'Tenue de Civile',
  ['recrue_outfit']            = 'Tenue de Service',
  ['barman_outfit']            = 'Tenue de Barman',
  ['open_cloackroom']          = '~INPUT_CONTEXT~ Pour accéder au ~b~Vestiaire',

  -- Vault  
  ['get_weapon']               = 'Prendre Arme',
  ['put_weapon']               = 'Déposer Arme',
  ['get_weapon_menu']          = 'Coffre - Prendre Arme',
  ['put_weapon_menu']          = 'Coffre - Déposer Arme',
  ['get_object']               = 'Prendre Objet',
  ['put_object']               = 'Déposer Objet',
  ['vault']                    = 'Coffre',
  ['open_vault']               = '~INPUT_CONTEXT~ Pour accéder au ~b~Coffre',

  -- Fridge  
  ['get_object']               = 'Prendre Objet',
  ['put_object']               = 'Déposer Objet',
  ['fridge']                   = 'Frigo',
  ['open_fridge']              = '~INPUT_CONTEXT~ Pour accéder au ~b~Frigo',
  ['mamas_fridge_stock']       = 'Frigo du Bahama Mamas',
  ['fridge_inventory']         = 'Contenu du Frigo',

  -- Shops  
  ['shop']                     = 'Boutique du Bahama Mamas',
  ['shop_menu']                = '~INPUT_CONTEXT~ Pour accéder à la ~b~Boutique',
  ['bought']                   = 'Vous avez acheté ~g~1x ',
  ['not_enough_money']         = '~r~Vous n\'avez pas assez d\'Argent',
  ['max_item']                 = '~r~Vous n\'avez plus de place',

  -- Vehicles  
  ['vehicle_menu']             = 'Garage',
  ['vehicle_out']              = '~r~Il y a déja un Véhicule dehors',
  ['vehicle_spawner']          = '~INPUT_CONTEXT~ Pour sortir un ~g~Véhicule',
  ['store_vehicle']            = '~INPUT_CONTEXT~ Pour ranger le ~g~Véhicule',
  ['service_max']              = 'Service Complet : ',
  ['spawn_point_busy']         = '~r~La zone est encombrée',
  ['helico_menu']              = 'Hélicoptère',
  ['helico_out']              = '~r~Il y a déja un Hélicoptère dehors',
  ['helico_spawner']          = '~INPUT_CONTEXT~ Pour sortir un ~g~Hélicoptère',
  ['store_helico']            = '~INPUT_CONTEXT~ Pour ranger le ~g~Hélicoptère',
  ['vehicle2_menu']             = 'Garage PDG',
  ['vehicle2_out']              = '~r~Il y a déja un Véhicule dehors',
  ['vehicle2_spawner']          = '~INPUT_CONTEXT~ Pour sortir un ~g~Véhicule',
  ['store_vehicle2']            = '~INPUT_CONTEXT~ Pour ranger le ~g~Véhicule',

  -- Boss Menu  
  ['take_company_money']       = 'Retirer Argent Société',
  ['deposit_money']            = 'Déposer Argent Société',
  ['amount_of_withdrawal']     = 'Montant du Retrait',
  ['invalid_amount']           = '~r~Montant Invalide',
  ['amount_of_deposit']        = 'Montant du Dépôt',
  ['open_bossmenu']            = '~INPUT_CONTEXT~ Pour ouvrir le ~b~Menu',
  ['invalid_quantity']         = '~r~Quantité Invalide',
  ['you_removed']              = 'Vous avez retiré ~g~x',
  ['you_added']                = 'Vous avez ajouté ~g~x',
  ['quantity']                 = 'Quantité',
  ['inventory']                = 'Inventaire',
  ['mamas_stock']              = 'Stock du Bahama Mamas',

  -- Billing Menu  
  ['billing']                  = 'Facture',
  ['no_players_nearby']        = '~r~Personne à proximité',
  ['billing_amount']           = 'Montant de la Facture',
  ['amount_invalid']           = '~r~Montant Invalide',

  -- Crafting Menu  
  ['crafting']                 = 'Boissons',
  ['mojito']                   = 'Mojito',
  ['rhum']                     = 'Rhum',
  ['rhumfruit']                = 'Rhum-jus de fruits',
  ['menthe']                   = 'Menthe',
  ['vin']                      = 'Vin',
  ['alcool']                   = 'Alcool',
  ['citron']                   = 'Citron',
  ['sucre']                    = 'Sucre',
  ['jusraisin']                = 'Jus de Raisin',
  ['rhumcoca']                = 'Rhum Coca',
  ['jagerbomb']                = 'Jager Bomb',
  ['vodkaenergy ']             = 'Vodka Redbull',
  ['whiskycoca']               = 'Whisky coca ',
  ['jusfruit']                 = 'Jus de Fruits',
  ['vodkaenergy']              = 'Vodka Energy',
  ['redbull']                  = 'Redbull',
  ['water']                    = 'Eau',
  ['assembling_cocktail']      = 'Mélange des différents ingrédients en cours !',
  ['craft_miss']               = 'Malheureux échec du mélange...',
  ['not_enough']               = 'Pas assez de ~r~ ',
  ['craft']                    = 'Mélange terminé de ~g~',

  -- Récolte / Traitement / Revente
  ['inv_full'] = '~r~Votre sac à dos est plein',
  ['pickup_in_prog'] = '~b~Ramassage en cours..',
  ['sale_in_prog'] = '~b~Vente en cours..',
  ['press_collect_raisin'] = '~INPUT_CONTEXT~ Pour récolter du ~g~Raisin',
  ['press_sell_raisin'] = '~INPUT_CONTEXT~ Pour vendre du ~g~Raisin',
  ['no_raisin_sale'] = '~r~Vous n\'avez plus de Raisin',
  ['not_enough_raisin'] = '~r~Vous n\'avez pas assez de Raisin',
  ['sold_one_raisin'] = 'Vous avez vendu ~g~x1 Raisin~s~',
  ['press_collect_citron'] = '~INPUT_CONTEXT~ Pour récolter du ~g~Citron',
  ['press_sell_citron'] = '~INPUT_CONTEXT~ Pour vendre du ~g~Citron',
  ['no_citron_sale'] = '~r~Vous n\'avez plus de Citron',
  ['not_enough_citron'] = '~r~Vous n\'avez pas assez de Citron',
  ['sold_one_citron'] = 'Vous avez vendu ~g~x1 Citron~s~',
  ['press_collect_fruit'] = '~INPUT_CONTEXT~ Pour récolter du ~g~Fruit',
  ['press_sell_fruit'] = '~INPUT_CONTEXT~ Pour vendre du ~g~Fruit',
  ['no_fruit_sale'] = '~r~Vous n\'avez plus de Fruit',
  ['not_enough_fruit'] = '~r~Vous n\'avez pas assez de Fruit',
  ['sold_one_fruit'] = 'Vous avez vendu ~g~x1 Fruit~s~',

  -- Misc  
  ['map_blip']                 = 'Bahama Mamas',
  ['mamas']                  = 'Bahama Mamas',

  -- Phone  
  ['mamas_phone']            = 'Bahama.M',
  ['mamas_customer']         = 'Citoyen',

  -- GPS
  ['gps']                    = 'GPS',

  -- Teleport Notifications  
  ['e_to_enter']               = '~INPUT_CONTEXT~ Pour ~g~Entrer',
  ['e_to_ring']                = '~INPUT_CONTEXT~ Pour ~g~Sonner',
  
}
