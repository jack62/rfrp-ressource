Config                            = {}
Config.DrawDistance               = 50.0

Config.EnablePlayerManagement     = true
Config.EnableSocietyOwnedVehicles = false
Config.EnableVaultManagement      = true
Config.EnableHelicopters          = true
Config.MaxInService               = -1
Config.PriceResell                = {vin = 7, citron = 8, fruit = 7}
Config.Locale                     = 'fr'

Config.MissCraft                  = 10 -- %


Config.AuthorizedVehicles = {
    { name = 'mule',  label = 'Service' },
}


Config.AuthorizedVehicles2 = {
    { name = 'xls2',  label = 'Sécurité' },
    { name = 'schafter3',  label = 'Véhicule de la patronne' },
}

Config.AuthorizedHelico = {
    { name = 'volatus',  label = 'Commerciale' },
}

Config.Blips = {
    
    Blip = {
      Pos     = { x = -1388.652, y = -586.179, z = 30.218 },
      Sprite  = 136,
      Display = 4,
      Scale   = 1.2,
      Colour  = 20,
    },
    
}

Config.Zones = {

    Cloakrooms = {
        Pos   = { x = -1403.793, y = -606.903, z = 30.319 },
        Size  = { x = 1.0001, y = 1.0001, z = 1.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 27,
    },

    Vaults = {
        Pos   = { x = -1379.043, y = -477.821, z = 72.042 },
        Size  = { x = 1.0001, y = 1.0001, z = 1.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 27,
    },

    Fridge = {
        Pos   = { x = -1386.614, y = -608.631, z = 30.319 },
        Size  = { x = 1.0001, y = 1.0001, z = 1.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 27,
    }, 

    RaisinFarm = {
        Pos   = { x = -1716.128, y = 1992.310, z = 120.909 },
        Size  = { x = 2.00, y = 2.00, z = 1.00 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 0,
    },
    
    RaisinResell = {
        Pos   = { x = 878.952, y = -1663.665, z = 31.434 },
        Size  = { x = 2.00, y = 2.00, z = 1.00 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 0,
    },

    CitronFarm = {
        Pos   = { x = -1944.976, y = 1795.310, z = 171.927 },
        Size  = { x = 2.00, y = 2.00, z = 1.00 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 0,
    },
    
    CitronResell = {
        Pos   = { x = -1147.774, y = -1599.557, z = 4.389 },
        Size  = { x = 2.00, y = 2.00, z = 1.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 0,
    },

    FruitFarm = {
        Pos   = { x = 353.922, y = 6518.403, z = 28.334 },
        Size  = { x = 2.00, y = 2.00, z = 1.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 0,
    },
    
    FruitResell = {
        Pos   = { x = 1408.634, y = 3619.576, z = 34.894 },
        Size  = { x = 2.00, y = 2.00, z = 1.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 0,
    },

    Vehicles = {
        Pos = { x = -1404.248, y = -630.268, z = 28.673 },
        SpawnPoint = { x = -1402.187, y = -641.009, z = 28.673 },
        Size  = { x = 1.0001, y = 1.0001, z = 1.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 27,
        Heading  = 125.02,
    },

    Vehicles2 = {
        Pos = { x = -1399.033, y = -657.551, z = 28.673 },
        SpawnPoint = { x = -1402.187, y = -641.009, z = 28.673 },
        Size  = { x = 1.0001, y = 1.0001, z = 1.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 0,
        Heading  = 125.02,
    },

    VehicleDeleters = {
        Pos   = { x = -1414.391, y = -639.873, z = 28.673 },
        Size  = { x = 1.0001, y = 1.0001, z = 1.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160 },
        Type  = 27,
    },

    Helicopters = {
        Pos          = { x = -1379.661, y = -603.941, z = 43.243 },
        SpawnPoint   = { x = -1387.685, y = -607.804, z = 43.243 },
        Size  = { x = 1.0001, y = 1.0001, z = 1.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 0,
        Heading = 128.84,
    },

    HelicopterDeleters = {
        Pos   = { x = -1387.685, y = -607.804, z = 43.243 },
        Size  = { x = 1.0001, y = 1.0001, z = 1.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160 },
        Type  = 27,
    },

    BossActions = {
        Pos   = { x = -1392.44, y = -601.1, z = 30.32 },
        Size  = { x = 1.0001, y = 1.0001, z = 1.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 27,
    },

-----------------------
-------- SHOPS --------

    Aliment = {
        Pos   = { x = -2511.394, y = 3615.555, z = 13.653 },
        Size  = { x = 1.0001, y = 1.0001, z = 1.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160 },
        Type  = 0,
        Items = {
        	{ name = 'menthe',     label = 'Feuille de Menthe',	price = 2 },
            { name = 'sucre',      label = 'Sucre',  			price = 1 },
            { name = 'bolcacahuetes', label = 'Cacahuètes',  price = 2 },
            { name = 'bolnoixcajou', label = 'Noix de Cajou',  price = 2 },
            { name = 'bolpistache', label = 'Pistaches',  price = 2 }
        }
    },

    Alcool = {
        Pos   = { x = 1144.034, y = -299.420, z = 68.806 },
        Size  = { x = 1.0001, y = 1.0001, z = 1.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160 },
        Type  = 0,
        Items = {
            { name = 'rhum',    label = 'Rhum',  price = 2 },
            { name = 'whisky',  label = 'Whisky',  price = 2 },
            { name = 'vodka',   label = 'Vodka',  price = 2 },
			{ name = 'jager',   label = 'Jager',  price = 2 },
			{ name = 'martini',   label = 'Martini',  price = 2 },
			{ name = 'alcool',   label = 'Alcool',  price = 2 },
            { name = 'tequila', label = 'Tequila',  price = 2 }
        }
    },

}


-----------------------
----- TELEPORTERS -----

Config.TeleportZones = {
    
  --[[EnterMamas = {
    Pos       = { x = -1388.6527, y = -586.1796, z = 30.2184 },
    Size      = { x = 1.0001, y = 1.0001, z = 0.801 },
    Color     = { r = 0, g = 155, b = 255, a = 165 },
    Marker    = 27,
    Blip      = false,
    Name      = "Bahama Mamas : Entrée",
    Type      = "teleport",
    Hint      = "~INPUT_PICKUP~ Pour entrer dans ~b~Bahama Mamas",
    Teleport  = { x = -1396.298, y = -593.719, z = 30.3198 }
  },

  ExitMamas = {
    Pos       = { x = -1396.298, y = -593.719, z = 30.3198 },
    Size      = { x = 1.0001, y = 1.0001, z = 0.801 },
    Color     = { r = 0, g = 155, b = 255, a = 165 },
    Marker    = 27,
    Blip      = false,
    Name      = "Bahama Mamas : Sortie",
    Type      = "teleport",
    Hint      = "~INPUT_PICKUP~ Pour sortir du ~b~Bahama Mamas",
    Teleport  = { x = -1388.6527, y = -586.1796, z = 30.2184 },
  },]]-- 

  EnterBuilding = {
    Pos       = { x = -1381.670, y = -632.923, z = 30.819 },
    Size      = { x = 1.0001, y = 1.0001, z = 0.801 },
    Color     = { r = 0, g = 155, b = 255, a = 165 },
    Marker    = 27,
    Blip      = false,
    Name      = "Bar 2 : Entrée",
    Type      = "teleport",
    Hint      = "~INPUT_PICKUP~ Pour passer derrière le ~b~Bar",
    Teleport  = { x = -1378.976, y = -630.603, z = 30.819 } 
  },

  ExitBuilding = {
    Pos       = { x = -1378.976, y = -630.603, z = 30.819  },
    Size      = { x = 1.0001, y = 1.0001, z = 0.801 },
    Color     = { r = 0, g = 155, b = 255, a = 165 },
    Marker    = 27,
    Blip      = false,
    Name      = "Bar 2 : Sortie",
    Type      = "teleport",
    Hint      = "~INPUT_PICKUP~ Pour passer devant le ~b~Bar",
    Teleport  = { x = -1381.670, y = -632.923, z = 30.819 },
  },

  EnterBar = {
    Pos       = { x = -1390.801, y = -597.908, z = 30.319 },
    Size      = { x = 1.0001, y = 1.0001, z = 0.801 },
    Color     = { r = 0, g = 155, b = 255, a = 165 },
    Marker    = 27,
    Blip      = false,
    Name      = "Bar : Entrée",
    Type      = "teleport",
    Hint      = "~INPUT_PICKUP~ Pour passer derrière le ~b~Bar",
    Teleport  = { x = -1385.263, y = -606.508, z = 30.319 }
  },

  ExitBar = {
    Pos       = { x = -1390.263, y = -600.508, z = 30.319 },
    Size      = { x = 1.0001, y = 1.0001, z = 0.801 },
    Color     = { r = 0, g = 155, b = 255, a = 165 },
    Marker    = 27,
    Blip      = false,
    Name      = "Bar : Sortie",
    Type      = "teleport",
    Hint      = "~INPUT_PICKUP~ Pour passer devant le ~b~Bar",
    Teleport  = { x = -1390.801, y = -597.908, z = 30.319 },
  },


  EnterService = {
    Pos       = { x = -1383.213, y = -640.145, z = 28.673 },
    Size      = { x = 1.0001, y = 1.0001, z = 0.801 },
    Color     = { r = 0, g = 155, b = 255, a = 165 },
    Marker    = 27,
    Blip      = false,
    Name      = "Employés : Entrée",
    Type      = "teleport",
    Hint      = "~INPUT_PICKUP~ Pour rentrer dans le ~b~Bar",
    Teleport  = { x = -1371.559, y = -626.167, z = 30.819 } 
  },

  ExitService = {
    Pos       = { x = -1371.559, y = -626.167, z = 30.819 },
    Size      = { x = 1.0001, y = 1.0001, z = 0.801 },
    Color     = { r = 0, g = 155, b = 255, a = 165 },
    Marker    = 27,
    Blip      = false,
    Name      = "Employés : Sortie",
    Type      = "teleport",
    Hint      = "~INPUT_PICKUP~ Pour sortir du ~b~Bar",
    Teleport  = { x = -1383.213, y = -640.145, z = 28.673 },
  },

  --[[EnterBureaux = {
    Pos       = { x = -1389.321, y = -591.960, z = 30.319 },
    Size      = { x = 1.0001, y = 1.0001, z = 0.801 },
    Color     = { r = 0, g = 155, b = 255, a = 165 },
    Marker    = 27,
    Blip      = false,
    Name      = "Bureaux : Entrée",
    Type      = "teleport",
    Hint      = "~INPUT_PICKUP~ Pour entrer dans les ~b~Bureaux",
    Teleport  = {  x = -1392.74, y = -480.18, z = 71.14 }
  },

  ExitBureaux = {
    Pos       = { x = -1392.74, y = -480.18, z = 71.14 },
    Size      = { x = 1.0001, y = 1.0001, z = 0.801 },
    Color     = { r = 0, g = 155, b = 255, a = 165 },
    Marker    = 27,
    Blip      = false,
    Name      = "Bureaux : Sortie",
    Type      = "teleport",
    Hint      = "~INPUT_PICKUP~ Pour sortir des ~b~Bureaux",
    Teleport  = { x = -1389.321, y = -591.960, z = 30.319 },
  },]]--

  EnterHeliport = {
    Pos       = { x = -1392.812, y = -640.413, z = 28.673 },
    Size      = { x = 1.0001, y = 1.0001, z = 0.801 },
    Color     = { r = 0, g = 155, b = 255, a = 165 },
    Marker    = 27,
    Blip      = false,
    Name      = "Héliport : Entrée",
    Type      = "teleport",
    Hint      = "~INPUT_PICKUP~ Pour monter sur le ~b~Toit",
    Teleport  = { x = -1381.643, y = -600.125, z = 43.243 }
  },

  ExitHeliport = {
    Pos       = { x = -1381.643, y = -600.125, z = 43.243 },
    Size      = { x = 1.0001, y = 1.0001, z = 0.801 },
    Color     = { r = 0, g = 155, b = 255, a = 165 },
    Marker    = 27,
    Blip      = false,
    Name      = "Héliport : Sortie",
    Type      = "teleport",
    Hint      = "~INPUT_PICKUP~ Pour descendre du ~b~Toit",
    Teleport  = { x = -1392.812, y = -640.413, z = 28.673 },
  },
}
