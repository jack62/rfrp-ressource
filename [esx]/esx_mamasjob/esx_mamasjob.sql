SET @job_name = 'mamas';
SET @society_name = 'society_mamas';
SET @job_Name_Caps = 'Bahama Mamas';
SET @salary_1 = 250;
SET @salary_2 = 500;
SET @salary_3 = 750;
SET @salary_4 = 900;
SET @salary_5 = 1500;


INSERT INTO `addon_account` (name, label, shared) VALUES
  (@society_name, @job_Name_Caps, 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
  (@society_name, @job_Name_Caps, 1),
  ('society_mamas_fridge', 'Mamas (Frigo)', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES 
    (@society_name, @job_Name_Caps, 1)
;

INSERT INTO `jobs` (name, label, whitelisted) VALUES
  (@job_name, @job_Name_Caps, 1)
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
  (@job_name, 0, 'recrue', 'Recrue', @salary_1, '{}', '{}'),
  (@job_name, 1, 'salarie', 'Salarié', @salary_2, '{}', '{}'),
  (@job_name, 2, 'security', 'Vigile', @salary_3, '{}', '{}'),
  (@job_name, 3, 'chef', 'Chef-d\'Equipe', @salary_4, '{}', '{}'),
  (@job_name, 4, 'boss', 'Patron', @salary_5, '{}', '{}')
;

INSERT INTO `items` (`name`, `label`) VALUES  
    ('vin', 'Vin'),
    ('raisin', 'Raisin'),
    ('citron', 'Citron'),
    ('sucre', 'Sucre'),
    ('alcool', 'Alcool'),
    ('tequila', 'Tequila'),
    ('jusraisin', 'Jus de Raisin')
;
