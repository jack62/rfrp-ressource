local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData              = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local Blips                   = {}

local raisinQTE				  = 0
local citronQTE               = 0
local fruitQTE                = 0

local isService 			        = false
local isBarman                = false
local isInMarker              = false
local isInPublicMarker        = false
local hintIsShowed            = false
local hintToDisplay           = "no hint to display"

ESX                           = nil

Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end
end)

function IsJobTrue()
    if PlayerData ~= nil then
        local IsJobTrue = false
        if PlayerData.job ~= nil and PlayerData.job.name == 'mamas' then
            IsJobTrue = true
        end
        return IsJobTrue
    end
end

function IsGradeRecrue()
    if PlayerData ~= nil then
        local IsGradeRecrue = false
        if PlayerData.job.grade_name == 'recrue' then
            IsGradeRecrue = true
        end
        return IsGradeRecrue
    end
end

function IsGradeVigile()
    if PlayerData ~= nil then
        local IsGradeVigile = false
        if PlayerData.job.grade_name == 'security' then
            IsGradeVigile = true
        end
		if PlayerData.job.grade_name == 'chef' then
            IsGradeVigile = true
        end
        return IsGradeVigile
    end
end

function IsGradeBoss()
    if PlayerData ~= nil then
        local IsGradeBoss = false
        if PlayerData.job.grade_name == 'boss' then
            IsGradeBoss = true
        end
        return IsGradeBoss
    end
end

function SetVehicleMaxMods(vehicle)

  local props = {
    modEngine       = 4,
    modBrakes       = 4,
    modTransmission = 4,
    modSuspension   = 4,
    modTurbo        = true,
  }

  ESX.Game.SetVehicleProperties(vehicle, props)

end

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

function OpenCloakroomMenu()

  local elements = {
        {label = _U('citizen_wear'), value = 'citizen_wear'},
        {label = _U('recrue_outfit'), value = 'recrue_outfit'},
        {label = _U('barman_outfit'), value = 'barman_outfit'},
      } 

  ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'cloakroom',
      {
        title    = _U('cloakroom'),
        align    = 'top-left',
        elements = elements,
        },

        function(data, menu)

      --menu.close()

        if data.current.value == 'citizen_wear' then
            ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
                local playerPed = GetPlayerPed(-1)
                TriggerEvent('skinchanger:loadSkin', skin)
                ClearPedBloodDamage(playerPed)
                ResetPedVisibleDamage(playerPed)
                ClearPedLastWeaponDamage(playerPed)

                ResetPedMovementClipset(playerPed, 0)

                isService = false	
                isBarman = false
            end)
        end
        if data.current.value == 'recrue_outfit' then
            ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
              local playerPed = GetPlayerPed(-1)
              if skin.sex == 0 then
                SetPedComponentVariation(playerPed, 8, 44, 0, 0) --T-shirt
                SetPedComponentVariation(playerPed, 11, 69, 3, 0) --Torse
                --SetPedComponentVariation(playerPed, 10, 0, 0, 0) --Decals
                SetPedComponentVariation(playerPed, 3, 1, 0, 0) --Bras
                SetPedComponentVariation(playerPed, 4, 74, 0, 0) --Pantalon
                SetPedComponentVariation(playerPed, 6, 24, 0, 0) --Chaussures
                SetPedComponentVariation(playerPed, 7, 0, 0, 0) --Collier

                ClearPedBloodDamage(playerPed)
                ResetPedVisibleDamage(playerPed)
                ClearPedLastWeaponDamage(playerPed)

                --RequestAnimSet("MOVE_M@POSH@")
                --while not HasAnimSetLoaded("MOVE_M@POSH@") do
                  --Citizen.Wait(0)
                --end
                --SetPedMovementClipset(playerPed, "MOVE_M@POSH@", true)

                isService = true
                isBarman = false
				TriggerServerEvent("player:serviceOn", "bahama")
              else
                SetPedComponentVariation(playerPed, 8, 44, 1, 0) --T-shirt
                SetPedComponentVariation(playerPed, 11, 63, 3, 0) --Torse
                --SetPedComponentVariation(playerPed, 10, 0, 0, 0) --Decals
                SetPedComponentVariation(playerPed, 3, 0, 0, 0) --Bras
                SetPedComponentVariation(playerPed, 4, 45, 1, 0) --Pantalon
                SetPedComponentVariation(playerPed, 6, 25, 0, 0) --Chaussures
                SetPedComponentVariation(playerPed, 7, 0, 0, 0) --Collier

                ClearPedBloodDamage(playerPed)
                ResetPedVisibleDamage(playerPed)
                ClearPedLastWeaponDamage(playerPed)

                --RequestAnimSet("MOVE_F@POSH@")
                --while not HasAnimSetLoaded("MOVE_F@POSH@") do
                  --Citizen.Wait(0)
                --end
                --SetPedMovementClipset(playerPed, "MOVE_F@POSH@", true)

                isService = true
                isBarman = false
				TriggerServerEvent("player:serviceOn", "bahama")
              end
            end)
        end
        if data.current.value == 'barman_outfit' and not IsGradeRecrue() then
            ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
              local playerPed = GetPlayerPed(-1)
              if skin.sex == 0 then
                SetPedComponentVariation(playerPed, 8, 31, 0, 0) --T-shirt
                SetPedComponentVariation(playerPed, 11, 29, 5, 0) --Torse
                --SetPedComponentVariation(playerPed, 0, 0, 0, 0) --Decals
                SetPedComponentVariation(playerPed, 3, 77, 0, 0) --Bras
                SetPedComponentVariation(playerPed, 4, 20, 0, 0) --Pantalon
                SetPedComponentVariation(playerPed, 6, 10, 0, 0) --Chaussures
                SetPedComponentVariation(playerPed, 7, 11, 2, 0) --Collier

                ClearPedBloodDamage(playerPed)
                ResetPedVisibleDamage(playerPed)
                ClearPedLastWeaponDamage(playerPed)

                ResetPedMovementClipset(playerPed, 0)

				isService = false
                isBarman = true
				TriggerServerEvent("player:serviceOn", "bahama")
              else
                SetPedComponentVariation(playerPed, 8, 40, 2, 0) --T-shirt
                SetPedComponentVariation(playerPed, 11, 7, 1, 0) --Torse
                --SetPedComponentVariation(playerPed, 10, 0, 0, 0) --Decals
                SetPedComponentVariation(playerPed, 3, 86, 0, 0) --Bras
                SetPedComponentVariation(playerPed, 4, 27, 0, 0) --Pantalon
                SetPedComponentVariation(playerPed, 6, 6, 0, 0) --Chaussures
                SetPedComponentVariation(playerPed, 0, 0, 0, 0) --Collier

                ClearPedBloodDamage(playerPed)
                ResetPedVisibleDamage(playerPed)
                ClearPedLastWeaponDamage(playerPed)

                ResetPedMovementClipset(playerPed, 0)

                isService = false
                isBarman = true
				TriggerServerEvent("player:serviceOn", "bahama")
              end
            end)
        end

      CurrentAction     = 'menu_cloakroom'
      CurrentActionMsg  = _U('open_cloackroom')
      CurrentActionData = {}

    end,
    function(data, menu)

      menu.close()

      CurrentAction     = 'menu_cloakroom'
      CurrentActionMsg  = _U('open_cloackroom')
      CurrentActionData = {}
    end
    )

end

function OpenVaultMenu()

  if Config.EnableVaultManagement and not IsGradeRecrue() then

    local elements = {
      {label = _U('get_weapon'), value = 'get_weapon'},
      {label = _U('put_weapon'), value = 'put_weapon'},
      {label = _U('get_object'), value = 'get_stock'},
      {label = _U('put_object'), value = 'put_stock'}
    }
    

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'vault',
      {
        title    = _U('vault'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        if data.current.value == 'get_weapon' and not IsGradeRecrue() then
          OpenGetWeaponMenu()
        end

        if data.current.value == 'put_weapon' then
          OpenPutWeaponMenu()
        end

        if data.current.value == 'put_stock' then
           OpenPutStocksMenu()
        end

        if data.current.value == 'get_stock' and not IsGradeRecrue() then
           OpenGetStocksMenu()
        end

      end,
      
      function(data, menu)

        menu.close()

        CurrentAction     = 'menu_vault'
        CurrentActionMsg  = _U('open_vault')
        CurrentActionData = {}
      end
    )

  end

end

function OpenFridgeMenu()

    local elements = {
      {label = _U('get_object'), value = 'get_stock'},
      {label = _U('put_object'), value = 'put_stock'}
    }
    

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'fridge',
      {
        title    = _U('fridge'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        if data.current.value == 'put_stock' then
           OpenPutFridgeStocksMenu()
        end

        if data.current.value == 'get_stock' then
           OpenGetFridgeStocksMenu()
        end

      end,
      
      function(data, menu)

        menu.close()

        CurrentAction     = 'menu_fridge'
        CurrentActionMsg  = _U('open_fridge')
        CurrentActionData = {}
      end
    )

end


function OpenVehicleSpawnerMenu()

  local vehicles = Config.Zones.Vehicles

  ESX.UI.Menu.CloseAll()

  if Config.EnableSocietyOwnedVehicles then

    local elements = {}

    ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)

      for i=1, #garageVehicles, 1 do
        table.insert(elements, {label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']', value = garageVehicles[i]})
      end

      ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'vehicle_spawner',
        {
          title    = _U('vehicle_menu'),
          align    = 'top-left',
          elements = elements,
        },
        function(data, menu)

          menu.close()

          local vehicleProps = data.current.value
          ESX.Game.SpawnVehicle(vehicleProps.model, vehicles.SpawnPoint, vehicles.Heading, function(vehicle)
              ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
              local playerPed = GetPlayerPed(-1)
			  SetVehicleOnGroundProperly(vehicle)
		SetVehicleHasBeenOwnedByPlayer(vehicle, true)
		local id = NetworkGetNetworkIdFromEntity(vehicle)
		SetNetworkIdCanMigrate(id, true)
              TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)  -- teleport into vehicle
          end)            

          TriggerServerEvent('esx_society:removeVehicleFromGarage', 'mamas', vehicleProps)

        end,
        function(data, menu)

          menu.close()

          CurrentAction     = 'menu_vehicle_spawner'
          CurrentActionMsg  = _U('vehicle_spawner')
          CurrentActionData = {}

        end
      )

    end, 'mamas')

  else

    local elements = {}

    for i=1, #Config.AuthorizedVehicles, 1 do
      local vehicle = Config.AuthorizedVehicles[i]
      table.insert(elements, {label = vehicle.label, value = vehicle.name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'vehicle_spawner',
      {
        title    = _U('vehicle_menu'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        menu.close()

        local model = data.current.value

        local vehicle = GetClosestVehicle(vehicles.SpawnPoint.x,  vehicles.SpawnPoint.y,  vehicles.SpawnPoint.z,  3.0,  0,  71)

        if not DoesEntityExist(vehicle) then

          local playerPed = GetPlayerPed(-1)

          if Config.MaxInService == -1 then

            ESX.Game.SpawnVehicle(model, {
              x = vehicles.SpawnPoint.x,
              y = vehicles.SpawnPoint.y,
              z = vehicles.SpawnPoint.z
            }, vehicles.Heading, function(vehicle)
			  SetVehicleNumberPlateText(vehicle, 'BAHA ' .. ESX.GetRandomString(3))
			  SetVehicleOnGroundProperly(vehicle)
		SetVehicleHasBeenOwnedByPlayer(vehicle, true)
		local id = NetworkGetNetworkIdFromEntity(vehicle)
		SetNetworkIdCanMigrate(id, true)
              TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1) -- teleport into vehicle
              SetVehicleMaxMods(vehicle)
              SetVehicleDirtLevel(vehicle, 0)
            end)

          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                ESX.Game.SpawnVehicle(model, {
                  x = vehicles[partNum].SpawnPoint.x,
                  y = vehicles[partNum].SpawnPoint.y,
                  z = vehicles[partNum].SpawnPoint.z
                }, vehicles[partNum].Heading, function(vehicle)
				  SetVehicleNumberPlateText(vehicle, 'BAHA ' .. ESX.GetRandomString(3))
				  SetVehicleOnGroundProperly(vehicle)
		SetVehicleHasBeenOwnedByPlayer(vehicle, true)
		local id = NetworkGetNetworkIdFromEntity(vehicle)
		SetNetworkIdCanMigrate(id, true)
                  TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)  -- teleport into vehicle
                  SetVehicleMaxMods(vehicle)
                  SetVehicleDirtLevel(vehicle, 0)
                end)

              else
                ESX.ShowNotification(_U('service_max') .. inServiceCount .. '/' .. maxInService)
              end

            end, 'etat')

          end

        else
          ESX.ShowNotification(_U('vehicle_out'))
        end

      end,
      function(data, menu)

        menu.close()

        CurrentAction     = 'menu_vehicle_spawner'
        CurrentActionMsg  = _U('vehicle_spawner')
        CurrentActionData = {}

      end
    )

  end

end

function OpenVehicle2SpawnerMenu()

  local vehicles2 = Config.Zones.Vehicles2

  ESX.UI.Menu.CloseAll()

    local elements = {}

    for i=1, #Config.AuthorizedVehicles2, 1 do
      local vehicle2 = Config.AuthorizedVehicles2[i]
      table.insert(elements, {label = vehicle2.label, value = vehicle2.name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'vehicle2_spawner',
      {
        title    = _U('vehicle2_menu'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        menu.close()

        local model = data.current.value

        local vehicle2 = GetClosestVehicle(vehicles2.SpawnPoint.x,  vehicles2.SpawnPoint.y,  vehicles2.SpawnPoint.z,  3.0,  0,  71)

        if not DoesEntityExist(vehicle2) then

          local playerPed = GetPlayerPed(-1)

          if Config.MaxInService == -1 then

            ESX.Game.SpawnVehicle(model, {
              x = vehicles2.SpawnPoint.x,
              y = vehicles2.SpawnPoint.y,
              z = vehicles2.SpawnPoint.z
            }, vehicles2.Heading, function(vehicle)
			  SetVehicleNumberPlateText(vehicle, 'BAHA ' .. ESX.GetRandomString(3))
			  SetVehicleOnGroundProperly(vehicle)
		SetVehicleHasBeenOwnedByPlayer(vehicle, true)
		local id = NetworkGetNetworkIdFromEntity(vehicle)
		SetNetworkIdCanMigrate(id, true)
              TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1) -- teleport into vehicle
              SetVehicleMaxMods(vehicle)
              SetVehicleDirtLevel(vehicle, 0)
            end)

          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                ESX.Game.SpawnVehicle(model, {
                  x = vehicles2[partNum].SpawnPoint.x,
                  y = vehicles2[partNum].SpawnPoint.y,
                  z = vehicles2[partNum].SpawnPoint.z
                }, vehicles2[partNum].Heading, function(vehicle)
				  SetVehicleNumberPlateText(vehicle, 'BAHA ' .. ESX.GetRandomString(3))
				  SetVehicleOnGroundProperly(vehicle)
		SetVehicleHasBeenOwnedByPlayer(vehicle, true)
		local id = NetworkGetNetworkIdFromEntity(vehicle)
		SetNetworkIdCanMigrate(id, true)
                  TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)  -- teleport into vehicle
                  SetVehicleMaxMods(vehicle)
                  SetVehicleDirtLevel(vehicle, 0)
                end)

              else
                ESX.ShowNotification(_U('service_max') .. inServiceCount .. '/' .. maxInService)
              end

            end, 'etat')

          end

        else
          ESX.ShowNotification(_U('vehicle2_out'))
        end

      end,
      function(data, menu)

        menu.close()

        CurrentAction     = 'menu_vehicle2_spawner'
        CurrentActionMsg  = _U('vehicle2_spawner')
        CurrentActionData = {}

      end
    )

end

function OpenHelicoSpawnerMenu()

  local helicos = Config.Zones.Helicopters

  ESX.UI.Menu.CloseAll()

    local elements = {}

    for i=1, #Config.AuthorizedHelico, 1 do
      local helico = Config.AuthorizedHelico[i]
      table.insert(elements, {label = helico.label, value = helico.name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'helico_spawner',
      {
        title    = _U('helico_menu'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        menu.close()

        local model = data.current.value

        local helico = GetClosestVehicle(helicos.SpawnPoint.x,  helicos.SpawnPoint.y,  helicos.SpawnPoint.z,  3.0,  0,  71)

        if not DoesEntityExist(helico) then

          local playerPed = GetPlayerPed(-1)

          if Config.MaxInService == -1 then

            ESX.Game.SpawnVehicle(model, {
              x = helicos.SpawnPoint.x,
              y = helicos.SpawnPoint.y,
              z = helicos.SpawnPoint.z
            }, helicos.Heading, function(vehicle)
              TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1) -- teleport into vehicle
              SetVehicleMaxMods(vehicle)
              SetVehicleDirtLevel(vehicle, 0)
            end)

          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                ESX.Game.SpawnVehicle(model, {
                  x = helicos[partNum].SpawnPoint.x,
                  y = helicos[partNum].SpawnPoint.y,
                  z = helicos[partNum].SpawnPoint.z
                }, helicos[partNum].Heading, function(vehicle)
                  TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)  -- teleport into vehicle
                  SetVehicleMaxMods(vehicle)
                  SetVehicleDirtLevel(vehicle, 0)
                end)

              else
                ESX.ShowNotification(_U('service_max') .. inServiceCount .. '/' .. maxInService)
              end

            end, 'etat')

          end

        else
          ESX.ShowNotification(_U('helico_out'))
        end

      end,
      function(data, menu)

        menu.close()

        CurrentAction     = 'menu_helico_spawner'
        CurrentActionMsg  = _U('helico_spawner')
        CurrentActionData = {}

      end
    )

end

function OpenSocietyActionsMenu()

  local elements = {}

  if not IsGradeRecrue() then
  	table.insert(elements, {label = _U('billing'),    value = 'billing'})
  end
  if (isBarman or IsGradeBoss()) then
    table.insert(elements, {label = _U('crafting'),    value = 'menu_crafting'})
  end
  if IsJobTrue() then
  	table.insert(elements, {label = _U('gps'),    value = 'menumamas_gps'})
  end

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'mamas_actions',
    {
      title    = _U('mamas'),
      align    = 'top-left',
      elements = elements
    },
    function(data, menu)

      if data.current.value == 'billing' then
        OpenBillingMenu()
      end

      if data.current.value == 'menumamas_gps' then
        ESX.UI.Menu.Open(
						'default', GetCurrentResourceName(), 'menumamas_gps',
						{
							title    = 'GPS Bahama Mamas',
							align    = 'top-left',
							elements = {
								{label = 'Entreprise', value = 'menumamas_gps_entreprise'},
								{label = 'Récolte Raisin', value = 'menumamas_gps_rraisin'},
								{label = 'Vente Raisin', value = 'menumamas_gps_vraisin'},
                {label = 'Récolte Citron', value = 'menumamas_gps_rcitron'},
                {label = 'Vente Citron', value = 'menumamas_gps_vcitron'},
                {label = 'Récolte Fruit', value = 'menumamas_gps_rfruit'},
                {label = 'Vente Fruit', value = 'menumamas_gps_vfruit'},
                {label = 'Boutique Aliments', value = 'menumamas_gps_aliment'},
                {label = 'Boutique Alcool', value = 'menumamas_gps_alcool'}
							},
						},
						function(data2, menu2)

							if data2.current.value == 'menumamas_gps_entreprise' then
								SetNewWaypoint( -1388.652, -586.179, 30.218 )
								local source = GetPlayerServerId();
								ESX.ShowNotification("~b~Destination ajouté au GPS !")
							end

							if data2.current.value == 'menumamas_gps_rraisin' then
								SetNewWaypoint( -1716.128, 1992.310, 120.909 )
								local source = GetPlayerServerId();
								ESX.ShowNotification("~b~Destination ajouté au GPS !")
							end

							if data2.current.value == 'menumamas_gps_vraisin' then
								SetNewWaypoint( 878.952, -1663.665, 30.434 )
								local source = GetPlayerServerId();
								ESX.ShowNotification("~b~Destination ajouté au GPS !")
							end

				      if data2.current.value == 'menumamas_gps_rcitron' then
				        SetNewWaypoint( -1944.976, 1795.310, 171.927 )
				        local source = GetPlayerServerId();
				        ESX.ShowNotification("~b~Destination ajouté au GPS !")
				      end

				      if data2.current.value == 'menumamas_gps_vcitron' then
				        SetNewWaypoint( -1147.774, -1599.557, 4.389 )
				        local source = GetPlayerServerId();
				        ESX.ShowNotification("~b~Destination ajouté au GPS !")
				      end

              if data2.current.value == 'menumamas_gps_rfruit' then
                SetNewWaypoint( 353.922, 6518.403, 28.334 )
                local source = GetPlayerServerId();
                ESX.ShowNotification("~b~Destination ajouté au GPS !")
              end

              if data2.current.value == 'menumamas_gps_vfruit' then
                SetNewWaypoint( 1408.634, 3619.576, 34.894 )
                local source = GetPlayerServerId();
                ESX.ShowNotification("~b~Destination ajouté au GPS !")
              end

				      if data2.current.value == 'menumamas_gps_aliment' then
				        SetNewWaypoint( -2511.394, 3615.555, 13.653 )
				        local source = GetPlayerServerId();
				        ESX.ShowNotification("~b~Destination ajouté au GPS !")
				      end

				      if data2.current.value == 'menumamas_gps_alcool' then
				        SetNewWaypoint( 1144.034, -299.420, 68.806 )
				        local source = GetPlayerServerId();
				        ESX.ShowNotification("~b~Destination ajouté au GPS !")
				      end

						end,
						function(data2, menu2)
							menu2.close()
						end
					)
      end

      if data.current.value == 'menu_crafting' then
        
          ESX.UI.Menu.Open(
              'default', GetCurrentResourceName(), 'menu_crafting',
              {
                  title = _U('crafting'),
                  align = 'top-left',
                  elements = {
                      {label = _U('vin'), value = 'vin'},
                      {label = _U('mojito'), value = 'mojito'},
                      {label = _U('jusraisin'), value = 'jusraisin'},
					  {label = _U('vodkaenergy'), value = 'vodkaenergy'},
                      {label = _U('rhumfruit'), value = 'rhumfruit'},
					  {label = _U('rhumcoca'), value = 'rhumcoca'},
					  {label = _U('jagerbomb'), value = 'jagerbomb'},
                      {label = _U('jusfruit'), value = 'jusfruit'},
					  {label = _U('whiskycoca'), value = 'whiskycoca'},
					  
                  }
              },
              function(data2, menu2)
            
                TriggerServerEvent('esx_mamasjob:craftingCoktails', data2.current.value)
                --animsAction({ lib = "mini@drinking", anim = "shots_barman_b" })
      
              end,
              function(data2, menu2)
                  menu2.close()
              end
          )
      end
     
    end,
    function(data, menu)

      menu.close()

    end
  )

end

function OpenBillingMenu()
local playerPed        = GetPlayerPed(-1)
	TaskStartScenarioInPlace(playerPed, 'CODE_HUMAN_MEDIC_TIME_OF_DEATH', 0, true)
  ESX.UI.Menu.Open(
    'dialog', GetCurrentResourceName(), 'billing',
    {
      title = _U('billing_amount')
    },
    function(data, menu)
    
      local amount = tonumber(data.value)
      local player, distance = ESX.Game.GetClosestPlayer()

      if player ~= -1 and distance <= 3.0 then

        menu.close()
        if amount == nil then
            ESX.ShowNotification(_U('amount_invalid'))
        else
            TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(player), 'society_mamas', _U('billing'), amount)
        end

      else
        ESX.ShowNotification(_U('no_players_nearby'))
      end

    end,
    function(data, menu)
        menu.close()
		ClearPedTasks(playerPed)
    end
  )
end

function OpenGetStocksMenu()

  ESX.TriggerServerCallback('esx_mamasjob:getStockItems', function(items)

    local elements = {}

    for i=1, #items, 1 do
      table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('mamas_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('esx_mamasjob:getStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutStocksMenu()

ESX.TriggerServerCallback('esx_mamasjob:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('esx_mamasjob:putStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenGetFridgeStocksMenu()

  ESX.TriggerServerCallback('esx_mamasjob:getFridgeStockItems', function(items)

    local elements = {}

    for i=1, #items, 1 do
      table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('mamas_fridge_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('esx_mamasjob:getFridgeStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutFridgeStocksMenu()

ESX.TriggerServerCallback('esx_mamasjob:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('fridge_inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              OpenPutFridgeStocksMenu()

              TriggerServerEvent('esx_mamasjob:putFridgeStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenGetWeaponMenu()

  ESX.TriggerServerCallback('esx_mamasjob:getVaultWeapons', function(weapons)

    local elements = {}

    for i=1, #weapons, 1 do
      if weapons[i].count > 0 then
        table.insert(elements, {label = 'x' .. weapons[i].count .. ' ' .. ESX.GetWeaponLabel(weapons[i].name), value = weapons[i].name})
      end
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'vault_get_weapon',
      {
        title    = _U('get_weapon_menu'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        menu.close()

        ESX.TriggerServerCallback('esx_mamasjob:removeVaultWeapon', function()
          OpenGetWeaponMenu()
        end, data.current.value)

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutWeaponMenu()

  local elements   = {}
  local playerPed  = GetPlayerPed(-1)
  local weaponList = ESX.GetWeaponList()

  for i=1, #weaponList, 1 do

    local weaponHash = GetHashKey(weaponList[i].name)

    if HasPedGotWeapon(playerPed,  weaponHash,  false) and weaponList[i].name ~= 'WEAPON_UNARMED' then
      local ammo = GetAmmoInPedWeapon(playerPed, weaponHash)
      table.insert(elements, {label = weaponList[i].label, value = weaponList[i].name})
    end

  end

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'vault_put_weapon',
    {
      title    = _U('put_weapon_menu'),
      align    = 'top-left',
      elements = elements,
    },
    function(data, menu)

      menu.close()

      ESX.TriggerServerCallback('esx_mamasjob:addVaultWeapon', function()
        OpenPutWeaponMenu()
      end, data.current.value)

    end,
    function(data, menu)
      menu.close()
    end
  )

end



function OpenShopMenu(zone)

    local elements = {}
    for i=1, #Config.Zones[zone].Items, 1 do

        local item = Config.Zones[zone].Items[i]

        table.insert(elements, {
            label     = item.label .. ' - <span style="color:green;">$' .. item.price .. ' </span>',
            realLabel = item.label,
            value     = item.name,
            price     = item.price
        })

    end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'mamas_shop',
        {
            title    = _U('shop'),
            elements = elements
        },
        function(data, menu)
            TriggerServerEvent('esx_mamasjob:buyItem', data.current.value, data.current.price, data.current.realLabel)
        end,
        function(data, menu)
            menu.close()
        end
    )

end


function animsAction(animObj)
    Citizen.CreateThread(function()
        if not playAnim then
            local playerPed = GetPlayerPed(-1);
            if DoesEntityExist(playerPed) then -- Check if ped exist
                dataAnim = animObj

                -- Play Animation
                RequestAnimDict(dataAnim.lib)
                while not HasAnimDictLoaded(dataAnim.lib) do
                    Citizen.Wait(0)
                end
                if HasAnimDictLoaded(dataAnim.lib) then
                    local flag = 0
                    if dataAnim.loop ~= nil and dataAnim.loop then
                        flag = 1
                    elseif dataAnim.move ~= nil and dataAnim.move then
                        flag = 49
                    end

                    TaskPlayAnim(playerPed, dataAnim.lib, dataAnim.anim, 8.0, -8.0, -1, flag, 0, 0, 0, 0)
                    playAnimation = true
                end

                -- Wait end animation
                while true do
                    Citizen.Wait(0)
                    if not IsEntityPlayingAnim(playerPed, dataAnim.lib, dataAnim.anim, 3) then
                        playAnim = false
                        TriggerEvent('ft_animation:ClFinish')
                        break
                    end
                end
            end -- end ped exist
        end
    end)
end


AddEventHandler('esx_mamasjob:hasEnteredMarker', function(zone)
 
    if zone == 'BossActions' and IsGradeBoss() then
      CurrentAction     = 'menu_boss_actions'
      CurrentActionMsg  = _U('open_bossmenu')
      CurrentActionData = {}
    end

    if zone == 'Cloakrooms' then
      CurrentAction     = 'menu_cloakroom'
      CurrentActionMsg  = _U('open_cloackroom')
      CurrentActionData = {}
    end

    if Config.EnableVaultManagement then
      if zone == 'Vaults' then
        CurrentAction     = 'menu_vault'
        CurrentActionMsg  = _U('open_vault')
        CurrentActionData = {}
      end
    end

    if zone == 'Fridge' then
      CurrentAction     = 'menu_fridge'
      CurrentActionMsg  = _U('open_fridge')
      CurrentActionData = {}
    end

    if zone == 'Aliment' or  zone == 'Alcool' then
      CurrentAction     = 'menu_shop'
      CurrentActionMsg  = _U('shop_menu')
      CurrentActionData = {zone = zone}
    end
    
    if zone == 'Vehicles' then
        CurrentAction     = 'menu_vehicle_spawner'
        CurrentActionMsg  = _U('vehicle_spawner')
        CurrentActionData = {}
    end

    if IsGradeBoss() or IsGradeVigile() then
      if zone == 'Vehicles2' then
        CurrentAction     = 'menu_vehicle2_spawner'
        CurrentActionMsg  = _U('vehicle2_spawner')
        CurrentActionData = {}
      end
    end

    if zone == 'VehicleDeleters' then

      local playerPed = GetPlayerPed(-1)

      if IsPedInAnyVehicle(playerPed,  false) then

        local vehicle = GetVehiclePedIsIn(playerPed,  false)

        CurrentAction     = 'delete_vehicle'
        CurrentActionMsg  = _U('store_vehicle')
        CurrentActionData = {vehicle = vehicle}
      end

    end

    if zone == 'RaisinFarm' then
            if IsJobTrue() then
                CurrentAction     = 'raisin_harvest'
                CurrentActionMsg  = _U('press_collect_raisin')
                CurrentActionData = {}
            end
    end

    if zone == 'RaisinResell' then
            if IsJobTrue() then
                if raisinQTE >= 1 then
                    CurrentAction     = 'raisin_resell'
                    CurrentActionMsg  = _U('press_sell_raisin')
                    CurrentActionData = {}
                end
            end
    end

    if zone == 'CitronFarm' then
            if IsJobTrue() then
                CurrentAction     = 'citron_harvest'
                CurrentActionMsg  = _U('press_collect_citron')
                CurrentActionData = {}
            end
    end

    if zone == 'CitronResell' then
            if IsJobTrue() then
                if citronQTE >= 1 then
                    CurrentAction     = 'citron_resell'
                    CurrentActionMsg  = _U('press_sell_citron')
                    CurrentActionData = {}
                end
            end
    end

    if zone == 'FruitFarm' then
            if IsJobTrue() then
                CurrentAction     = 'fruit_harvest'
                CurrentActionMsg  = _U('press_collect_fruit')
                CurrentActionData = {}
            end
    end

    if zone == 'FruitResell' then
            if IsJobTrue() then
                if fruitQTE >= 1 then
                    CurrentAction     = 'fruit_resell'
                    CurrentActionMsg  = _U('press_sell_fruit')
                    CurrentActionData = {}
                end
            end
    end

    if Config.EnableHelicopters and IsGradeBoss() or IsGradeVigile() then
        
    	if zone == 'Helicopters' then
      		CurrentAction     = 'menu_helico_spawner'
      		CurrentActionMsg  = _U('helico_spawner')
      		CurrentActionData = {}
    	end

        if zone == 'HelicopterDeleters' then

          local playerPed = GetPlayerPed(-1)

          if IsPedInAnyVehicle(playerPed,  false) then

            local vehicle = GetVehiclePedIsIn(playerPed,  false)

            CurrentAction     = 'delete_vehicle'
            CurrentActionMsg  = _U('store_vehicle')
            CurrentActionData = {vehicle = vehicle}
          end

        end
    end


end)

AddEventHandler('esx_mamasjob:hasExitedMarker', function(zone)

    CurrentAction = nil
    ESX.UI.Menu.CloseAll()

    TriggerServerEvent('esx_mamasjob:stopHarvestCitron')
    TriggerServerEvent('esx_mamasjob:stopSellCitron')
    TriggerServerEvent('esx_mamasjob:stopHarvestRaisin')
    TriggerServerEvent('esx_mamasjob:stopSellRaisin')
    TriggerServerEvent('esx_mamasjob:stopHarvestFruit')
    TriggerServerEvent('esx_mamasjob:stopSellFruit')

end)

-- RETURN NUMBER OF ITEMS FROM SERVER
RegisterNetEvent('esx_mamasjob:ReturnInventory')
AddEventHandler('esx_mamasjob:ReturnInventory', function(raisinNbr, citronNbr, fruitNbr, currentZone)
    raisinQTE       = raisinNbr
    citronQTE       = citronNbr
    fruitQTE        = fruitNbr
    TriggerEvent('esx_mamasjob:hasEnteredMarker', currentZone)
end)

RegisterNetEvent('esx_phone:loaded')
AddEventHandler('esx_phone:loaded', function(phoneNumber, contacts)
  local specialContact = {
    name       = _U('mamas_phone'),
    number     = 'mamas',
    base64Icon = 'https://makemeacocktail.com/icon_32.png'
  }
  TriggerEvent('esx_phone:addSpecialContact', specialContact.name, specialContact.number, specialContact.base64Icon)
end)

-- Create blips
Citizen.CreateThread(function()

    local blipMarker = Config.Blips.Blip
    local blipCoord = AddBlipForCoord(blipMarker.Pos.x, blipMarker.Pos.y, blipMarker.Pos.z)

    SetBlipSprite (blipCoord, blipMarker.Sprite)
    SetBlipDisplay(blipCoord, blipMarker.Display)
    SetBlipScale  (blipCoord, blipMarker.Scale)
    --SetBlipColour (blipCoord, blipMarker.Colour)
    SetBlipAsShortRange(blipCoord, true)

    BeginTextCommandSetBlipName("STRING")
    AddTextComponentString(_U('map_blip'))
    EndTextCommandSetBlipName(blipCoord)

end)

-- Display markers
Citizen.CreateThread(function()
    while true do

        Wait(0)
        if IsJobTrue() then

            local coords = GetEntityCoords(GetPlayerPed(-1))

            for k,v in pairs(Config.Zones) do
                if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
                    DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z-1, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, v.Color.a, false, false, 2, false, false, false, false)
                end
            end

        end

    end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
    while true do

        Wait(0)
        if IsJobTrue() then

            local coords      = GetEntityCoords(GetPlayerPed(-1))
            local isInMarker  = false
            local currentZone = nil

            for k,v in pairs(Config.Zones) do
                if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
                    isInMarker  = true
                    currentZone = k
                end
            end

            if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
                HasAlreadyEnteredMarker = true
                LastZone                = currentZone
                TriggerEvent('esx_mamasjob:hasEnteredMarker', currentZone)
                TriggerServerEvent('esx_mamasjob:GetUserInventory', currentZone)
            end

            if not isInMarker and HasAlreadyEnteredMarker then
                HasAlreadyEnteredMarker = false
                TriggerEvent('esx_mamasjob:hasExitedMarker', LastZone)
            end

        end

    end
end)

-- Key Controls
Citizen.CreateThread(function()
  while true do

    Citizen.Wait(0)

    if CurrentAction ~= nil then

      SetTextComponentFormat('STRING')
      AddTextComponentString(CurrentActionMsg)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)

      if IsControlJustReleased(0,  Keys['E']) and IsJobTrue() then

        if CurrentAction == 'menu_cloakroom' then
            OpenCloakroomMenu()
        end

        if CurrentAction == 'menu_vault' then
            OpenVaultMenu()
        end

        if CurrentAction == 'menu_fridge' then
            OpenFridgeMenu()
        end

        if CurrentAction == 'menu_shop' then
            OpenShopMenu(CurrentActionData.zone)
        end

        if CurrentAction == 'raisin_harvest' then
            TriggerServerEvent('esx_mamasjob:startHarvestRaisin')
        end

        if CurrentAction == 'raisin_resell' then
            TriggerServerEvent('esx_mamasjob:startSellRaisin')
        end

        if CurrentAction == 'citron_harvest' then
            TriggerServerEvent('esx_mamasjob:startHarvestCitron')
        end

        if CurrentAction == 'citron_resell' then
            TriggerServerEvent('esx_mamasjob:startSellCitron')
        end

        if CurrentAction == 'fruit_harvest' then
            TriggerServerEvent('esx_mamasjob:startHarvestFruit')
        end

        if CurrentAction == 'fruit_resell' then
            TriggerServerEvent('esx_mamasjob:startSellFruit')
        end
        
        if CurrentAction == 'menu_vehicle_spawner' then
            OpenVehicleSpawnerMenu()
        end

        if CurrentAction == 'menu_vehicle2_spawner' then
            OpenVehicle2SpawnerMenu()
        end

        if CurrentAction == 'delete_vehicle' then

          if Config.EnableSocietyOwnedVehicles then

            local vehicleProps = ESX.Game.GetVehicleProperties(CurrentActionData.vehicle)
            TriggerServerEvent('esx_society:putVehicleInGarage', 'mamas', vehicleProps)

          else

            if
              GetEntityModel(vehicle) == GetHashKey('xls2')
            then
              TriggerServerEvent('esx_service:disableService', 'mamas')
            end

          end

          ESX.Game.DeleteVehicle(CurrentActionData.vehicle)
        end

        if CurrentAction == 'menu_helico_spawner' then
            OpenHelicoSpawnerMenu()
        end

        if CurrentAction == 'menu_boss_actions' and IsGradeBoss() then

          ESX.UI.Menu.CloseAll()

          TriggerEvent('esx_society:openBossMenu', 'mamas', function(data, menu)

            menu.close()
            CurrentAction     = 'menu_boss_actions'
            CurrentActionMsg  = _U('open_bossmenu')
            CurrentActionData = {}

          end)

        end

        
        CurrentAction = nil

      end

    end

    if IsControlJustReleased(0,  Keys['F6']) and IsJobTrue() and not ESX.UI.Menu.IsOpen('default', GetCurrentResourceName(), 'mamas_actions') then
        OpenSocietyActionsMenu()
    end

  end
end)


-----------------------
----- TELEPORTERS -----

AddEventHandler('esx_mamasjob:teleportMarkers', function(position)
  --Citizen.Wait(1500)
  SetEntityCoords(GetPlayerPed(-1), position.x, position.y, position.z+1)
end)

--[[function EnterOffice()
	Citizen.CreateThread(function()

    DoScreenFadeOut(800)

	LoadMpDlcMaps()
    EnableMpDlcMaps(true)
    RequestIpl('ex_sm_15_office_02b')

    DoScreenFadeIn(800)

	end)
end]]--

-- Show top left hint
Citizen.CreateThread(function()
  while true do
    Wait(0)
    if hintIsShowed == true then
      SetTextComponentFormat("STRING")
      AddTextComponentString(hintToDisplay)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)
    end
  end
end)

-- Display teleport markers
Citizen.CreateThread(function()
  while true do
    Wait(0)

    if IsJobTrue() then

        local coords = GetEntityCoords(GetPlayerPed(-1))
        for k,v in pairs(Config.TeleportZones) do
          if(v.Marker ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
            DrawMarker(v.Marker, v.Pos.x, v.Pos.y, v.Pos.z-1, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, v.Color.a, false, true, 2, false, false, false, false)
          end
        end

    end

  end
end)

-- Activate teleport marker
Citizen.CreateThread(function()
  while true do
    Wait(0)
    local coords      = GetEntityCoords(GetPlayerPed(-1))
    local position    = nil
    local zone        = nil

    if IsJobTrue() then

        for k,v in pairs(Config.TeleportZones) do
          if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
            isInPublicMarker = true
            position = v.Teleport
            zone = v
            break
          else
            isInPublicMarker  = false
          end
        end

        if IsControlJustReleased(0, Keys["E"]) and isInPublicMarker then
          --EnterOffice()
          TriggerEvent('esx_mamasjob:teleportMarkers', position)
        end

        -- hide or show top left zone hints
        if isInPublicMarker then
          hintToDisplay = zone.Hint
          hintIsShowed = true
        else
          if not isInMarker then
            hintToDisplay = "no hint to display"
            hintIsShowed = false
          end
        end

    end

  end
end)

-----------------------
