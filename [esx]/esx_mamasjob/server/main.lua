ESX                             = nil
local PlayersHarvestingRaisin   = {}
local PlayersSellingRaisin      = {}
local PlayersHarvestingCitron   = {}
local PlayersSellingCitron      = {}
local PlayersHarvestingFruit    = {}
local PlayersSellingFruit       = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
  TriggerEvent('esx_service:activateService', 'mamas', Config.MaxInService)
end

TriggerEvent('esx_phone:registerNumber', 'mamas', _U('mamas_customer'), true, true)
TriggerEvent('esx_society:registerSociety', 'mamas', 'Bahama Mamas', 'society_mamas', 'society_mamas', 'society_mamas', {type = 'private'})



RegisterServerEvent('esx_mamasjob:getStockItem')
AddEventHandler('esx_mamasjob:getStockItem', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_mamas', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_removed') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('esx_mamasjob:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_mamas', function(inventory)
    cb(inventory.items)
  end)

end)

RegisterServerEvent('esx_mamasjob:putStockItems')
AddEventHandler('esx_mamasjob:putStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_mamas', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= 0 then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_added') .. count .. ' ' .. item.label)

  end)

end)


RegisterServerEvent('esx_mamasjob:getFridgeStockItem')
AddEventHandler('esx_mamasjob:getFridgeStockItem', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_mamas_fridge', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_removed') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('esx_mamasjob:getFridgeStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_mamas_fridge', function(inventory)
    cb(inventory.items)
  end)

end)

RegisterServerEvent('esx_mamasjob:putFridgeStockItems')
AddEventHandler('esx_mamasjob:putFridgeStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_mamas_fridge', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= 0 then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_added') .. count .. ' ' .. item.label)

  end)

end)


RegisterServerEvent('esx_mamasjob:buyItem')
AddEventHandler('esx_mamasjob:buyItem', function(itemName, price, itemLabel)

    local _source = source
    local xPlayer  = ESX.GetPlayerFromId(_source)
    local limit = xPlayer.getInventoryItem(itemName).limit
    local qtty = xPlayer.getInventoryItem(itemName).count

    if xPlayer.get('money') >= price then
        
            xPlayer.removeMoney(price)
            xPlayer.addInventoryItem(itemName, 1)
            TriggerClientEvent('esx:showNotification', _source, _U('bought') .. itemLabel)
        
        
    else
        TriggerClientEvent('esx:showNotification', _source, _U('not_enough'))
    end

end)


RegisterServerEvent('esx_mamasjob:craftingCoktails')
AddEventHandler('esx_mamasjob:craftingCoktails', function(itemValue)

    local _source = source
    local _itemValue = itemValue
    TriggerClientEvent('esx:showNotification', _source, _U('assembling_cocktail'))

    if _itemValue == 'vin' then
        SetTimeout(10000, function()        

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('raisin').count
            local bethQuantity      = xPlayer.getInventoryItem('alcool').count

            if alephQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('raisin'))
            elseif bethQuantity < 2 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('alcool'))
            else
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('raisin', 1)
                    xPlayer.removeInventoryItem('alcool', 2)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('vin'))
                    xPlayer.removeInventoryItem('raisin', 1)
                    xPlayer.removeInventoryItem('alcool', 2)
                    xPlayer.addInventoryItem('vin', 1)
                end
            end

        end)
    end

    if _itemValue == 'mojito' then
        SetTimeout(10000, function()        

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('rhum').count
            local bethQuantity      = xPlayer.getInventoryItem('citron').count
            local cethQuantity      = xPlayer.getInventoryItem('menthe').count
            local dethQuantity      = xPlayer.getInventoryItem('sucre').count

            if alephQuantity < 2 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('rhum'))
            elseif bethQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('citron'))
            elseif cethQuantity < 2 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('menthe'))
            elseif dethQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('sucre'))
            else
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('rhum', 2)
                    xPlayer.removeInventoryItem('citron', 1)
                    xPlayer.removeInventoryItem('menthe', 2)
                    xPlayer.removeInventoryItem('sucre', 1)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('mojito'))
                    xPlayer.removeInventoryItem('rhum', 2)
                    xPlayer.removeInventoryItem('citron', 1)
                    xPlayer.removeInventoryItem('menthe', 2)
                    xPlayer.removeInventoryItem('sucre', 1)
                    xPlayer.addInventoryItem('mojito', 1)
                end
            end

        end)
    end

    if _itemValue == 'jusraisin' then
        SetTimeout(10000, function()        

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('raisin').count

            if alephQuantity < 2 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('raisin'))
            else
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('raisin', 2)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('jusraisin'))
                    xPlayer.removeInventoryItem('raisin', 2)
                    xPlayer.addInventoryItem('jusraisin', 1)
                end
            end

        end)
    end
	
	
	if _itemValue == 'vodkaenergy' then
        SetTimeout(10000, function()        

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('vodka').count
			local bethQuantity     = xPlayer.getInventoryItem('redbull').count
			
            if alephQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('vodka'))
			elseif bethQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('redbull'))
            else
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('vodka', 1)
					xPlayer.removeInventoryItem('redbull', 1)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('vodkaenergy'))
                    xPlayer.removeInventoryItem('vodka', 1)
					xPlayer.removeInventoryItem('redbull', 1)
                    xPlayer.addInventoryItem('vodkaenergy', 1)
                end
            end

        end)
    end

    if _itemValue == 'jusfruit' then
        SetTimeout(10000, function()        

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('fruit').count
			
            if alephQuantity < 2 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('fruit'))

            else
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('fruit', 2)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('jusfruit'))
                    xPlayer.removeInventoryItem('fruit', 2)
                    xPlayer.addInventoryItem('jusfruit', 1)
                end
            end

        end)
    end
	
	if _itemValue == 'rhumcoca' then
        SetTimeout(10000, function()        

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('soda').count
			local bethQuantity     = xPlayer.getInventoryItem('rhum').count

            if alephQuantity < 2 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('soda'))
			elseif bethQuantity < 2 then
				TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('rhum'))

            else
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('soda', 2)
					xPlayer.removeInventoryItem('rhum', 2)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('rhumcoca'))
                    xPlayer.removeInventoryItem('soda', 2)
					xPlayer.removeInventoryItem('rhum', 2)
                    xPlayer.addInventoryItem('rhumcoca', 1)
                end
            end

        end)
    end

	if _itemValue == 'whiskycoca' then
        SetTimeout(10000, function()        

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('soda').count
			local bethQuantity     = xPlayer.getInventoryItem('rhum').count

            if alephQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('soda'))
			elseif bethQuantity < 1 then
				TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('whisky'))

            else
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('soda', 1)
					xPlayer.removeInventoryItem('whisky', 1)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('whiskycoca'))
                    xPlayer.removeInventoryItem('soda', 1)
					xPlayer.removeInventoryItem('rhum', 1)
                    xPlayer.addInventoryItem('whiskycoca', 1)
                end
            end

        end)
    end
	
	if _itemValue == 'jagerbomb' then
        SetTimeout(10000, function()        

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('redbull').count
            local bethQuantity      = xPlayer.getInventoryItem('jager').count
           

            if alephQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('redbull'))
            elseif bethQuantity < 2 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('jager'))
          
            else
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('jager', 2)
                    xPlayer.removeInventoryItem('redbull', 1)

                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('jagerbomb'))
                    xPlayer.removeInventoryItem('jager', 2)
                    xPlayer.removeInventoryItem('redbull', 1)
                    xPlayer.addInventoryItem('jagerbomb', 1)
                end
            end

        end)
    end
	
	
	
    if _itemValue == 'rhumfruit' then
        SetTimeout(10000, function()        

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('fruit').count
            local bethQuantity     = xPlayer.getInventoryItem('rhum').count

            if alephQuantity < 2 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('fruit'))
            elseif bethQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('rhum'))
            else
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('fruit', 2)
                    xPlayer.removeInventoryItem('rhum', 1)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('rhumfruit'))
                    xPlayer.removeInventoryItem('fruit', 2)
                    xPlayer.removeInventoryItem('rhum', 1)
                    xPlayer.addInventoryItem('rhumfruit', 1)
                end
            end

        end)
    end

end)


ESX.RegisterServerCallback('esx_mamasjob:getVaultWeapons', function(source, cb)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_mamas', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    cb(weapons)

  end)

end)

ESX.RegisterServerCallback('esx_mamasjob:addVaultWeapon', function(source, cb, weaponName)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.removeWeapon(weaponName)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_mamas', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    local foundWeapon = false

    for i=1, #weapons, 1 do
      if weapons[i].name == weaponName then
        weapons[i].count = weapons[i].count + 1
        foundWeapon = true
      end
    end

    if not foundWeapon then
      table.insert(weapons, {
        name  = weaponName,
        count = 1
      })
    end

     store.set('weapons', weapons)

     cb()

  end)

end)

ESX.RegisterServerCallback('esx_mamasjob:removeVaultWeapon', function(source, cb, weaponName)

  local xPlayer = ESX.GetPlayerFromId(source)

  xPlayer.addWeapon(weaponName, 1000)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_mamas', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    local foundWeapon = false

    for i=1, #weapons, 1 do
      if weapons[i].name == weaponName then
        weapons[i].count = (weapons[i].count > 0 and weapons[i].count - 1 or 0)
        foundWeapon = true
      end
    end

    if not foundWeapon then
      table.insert(weapons, {
        name  = weaponName,
        count = 0
      })
    end

     store.set('weapons', weapons)

     cb()

  end)

end)

-- raisin
local function HarvestRaisin(source)
  
  SetTimeout(200, function()

    if PlayersHarvestingRaisin[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)

      local raisin = xPlayer.getInventoryItem('raisin')

      if raisin.limit ~= -1 and raisin.count >= raisin.limit then
        TriggerClientEvent('esx:showNotification', source, _U('inv_full'))
      else
        xPlayer.addInventoryItem('raisin', 1)
        HarvestRaisin(source)
      end

    end
  end)
end

RegisterServerEvent('esx_mamasjob:startHarvestRaisin')
AddEventHandler('esx_mamasjob:startHarvestRaisin', function()

  local _source = source

  PlayersHarvestingRaisin[_source] = true

  TriggerClientEvent('esx:showNotification', _source, _U('pickup_in_prog'))

  HarvestRaisin(_source)

end)

RegisterServerEvent('esx_mamasjob:stopHarvestRaisin')
AddEventHandler('esx_mamasjob:stopHarvestRaisin', function()

  local _source = source

  PlayersHarvestingRaisin[_source] = false

end)

local function SellRaisin(source)

  SetTimeout(200, function()

  	local societyAccount = nil

  	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_mamas', function(account)
  		societyAccount = account
  	end)

    if PlayersSellingRaisin[source] == true and societyAccount ~= nil then

      local xPlayer  = ESX.GetPlayerFromId(source)

      local raisinQuantity = xPlayer.getInventoryItem('raisin').count

      if raisinQuantity == 0 then
        TriggerClientEvent('esx:showNotification', source, _U('no_raisin_sale'))
      else
        xPlayer.removeInventoryItem('raisin', 1)
        xPlayer.addMoney(Config.PriceResell.vin)
        societyAccount.addMoney(Config.PriceResell.vin * 2)
        TriggerClientEvent('esx:showNotification', source, _U('sold_one_raisin'))

        SellRaisin(source)
      end

    end
  end)
end

RegisterServerEvent('esx_mamasjob:startSellRaisin')
AddEventHandler('esx_mamasjob:startSellRaisin', function()

  local _source = source

  PlayersSellingRaisin[_source] = true

  TriggerClientEvent('esx:showNotification', _source, _U('sale_in_prog'))

  SellRaisin(_source)

end)

RegisterServerEvent('esx_mamasjob:stopSellRaisin')
AddEventHandler('esx_mamasjob:stopSellRaisin', function()

  local _source = source

  PlayersSellingRaisin[_source] = false

end)

-- citron
local function HarvestCitron(source)
  
  SetTimeout(500, function()

    if PlayersHarvestingCitron[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)

      local citron = xPlayer.getInventoryItem('citron')

      if citron.limit ~= -1 and citron.count >= citron.limit then
        TriggerClientEvent('esx:showNotification', source, _U('inv_full'))
      else
        xPlayer.addInventoryItem('citron', 1)
        HarvestCitron(source)
      end

    end
  end)
end

RegisterServerEvent('esx_mamasjob:startHarvestCitron')
AddEventHandler('esx_mamasjob:startHarvestCitron', function()

  local _source = source

  PlayersHarvestingCitron[_source] = true

  TriggerClientEvent('esx:showNotification', _source, _U('pickup_in_prog'))

  HarvestCitron(_source)

end)

RegisterServerEvent('esx_mamasjob:stopHarvestCitron')
AddEventHandler('esx_mamasjob:stopHarvestCitron', function()

  local _source = source

  PlayersHarvestingCitron[_source] = false

end)

local function SellCitron(source)

  SetTimeout(500, function()

  	local societyAccount = nil

  	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_mamas', function(account)
  		societyAccount = account
  	end)

    if PlayersSellingCitron[source] == true and societyAccount ~= nil then

      local xPlayer  = ESX.GetPlayerFromId(source)

      local citronQuantity = xPlayer.getInventoryItem('citron').count

      if citronQuantity == 0 then
        TriggerClientEvent('esx:showNotification', source, _U('no_citron_sale'))
      else
        xPlayer.removeInventoryItem('citron', 1)
        xPlayer.addMoney(Config.PriceResell.citron)
        societyAccount.addMoney(Config.PriceResell.citron * 2)
        TriggerClientEvent('esx:showNotification', source, _U('sold_one_citron'))

        SellCitron(source)
      end

    end
  end)
end

RegisterServerEvent('esx_mamasjob:startSellCitron')
AddEventHandler('esx_mamasjob:startSellCitron', function()

  local _source = source

  PlayersSellingCitron[_source] = true

  TriggerClientEvent('esx:showNotification', _source, _U('sale_in_prog'))

  SellCitron(_source)

end)

RegisterServerEvent('esx_mamasjob:stopSellCitron')
AddEventHandler('esx_mamasjob:stopSellCitron', function()

  local _source = source

  PlayersSellingCitron[_source] = false

end)

-- fruit
local function HarvestFruit(source)
  
  SetTimeout(750, function()

    if PlayersHarvestingFruit[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)

      local fruit = xPlayer.getInventoryItem('fruit')

      if fruit.limit ~= -1 and fruit.count >= fruit.limit then
        TriggerClientEvent('esx:showNotification', source, _U('inv_full'))
      else
        xPlayer.addInventoryItem('fruit', 1)
        HarvestFruit(source)
      end

    end
  end)
end

RegisterServerEvent('esx_mamasjob:startHarvestFruit')
AddEventHandler('esx_mamasjob:startHarvestFruit', function()

  local _source = source

  PlayersHarvestingFruit[_source] = true

  TriggerClientEvent('esx:showNotification', _source, _U('pickup_in_prog'))

  HarvestFruit(_source)

end)

RegisterServerEvent('esx_mamasjob:stopHarvestFruit')
AddEventHandler('esx_mamasjob:stopHarvestFruit', function()

  local _source = source

  PlayersHarvestingFruit[_source] = false

end)

local function SellFruit(source)

  SetTimeout(750, function()

  	local societyAccount = nil

  	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_mamas', function(account)
  		societyAccount = account
  	end)

    if PlayersSellingFruit[source] == true and societyAccount ~= nil then

      local xPlayer  = ESX.GetPlayerFromId(source)

      local fruitQuantity = xPlayer.getInventoryItem('fruit').count

      if fruitQuantity == 0 then
        TriggerClientEvent('esx:showNotification', source, _U('no_fruit_sale'))
      else
        xPlayer.removeInventoryItem('fruit', 1)
        xPlayer.addMoney(Config.PriceResell.fruit)
        societyAccount.addMoney(Config.PriceResell.fruit * 2)
        TriggerClientEvent('esx:showNotification', source, _U('sold_one_fruit'))

        SellFruit(source)
      end

    end
  end)
end

RegisterServerEvent('esx_mamasjob:startSellFruit')
AddEventHandler('esx_mamasjob:startSellFruit', function()

  local _source = source

  PlayersSellingFruit[_source] = true

  TriggerClientEvent('esx:showNotification', _source, _U('sale_in_prog'))

  SellFruit(_source)

end)

RegisterServerEvent('esx_mamasjob:stopSellFruit')
AddEventHandler('esx_mamasjob:stopSellFruit', function()

  local _source = source

  PlayersSellingFruit[_source] = false

end)

-- RETURN INVENTORY TO CLIENT
RegisterServerEvent('esx_mamasjob:GetUserInventory')
AddEventHandler('esx_mamasjob:GetUserInventory', function(currentZone)
  local _source = source
  local xPlayer  = ESX.GetPlayerFromId(_source)
    TriggerClientEvent('esx_mamasjob:ReturnInventory', 
     _source, 
    xPlayer.getInventoryItem('raisin').count,
    xPlayer.getInventoryItem('citron').count,
    xPlayer.getInventoryItem('fruit').count,  
    currentZone
    )
end)


ESX.RegisterServerCallback('esx_mamasjob:getPlayerInventory', function(source, cb)

  local xPlayer    = ESX.GetPlayerFromId(source)
  local items      = xPlayer.inventory

  cb({
    items      = items
  })

end)
