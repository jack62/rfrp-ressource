Config                            = {}
Config.DrawDistance               = 100.0

Config.Zones = {

    LocationVehicleEntering = {
        Pos = {x = -962.16, y = -2700.25, z = 12.74}, -- Position location de Véhicule
        Size = { x = 2.5, y = 2.5, z= 1.0},
        Color = {r = 0, g = 155, b = 255},
        Type = 1,
    },
    {
        Pos = {x = -905.96, y = -2333.15, z = 5.7}, -- Position location de Véhicule
        Size = { x = 2.5, y = 2.5, z= 1.0},
        Color = {r = 0, g = 155, b = 255},
        Type = 1,
    },
    {
        Pos = {x = 214.68, y = -937.72, z = 23.14}, -- Position location de Véhicule
        Size = { x = 2.5, y = 2.5, z= 1.0},
        Color = {r = 0, g = 155, b = 255},
        Type = 1,
    },
    {
        Pos = {x = 1982.6028, y = 3774.238, z = 31.18}, -- Position location de Véhicule
        Size = { x = 2.5, y = 2.5, z= 1.0},
        Color = {r = 0, g = 155, b = 255},
        Type = 1,
    },
}