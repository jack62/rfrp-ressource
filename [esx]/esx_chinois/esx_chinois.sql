USE `essentialmode`;

CREATE TABLE `chinois` (
  
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  
  PRIMARY KEY (`id`)
);

INSERT INTO `chinois` (name, item, price) VALUES
	('chinois','nems',90),
	('chinois','sake',175),
	('chinois','samoussa',90),
	('chinois','rizthai',110),
	('chinois','rizcantone',110),
	('chinois','rouleauprintemps',80)
;