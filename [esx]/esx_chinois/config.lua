Config = {}

Config.DrawDistance = 100
Config.Size         = {x = 1.5, y = 1.5, z = 1.5}
Config.Color        = {r = 0, g = 128, b = 255}
Config.Type         = 1
Config.Locale = 'fr'

Config.Zones = {

    chinois = {
        Items = {},
        Pos = {
            {x = 1907.170, y = 3710.510, z = 31.743},
			{x = -1311.995, y = -1336.804, z = 3.636},
			{x = -50.045, y = -1058.828, z = 26.805}
        }
    },

}
