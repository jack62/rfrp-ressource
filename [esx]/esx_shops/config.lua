Config = {}

Config.DrawDistance = 100
Config.Size         = {x = 1.5, y = 1.5, z = 1.5}
Config.Color        = {r = 0, g = 128, b = 255}
Config.Type         = 1
Config.Locale = 'fr'

Config.Zones = {

    TwentyFourSeven = {
        Items = {}, -- 100% du prix
        Pos = {
		--	{x = 373.875,   y = 325.896,  z = 102.566},   -- a coté de la grande banque
			{x = 2557.458,  y = 382.282,  z = 107.622},   -- sortie de la ville autoroute EST
			{x = -3038.939, y = 585.954,  z = 6.908},     -- sortie de la ville autoroute OUEST au milieu des 3
			{x = -3241.927, y = 1001.462, z = 11.830},    -- sortie de la ville autoroute OUEST au dessus des 3
			{x = 547.431,   y = 2671.710, z = 41.156},    -- desert plein milieu
			{x = 1961.464,  y = 3740.672, z = 31.343},    -- coiffeur sandy shore
			{x = 2678.916,  y = 3280.671, z = 54.241},    -- a coté de la mine
			{x = 1729.216,  y = 6414.131, z = 34.037},    -- au nord du mont chiliad
        }
    },

    RobsLiquor = {
        Items = {}, -- 125% du prix
        Pos = {
		--	{x = 1720.887,   y = 2505.687,  z = -78.031}, -- point de merde ?
		--	{x = -1222.915, y = -906.983,  z = 11.326},   -- près du magasin de masque
		--	{x = -1487.553, y = -379.107,  z = 39.163},   -- coin NORD OUEST de la ville
		--	{x = -2968.243, y = 390.910,   z = 14.043},   -- sortie de la ville autoroute OUEST en dessous des 3 
			{x = 1166.024,  y = 2708.930,  z = 37.157},   -- desert plein milieu, contre le magasin de vetement
			{x = 1392.562,  y = 3604.684,  z = 33.980},   -- OUEST de sandy shore
			{x = -976.599,  y = -2700.347, z = 12.863}    -- aéroport
        }
    },

    LTDgasoline = {
        Items = {}, -- 150% du prix
        Pos = {
			{x = -48.519,   y = -1757.514, z = 28.421},   -- près du bureau de vote
		--	{x = 1163.373,  y = -323.801,  z = 68.205},   -- a coté des taxi
		--	{x = -1820.523, y = 792.518,   z = 137.118},  -- vine hood
			{x = 1698.388,  y = 4924.404,  z = 41.063}    -- au dessus du lac
        }
    }
}
