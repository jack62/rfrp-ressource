Config              = {}
Config.MarkerType   =1
Config.DrawDistance = 100.0
Config.ZoneSize     = {x = 3.0, y = 3.0, z = 3.0}
--Config.MarkerColor  = {r = 200, g = 104, b = 100}
Config.RequiredCopsCodeine = 3
Config.RequiredCopsEssence = 3
Config.RequiredCopsDisolvant = 3
Config.RequiredCopsPhosphorerouge = 3
Config.RequiredCopsHeroine = 3
Config.RequiredCopsKroko = 2
Config.Locale = 'fr'

-- Change coordinates !!!
Config.Zones = {
	CodeineFarm = 		 {x=206.662,  y=-1851.518,  z=26.581},
	EssenceFarm = 		 {x=1710.847,  y=4938.267,  z=41.18},
	DisolvantFarm = 	 {x=-2948.043,  y=438.957,    z=14.355},
	PhosphorerougeFarm = {x=-1812.055,  y=-1205.367,  z=18.269},
	HeroineFarm =    	 {x=591.962,	  y=2782.575, z=42.518},
	IodeFarm =    		 {x=3091.909, y=6031.378,  z=122.297},
	KrokoTreatment =	 {x=753.063,   y=-3193.485,  z=5.173},
	KrokoResell =  		 {x=474.257,  y=-635.709,   z=24.748}
}
