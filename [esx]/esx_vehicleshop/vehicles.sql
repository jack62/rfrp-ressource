-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 07 Novembre 2018 à 20:32
-- Version du serveur :  5.7.23-0ubuntu0.16.04.1
-- Version de PHP :  7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `essentialmode`
--

-- --------------------------------------------------------

--
-- Structure de la table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `model` varchar(60) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `vehicles`
--

INSERT INTO `vehicles` (`id`, `name`, `model`, `price`, `category`) VALUES
(1, 'Blade', 'blade', 45000, 'muscle'),
(2, 'Buccaneer', 'buccaneer', 50000, 'muscle'),
(3, 'Buccaneer Rider', 'buccaneer2', 55000, 'muscle'),
(4, 'Chino', 'chino', 40000, 'muscle'),
(5, 'Chino Luxe', 'chino2', 60000, 'muscle'),
(6, 'Coquette BlackFin', 'coquette3', 130000, 'muscle'),
(7, 'Dominator', 'dominator', 55000, 'muscle'),
(8, 'Dukes', 'dukes', 60000, 'muscle'),
(9, 'Gauntlet', 'gauntlet', 50000, 'muscle'),
(10, 'Hotknife', 'hotknife', 150000, 'muscle'),
(11, 'faction', 'faction', 35000, 'muscle'),
(12, 'Faction Rider', 'faction2', 40000, 'muscle'),
(13, 'Faction XL', 'faction3', 100000, 'muscle'),
(14, 'Nightshade', 'nightshade', 100000, 'muscle'),
(15, 'Phoenix', 'phoenix', 50000, 'muscle'),
(16, 'Picador', 'picador', 30000, 'muscle'),
(17, 'Sabre Turbo', 'sabregt', 55000, 'muscle'),
(18, 'Sabre GT', 'sabregt2', 70000, 'muscle'),
(19, 'Slam Van', 'slamvan3', 90000, 'muscle'),
(20, 'Tampa', 'tampa', 50000, 'muscle'),
(21, 'Virgo', 'virgo', 50000, 'muscle'),
(22, 'Vigero', 'vigero', 50000, 'muscle'),
(23, 'Voodoo', 'voodoo', 35000, 'muscle'),
(25, 'Brioso R/A', 'brioso', 10000, 'compacts'),
(26, 'Issi', 'issi2', 25000, 'compacts'),
(27, 'Panto', 'panto', 9000, 'compacts'),
(28, 'Prairie', 'prairie', 35000, 'compacts'),
(29, 'Bison', 'bison', 75000, 'vans'),
(30, 'Bobcat XL', 'bobcatxl', 90000, 'vans'),
(31, 'Burrito', 'burrito3', 40000, 'vans'),
(32, 'Burrito', 'gburrito2', 50000, 'vans'),
(33, 'Camper', 'camper', 35000, 'vans'),
(34, 'Gang Burrito', 'gburrito', 30000, 'vans'),
(35, 'Journey', 'journey', 25000, 'vans'),
(36, 'Minivan', 'minivan', 55000, 'vans'),
(37, 'Moonbeam', 'moonbeam', 30000, 'vans'),
(38, 'Moonbeam Rider', 'moonbeam2', 40000, 'vans'),
(39, 'Paradise', 'paradise', 30000, 'vans'),
(40, 'Rumpo', 'rumpo', 40000, 'vans'),
(41, 'Rumpo Trail', 'rumpo3', 50000, 'vans'),
(42, 'Surfer', 'surfer', 30000, 'vans'),
(43, 'Youga', 'youga', 40000, 'vans'),
(44, 'Youga Luxuary', 'youga2', 55000, 'vans'),
(45, 'Asea', 'asea', 35000, 'sedans'),
(46, 'Cognoscenti', 'cognoscenti', 65000, 'sedans'),
(47, 'Emperor', 'emperor', 25000, 'sedans'),
(48, 'Fugitive', 'fugitive', 60000, 'sedans'),
(49, 'Glendale', 'glendale', 25000, 'sedans'),
(50, 'Intruder', 'intruder', 40000, 'sedans'),
(51, 'Premier', 'premier', 30000, 'sedans'),
(52, 'Primo Custom', 'primo2', 45000, 'sedans'),
(53, 'Regina', 'regina', 7500, 'sedans'),
(54, 'Schafter', 'schafter2', 60000, 'sedans'),
(55, 'Stretch', 'stretch', 250000, 'sedans'),
(56, 'Super Diamond', 'superd', 180000, 'sedans'),
(57, 'Tailgater', 'tailgater', 65000, 'sedans'),
(58, 'Warrener', 'warrener', 30000, 'sedans'),
(59, 'Washington', 'washington', 35000, 'sedans'),
(60, 'Baller', 'baller2', 65000, 'suvs'),
(61, 'Baller Sport', 'baller3', 80000, 'suvs'),
(62, 'Cavalcade', 'cavalcade2', 90000, 'suvs'),
(63, 'Contender', 'contender', 60000, 'suvs'),
(64, 'Dubsta', 'dubsta', 55999, 'suvs'),
(65, 'Dubsta Luxuary', 'dubsta2', 65000, 'suvs'),
(66, 'Fhantom', 'fq2', 50999, 'suvs'),
(67, 'Grabger', 'granger', 49000, 'suvs'),
(68, 'Gresley', 'gresley', 78000, 'suvs'),
(69, 'Huntley S', 'huntley', 65000, 'suvs'),
(70, 'Landstalker', 'landstalker', 60000, 'suvs'),
(71, 'Mesa', 'mesa', 50000, 'suvs'),
(72, 'Mesa Trail', 'mesa3', 65000, 'suvs'),
(73, 'Patriot', 'patriot', 85000, 'suvs'),
(74, 'Radius', 'radi', 60000, 'suvs'),
(75, 'Rocoto', 'rocoto', 80000, 'suvs'),
(76, 'Seminole', 'seminole', 65000, 'suvs'),
(77, 'XLS', 'xls', 49000, 'suvs'),
(78, 'Btype', 'btype', 80000, 'sportsclassics'),
(79, 'Btype Luxe', 'btype3', 100000, 'sportsclassics'),
(80, 'Btype Hotroad', 'btype2', 180000, 'sportsclassics'),
(81, 'Casco', 'casco', 90000, 'sportsclassics'),
(82, 'Coquette Classic', 'coquette2', 150000, 'sportsclassics'),
(83, 'Manana', 'manana', 100000, 'sportsclassics'),
(84, 'Monroe', 'monroe', 150000, 'sportsclassics'),
(85, 'Pigalle', 'pigalle', 140000, 'sportsclassics'),
(86, 'Stinger', 'stinger', 240000, 'sportsclassics'),
(87, 'Stinger GT', 'stingergt', 150000, 'sportsclassics'),
(88, 'Stirling GT', 'feltzer3', 170000, 'sportsclassics'),
(89, 'Z-Type', 'ztype', 280000, 'sportsclassics'),
(90, 'Bifta', 'bifta', 90000, 'offroad'),
(91, 'Bf Injection', 'bfinjection', 40000, 'offroad'),
(92, 'Blazer', 'blazer', 15000, 'offroad'),
(93, 'Blazer Sport', 'blazer4', 25000, 'offroad'),
(94, 'Brawler', 'brawler', 120000, 'offroad'),
(95, 'Bubsta 6x6', 'dubsta3', 250000, 'offroad'),
(96, 'Dune Buggy', 'dune', 65000, 'offroad'),
(97, 'Guardian', 'guardian', 180000, 'offroad'),
(98, 'Rebel', 'rebel2', 110000, 'offroad'),
(99, 'Sandking', 'sandking', 150000, 'offroad'),
(100, 'The Liberator', 'monster', 2000000, 'offroad'),
(101, 'Trophy Truck', 'trophytruck', 180000, 'offroad'),
(102, 'Trophy Truck Limited', 'trophytruck2', 240000, 'offroad'),
(103, 'Cognoscenti Cabrio', 'cogcabrio', 90000, 'coupes'),
(104, 'Exemplar', 'exemplar', 90000, 'coupes'),
(105, 'F620', 'f620', 80000, 'coupes'),
(106, 'Felon', 'felon', 50000, 'coupes'),
(107, 'Felon GT', 'felon2', 80000, 'coupes'),
(108, 'Jackal', 'jackal', 85000, 'coupes'),
(109, 'Oracle XS', 'oracle2', 95000, 'coupes'),
(110, 'Sentinel', 'sentinel', 80000, 'coupes'),
(111, 'Sentinel XS', 'sentinel2', 35000, 'coupes'),
(112, 'Windsor', 'windsor', 50000, 'coupes'),
(113, 'Windsor Drop', 'windsor2', 50000, 'coupes'),
(114, 'Zion', 'zion', 80000, 'coupes'),
(115, 'Zion Cabrio', 'zion2', 95000, 'coupes'),
(116, '9F', 'ninef', 90000, 'sports'),
(117, '9F Cabrio', 'ninef2', 50000, 'sports'),
(118, 'Alpha', 'alpha', 60000, 'sports'),
(119, 'Banshee', 'banshee', 120000, 'sports'),
(120, 'Bestia GTS', 'bestiagts', 100000, 'sports'),
(121, 'Buffalo', 'buffalo', 85000, 'sports'),
(122, 'Buffalo S', 'buffalo2', 175000, 'sports'),
(123, 'Carbonizzare', 'carbonizzare', 175000, 'sports'),
(124, 'Comet', 'comet2', 100000, 'sports'),
(125, 'Coquette', 'coquette', 85000, 'sports'),
(126, 'Drift Tampa', 'tampa2', 200000, 'sports'),
(127, 'Elegy', 'elegy2', 35000, 'sports'),
(128, 'Feltzer', 'feltzer2', 45000, 'sports'),
(129, 'Furore GT', 'furoregt', 65000, 'sports'),
(130, 'Fusilade', 'fusilade', 40000, 'sports'),
(131, 'Jester', 'jester', 65000, 'sports'),
(132, 'Jester(Racecar)', 'jester2', 850000, 'sports'),
(133, 'Khamelion', 'khamelion', 185000, 'sports'),
(134, 'Kuruma', 'kuruma', 35000, 'sports'),
(135, 'Lynx', 'lynx', 59000, 'sports'),
(136, 'Mamba', 'mamba', 80000, 'sports'),
(137, 'Massacro', 'massacro', 75000, 'sports'),
(138, 'Massacro(Racecar)', 'massacro2', 90000, 'sports'),
(139, 'Omnis', 'omnis', 95000, 'sports'),
(140, 'Penumbra', 'penumbra', 85000, 'sports'),
(141, 'Rapid GT', 'rapidgt', 120000, 'sports'),
(142, 'Rapid GT Convertible', 'rapidgt2', 150000, 'sports'),
(143, 'Schafter V12', 'schafter3', 95000, 'sports'),
(144, 'Seven 70', 'seven70', 130000, 'sports'),
(145, 'Sultan', 'sultan', 30000, 'sports'),
(146, 'Surano', 'surano', 100000, 'sports'),
(147, 'Tropos', 'tropos', 150000, 'sports'),
(148, 'Verlierer', 'verlierer2', 150000, 'sports'),
(149, 'Adder', 'adder', 250000, 'super'),
(150, 'Banshee 900R', 'banshee2', 450000, 'super'),
(151, 'Bullet', 'bullet', 700000, 'super'),
(152, 'Cheetah', 'cheetah', 1200000, 'super'),
(153, 'Entity XF', 'entityxf', 1200000, 'super'),
(154, 'ETR1', 'sheava', 1000000, 'super'),
(155, 'FMJ', 'fmj', 1500000, 'super'),
(156, 'Infernus', 'infernus', 245000, 'super'),
(157, 'Osiris', 'osiris', 1200000, 'super'),
(158, 'Pfister', 'pfister811', 900000, 'super'),
(159, 'RE-7B', 'le7b', 2000000, 'super'),
(160, 'Reaper', 'reaper', 1600000, 'super'),
(161, 'Sultan RS', 'sultanrs', 600000, 'super'),
(162, 'T20', 't20', 950000, 'super'),
(163, 'Turismo R', 'turismor', 1100000, 'super'),
(164, 'Tyrus', 'tyrus', 800000, 'super'),
(165, 'Vacca', 'vacca', 245000, 'super'),
(166, 'Voltic', 'voltic', 1200000, 'super'),
(167, 'X80 Proto', 'prototipo', 2800000, 'super'),
(168, 'Zentorno', 'zentorno', 1800000, 'super'),
(169, 'Akuma', 'AKUMA', 10000, 'motorcycles'),
(170, 'Avarus', 'avarus', 14000, 'motorcycles'),
(171, 'Bagger', 'bagger', 13000, 'motorcycles'),
(172, 'Bati 801', 'bati', 35000, 'motorcycles'),
(173, 'Bati 801RR', 'bati2', 65000, 'motorcycles'),
(174, 'BF400', 'bf400', 25000, 'motorcycles'),
(175, 'BMX (velo)', 'bmx', 110, 'motorcycles'),
(176, 'Carbon RS', 'carbonrs', 28000, 'motorcycles'),
(177, 'Chimera', 'chimera', 35000, 'motorcycles'),
(178, 'Cliffhanger', 'cliffhanger', 35000, 'motorcycles'),
(179, 'Cruiser (velo)', 'cruiser', 250, 'motorcycles'),
(180, 'Daemon', 'daemon', 150000, 'motorcycles'),
(181, 'Daemon High', 'daemon2', 65000, 'motorcycles'),
(182, 'Defiler', 'defiler', 120000, 'motorcycles'),
(183, 'Double T', 'double', 14000, 'motorcycles'),
(184, 'Enduro', 'enduro', 2800, 'motorcycles'),
(185, 'Esskey', 'esskey', 20000, 'motorcycles'),
(186, 'Faggio', 'faggio', 850, 'motorcycles'),
(188, 'Fixter (velo)', 'fixter', 450, 'motorcycles'),
(189, 'Gargoyle', 'gargoyle', 12500, 'motorcycles'),
(190, 'Hakuchou', 'hakuchou', 19500, 'motorcycles'),
(191, 'Hakuchou Sport', 'hakuchou2', 22000, 'motorcycles'),
(192, 'Hexer', 'hexer', 90000, 'motorcycles'),
(193, 'Innovation', 'innovation', 33500, 'motorcycles'),
(194, 'Manchez', 'manchez', 2500, 'motorcycles'),
(195, 'Nemesis', 'nemesis', 12000, 'motorcycles'),
(196, 'Nightblade', 'nightblade', 19000, 'motorcycles'),
(197, 'PCJ-600', 'pcj', 9000, 'motorcycles'),
(198, 'Ruffian', 'ruffian', 9500, 'motorcycles'),
(199, 'Sanchez', 'sanchez', 1500, 'motorcycles'),
(200, 'Sanchez Sport', 'sanchez2', 1600, 'motorcycles'),
(201, 'Sanctus', 'sanctus', 22000, 'motorcycles'),
(202, 'Scorcher (velo)', 'scorcher', 130, 'motorcycles'),
(203, 'Sovereign', 'sovereign', 19900, 'motorcycles'),
(204, 'Shotaro Concept', 'shotaro', 19000, 'motorcycles'),
(206, 'Tri bike (velo)', 'tribike3', 180, 'motorcycles'),
(207, 'Vader', 'vader', 18000, 'motorcycles'),
(208, 'Vortex', 'vortex', 17000, 'motorcycles'),
(209, 'Woflsbane', 'wolfsbane', 11000, 'motorcycles'),
(210, 'Zombie', 'zombiea', 15000, 'motorcycles'),
(211, 'Zombie Luxuary', 'zombieb', 18000, 'motorcycles'),
(212, 'Peugeot 208', '208', 35000, 'import'),
(214, 'Audi R8', 'r8ppi', 143000, 'import'),
(219, 'Renault Kangoo', 'kangoo', 18000, 'import'),
(222, 'Citroën 2CV', 'cit2cv', 30000, 'import'),
(223, 'Citroën C4', 'citroc4', 22000, 'import'),
(226, 'Porsche 911 Turbo S', '911turbos', 250000, 'import'),
(228, 'Porsche Panamera Turbo', 'pturismo', 275000, 'import'),
(229, 'Porsche 911 GTRS', '911gtrs', 350000, 'import'),
(230, 'Mercedes AMG A45', 'a45', 100000, 'import'),
(231, 'Mercedes AMG CLA45sb', 'cla45sb', 180000, 'import'),
(232, 'Mercedes AMG G65', 'g65amg', 120000, 'import'),
(233, 'Mercedes AMG S63C', 'mers63c', 190000, 'import'),
(235, 'Mercedes AMG SLS', 'sls', 275000, 'import'),
(236, 'BMW M4', 'f82', 90000, 'import'),
(237, 'BMW I8', 'I8', 142000, 'import'),
(240, 'BMW X6', 'x6m', 73000, 'import'),
(241, 'BMW M6', 'M6F13', 147000, 'import'),
(244, 'Ferrari F458', 'f458', 190000, 'import'),
(245, 'Ferrari F', 'fct', 120000, 'import'),
(246, 'Golf GTI (E)', 'MK7', 45000, 'import'),
(247, 'Bentley', 'ben17', 350000, 'import'),
(248, 'Bugatti', 'chiron', 3500000, 'import'),
(249, 'Nissan GTR', 'gtr', 70000, 'import'),
(250, 'Lamborghini', 'huraperfospy', 350000, 'import'),
(251, 'Lamborghini Ip700', 'lp700r', 450000, 'import'),
(254, 'KTM', 'ktmsm', 2500, 'import'),
(257, 'Spitro', 'spitro', 1200, 'import'),
(258, 'Tmax', 'tmax', 7500, 'import'),
(259, 'Yamaha 400 YZF', 'yzf', 1900, 'import'),
(260, 'Yamaha GYTR', 'yzfsm', 4500, 'import'),
(266, 'Rolls Royce', 'rrphantom', 650000, 'import'),
(267, 'Mercedes Class S', 's65amg', 17500, 'import'),
(269, 'HUDSON HORNET 1952', 'HUDHORC', 45000, 'import'),
(270, 'LaFerrari Aperta', 'aperta', 350000, 'import');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=273;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
