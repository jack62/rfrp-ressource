Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = false
Config.MaxInService               = -1
Config.Locale                     = 'fr'

Config.BallasStations = {

  Ballas = {

    Blip = {
--      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
	  { name = 'WEAPON_BAT',              price = 150 },
      { name = 'WEAPON_BALL',       	  price = 10 },
      --{ name = 'WEAPON_COMBATPISTOL',     price = 2000 },
      { name = 'WEAPON_MOLOTOV',       	  price = 800 },
      --{ name = 'WEAPON_FIREWORK',         price = 2500 },
    },

	  AuthorizedVehicles = {
		  { name = 'Granger',  		label = '4x4' },
		  { name = 'golf6',         label = 'voiture de la téci' },
		  { name = 'kx450f',   		label = 'kx540f' },
		  { name = 'bmx',        	label = 'bmx' },
	  },

    Cloakrooms = {
      { x = -811.603, y = 175.121, z = 76.745},
    },

    Armories = {
      { x = -1.859, y = -1821.205, z = 29.543},
    },

    Vehicles = {
      {
        Spawner    = { x = 78.402, y = -1974.705, z = 20.912 },
        SpawnPoint = { x = 85.587, y = -1971.873, z = 20.495 },
        Heading    = 322.788,
      }
    },

    Helicopters = {
      {
        Spawner    = { x = 20.312, y = 535.667, z = 173.627 },
        SpawnPoint = { x = 3.40, y = 525.56, z = 177.919 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = 85.275, y = -1971.347, z = 20.747 },
    },

    BossActions = {
      { x = -3.296, y = -1821.914, z = 29.543 },
    },

  },

}