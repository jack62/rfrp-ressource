INSERT INTO `addon_account` (name, label, shared) VALUES 
	('society_ballas','Ballas',1)
;

INSERT INTO `datastore` (name, label, shared) VALUES 
	('society_ballas','Ballas',1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES 
	('society_ballas', 'Ballas', 1)
;

INSERT INTO `jobs` (`name`, `label`, `whitelisted`) VALUES
	('ballas', 'Ballas', 1);
;

INSERT INTO `job_grades` (`job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
('ballas', 0, 'soldat', 'Petite Frappe', 0, '{}', '{}'),
('ballas', 1, 'soldato', 'Guetteur', 0, '{}', '{}'),
('ballas', 2, 'capo', 'Dealeur', 0, '{}', '{}'),
('ballas', 3, 'consigliere', 'Soldat', 0, '{}', '{}'),
('ballas', 4, 'boss', 'Chef', 0, '{}', '{}');