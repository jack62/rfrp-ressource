Config              = {}
Config.DrawDistance = 100.0
Config.ZoneSize     = {x = 2.0, y = 2.0, z = 2.0}
Config.MarkerColor  = {r = 0, g = 255, b = 0}
Config.MarkerType   = 27
Config.Locale = 'en'

Config.Zones = {
	{x = -268.8589, y = -956.7532, z = 30.223}
}

Config.Webhook = {
	police = "https://discordapp.com/api/webhooks/507338208212942849/9bMJO9s3nCQBomQf2EyxLa_CDHhyHUIayJk73VdGDdx1_0wUkfA8ScRkaerYnT1Kecc1",
	ambulance = "https://discordapp.com/api/webhooks/507338692092887040/A6aLiT6V84--_DxbYkJMD-KErG02ciB8JnUwsMAjPcbe3lkkbEA8qpKoN4OhAbHJF7WQ",
	journalist = "journalist",
	cardealer = "cardealer",
	mechanic = "https://discordapp.com/api/webhooks/507339181018841111/9gYu2qQjpDCbVYhhxoNTFuMq78Bw-mzEb49IR3Mac0VUNIut7r_4Sg9DHUbrxPmeZNPK",
	realestate = "https://discordapp.com/api/webhooks/507339576017289227/bVFINk908nP3o3nmC37y3fZpuKVF5Nm_BdTygQkC74pIguAsDTm5wIOibQRuWz4jas2b",
	taxi = "https://discordapp.com/api/webhooks/507339707680817162/xrcgTbAKK-zoDd7uXlam5pBdvsJeIcL8t90F8YcJYZ0bhbnZlMKav9nIwrqtFxi1KGjS",
	delivery = "delivery",
	poste = "https://discordapp.com/api/webhooks/507339272802533398/NtdqZ8mBxGX3RJ70oK4vEtQjBdb5Ay2LrPaA2rDWqdXqCC_MigLGbaPS-W6qiNhvlWzM",
	banker = "https://discordapp.com/api/webhooks/507339101184327690/Gu7CyzAtmZE9zwN3CRqyTuNCB6dbwVt52EQrbSoRsrWP-amJTETBPPLzUjfTFEoDazsg",
	epicerie = "https://discordapp.com/api/webhooks/507339379149373461/9u8vgmOdHV2lmaxrOXiB7AS1nrquZMY7FCvnJB4AEsFPXWr371NQu8IHuvbwlcW-tp_P",
	vigne = "https://discordapp.com/api/webhooks/507339642920763412/VsmnFfPgBHA2ZaM5wFP2Ige013191ppPqtR7txbeJZ9fVi58I4mrEARO7HVmh45ggKr6",
	bahamas = "https://discordapp.com/api/webhooks/507339439773712394/f8WuQ7I8mUPxFW6yisZa8At-w29wvBMYvonEEiX8wV9VkcfEooNoJy7L_VchR0FX-YLj",
	unicorn = "https://discordapp.com/api/webhooks/507339504181313545/lAjYwE4eVkg59z1OsbBOljiSkkjEU2Q1guleYj4shrcfTg7-nl6pJP97Z_O8m4zzUOdb",
	lawler = "https://discordapp.com/api/webhooks/507341104199499778/DgrFzv8cWUhrJAR4z67vQfr_k3xEvFchIeOJaSy8nso7sd-Zj1ldgui7txqZRiIA9Ntc",
	avocat = "https://discordapp.com/api/webhooks/507353384060452864/XOlHRErU4iVVyxCDNe5nEk_jEFgNGjUVxUeB7zvg5FfIIGYNbk-vdDzCA65IEPisNSt2",
}