Config                      = {}
Config.DrawDistance         = 100.0
Config.MenuDistance         = 2.0
Config.MarkerType           = 1
Config.MarkerSize           = {x = 4.0, y = 4.0, z = 4.0}
Config.MarkerColor          = {r = 255, g = 255, b = 255}
Config.ParkingMarkerSize    = {x = 4.0, y = 4.0, z = 4.0}
Config.ParkingMarkerColor   = {r = 120, g = 120, b = 240}
Config.ZDiff                = 5.0
Config.MinimumHealthPercent = 0

Config.Locale = 'fr'

Config.Zones = {}

Config.Garages = {

	MiltonDrive = {
		Marker 	= { x = -287.3333740234, y = -2593.3742675781, z = 5.59148},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},

	DelPerro = {
		Marker  = { x = -705.21, y = -974.11, z = 19.19},
		Size  	=  { x = 1.5, y = 1.5, z = 1.0 },
	},

  Prosperity = {
		Marker 	= { x = -1055.72814941406, y = -2030.4049072266, z = 12.5},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},

  RayLowenstein = {
		Marker 	= { x = 1210.51, y = -1262.58, z = 34.83},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},

  Square = {
		Marker 	= { x = -386.98, y = -123.88, z = 37.68},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},

  VinewoodBlvd = {
		Marker 	= { x = 451.92, y = -997.45, z = 25.0},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},

	SandyShores = {
		Marker 	= { x = -1432.92, y = -666.45, z = 26.0},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},
	
	MesCouilles = {
		Marker 	= { x = 367.6, y = -1453.82, z = 28.0},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},
	
	Mineurs = {
		Marker 	= { x = 875.25, y = -2177.46, z = 29.5},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},
	
		AgentImmo = {
		Marker 	= { x = -104.87, y = -610.06, z = 35.07},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},
	
	
	Paradise = {
		Marker = { x = 259.55, y = -1162.19, z = 28.19},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},
	
	Casino = {
		Marker = { x = 909.45, y = 47.23, z = 79.76},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},
	
	Ballas = {
		Marker = { x = 7.1, y = -1835.77, z = 23.81},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},
	
	Mecano = {
		Marker = { x = -190.76, y = -1290.15, z = 30.3},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},
	
	Eboueurs  = {
		Marker = { x = -427.17, y = -1693.4, z = 18.03},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},
	
	Textil  = {
		Marker = { x = 706.92, y = -980.16, z = 23.13},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},
	
	Hotel  = {
		Marker = { x = -1392.43, y = 80.23, z = 52.85},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},
	
	Redwood  = {
		Marker = { x = 2333.15, y = 3131.94, z = 47.18},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},
	
	peche  = {
		Marker = { x = -3236.15, y = 971.94, z = 12.18},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},
	
	
	Paintball  = {
		Marker = { x = 1855.4, y = 2578.73, z = 44.25},
		Size  	= { x = 1.5, y = 1.5, z = 1.0 },
	},
	
	
}