Config = {}

Config.Ped = {

  {label='Stripteaseuse 1',type=5, hash='s_f_y_stripper_01'},
  {label='Stripteaseuse 2',type=5, hash='s_f_y_stripper_02'},
  {label='Stripteaseuse Fat',type=5, hash='a_f_m_fatcult_01'},
  {label='Stripteaseurs 1',type=5, hash='a_m_y_musclbeac_01'},
  {label='Stripteaseurs SM',type=5, hash='a_m_m_acult_01'},
}

Config.Salle = {
  scene1 = {
    label = 'Podium droite',
    pos = {x=-1596.16, y=-3007.74, z=-78.21, a=177.43},
  },
  scene2 = {
    label = 'Podium gauche',
    pos = {x=104.6779, y=-1295.2697, z=29.258, a=297.289},
  },
  scene3 = {
    label = 'Fond scene centre',
    pos = {x=104.0453, y=-1292.199, z=29.258, a=298.912},
  },
  scene4 = {
    label = 'scene centre',
    pos = {x=107.359, y=-1290.2869, z=28.8587, a=297.33},
  },
  scene5 = {
    label = 'scene avant gauche',
    pos = {x=112.0371 ,y=-1286.2375, z=28.4586, a=30.04},
  },
  scene6 = {
    label = 'scene avant droit',
    pos = {x=113.205, y=-1288.293, z=28.4586, a=211.88},
  }
}

Config.Dict = {
  show1={
    label = 'Lap dance 1',
    name = 'mini@strip_club@lap_dance@ld_girl_a_song_a_p1',
    anim ='ld_girl_a_song_a_p1_f',
  },
  show2={
    label = 'Lap dance 2',
    name = 'mini@strip_club@lap_dance@ld_girl_a_song_a_p2',
    anim ='ld_girl_a_song_a_p2_f',
  },
  show3={
    label = 'Lap dance 3',
    name = 'mini@strip_club@lap_dance@ld_girl_a_song_a_p3',
    anim ='ld_girl_a_song_a_p3_f',
  },
  show4={
    label = 'pole_dance',
    name = 'mini@strip_club@pole_dance@pole_dance1',
    anim ='pd_dance_0',
  },
}
Config.Zones = {

      Pos = { x = -1596.04, y = -3005.42, z = -79.01},
}
