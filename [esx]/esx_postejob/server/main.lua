ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
TriggerEvent('esx_society:registerSociety', 'poste', 'Poste', 'society_poste', 'society_poste', 'society_poste', {type = 'private'})



local function payentreprise()

  	local societyAccount = nil

  	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_poste', function(account)
  		societyAccount = account
  	end)
        societyAccount.addMoney(700)
    end


RegisterServerEvent('esx_postejob:pay')
AddEventHandler('esx_postejob:pay', function(amount)

	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	xPlayer.addMoney(tonumber(amount))
	payentreprise()
end)

local function payentreprise()

  	local societyAccount = nil

  	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_poste', function(account)
  		societyAccount = account
  	end)
        societyAccount.addMoney(700)
    end

    
 
ESX.RegisterServerCallback('esx_postejob:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_poste', function(inventory)
    cb(inventory.items)
  end)

end)

RegisterServerEvent('esx_postejob:putStockItems')
AddEventHandler('esx_postejob:putStockItems', function(itemName, count)

	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_poste', function(inventory)

		local item = inventory.getItem(itemName)

		if item.count >= 0 then
			xPlayer.removeInventoryItem(itemName, count)
			inventory.addItem(itemName, count)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
		end

		TriggerClientEvent('esx:showNotification', xPlayer.source, _U('added') .. count .. ' ' .. item.label)

	end)
end)

ESX.RegisterServerCallback('esx_postejob:getPlayerInventory', function(source, cb)

	local xPlayer    = ESX.GetPlayerFromId(source)
	local items      = xPlayer.inventory

	cb({
		items      = items
	})

end)