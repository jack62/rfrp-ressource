Config              = {}
Config.DrawDistance = 100.0
Config.MaxDelivery	= 10
Config.vehiclePrice	= 1500
Config.MaxInService               = -1
Config.EnablePlayerManagement     = true
Config.EnableSocietyOwnedVehicles = false
Config.Locale       = 'fr'

Config.vehicles = {
	"blista",
		
}

Config.Cloakroom = {
	CloakRoom = {
			Pos   = {x = -1320.87, y = -246.93, z = 41.44},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Color = {r = 0, g = 0, b = 0},
			Type  = 1
		},
}

Config.Zones = {
	VehicleSpawner = {
			Pos   = {x = -1325.22, y = -239.89, z = 41.63},
			Size  = {x = 3.0, y = 3.0, z = 0.5},
			Color = {r = 0, g = 0, b = 0},
			Type  = 1
		},

	VehicleSpawnPoint = {
			Pos   = {x = -1327.67, y = -214.56, z = 42.32},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Type  = -1
		},
	
	VehicleSpawner2 = {
		Pos   = {x = -1324.653, y = -221.071, z = 42.985},
		Size = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Garage véhicule",
		Type  = 0
	},
	
	PosteActions = {
		Pos   = {x = -1333.372, y = -229.8010, z = 42.387},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Point d'action",
		Type  = 0
	 },
}

Config.Livraison = {
-------------------------------------------Los Santos
	--Delivery1LS = {
	--		Pos   = {x = -243.9425, y = 6228.9345, z = 30.5},
	--		Color = {r = 204, g = 204, b = 0},
	--		Size  = {x = 5.0, y = 5.0, z = 3.0},
	--		Color = {r = 204, g = 204, b = 0},
	--		Type  = 1,
	--		Paye = 500
	--	},
	Delivery2LS = {
			Pos   = {x = -1573.8420, y = 413.6582, z = 108.1},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 250
		},
	Delivery3LS = {
			Pos   = {x = 1597.6204, y = 3610.9853, z = 34.1},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 300
		},
	Delivery4LS = {
			Pos   = {x = -1117.36, y = 298.24, z = 65.62},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 264
		},
	Delivery5LS = {
			Pos   = {x =-1574.5502, y = 418.6790, z = 108.1},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 478
		},
	Delivery6LS = {
			Pos   = {x = -231.3688, y = 6242.4750, z = 30.2},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 260
		},
	Delivery7LS = {
			Pos   = {x = 1606.8367, y = 3596.0295, z = 34.1},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 180
		},
	Delivery8LS = {
			Pos   = {x = 491.70, y = -1025.42, z = 26.80},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 45
		},
	Delivery9LS = {
			Pos   = {x = 305.10, y = -1451.77, z = 28.62},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 496
		},
	Delivery10LS = {
			Pos   = {x = -251.85, y = 6135.86, z = 29.81},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 150
		},
	Delivery11LS = {
			Pos   = {x = -355.09, y = -114.71, z = 37.35},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 163
		},
	Delivery12LS = {
			Pos   = {x = 902.18, y = -186.29, z = 72.51},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 200
		},
	Delivery13LS = {
			Pos   = {x = -912.39, y = -799.27, z = 16.53},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 256
		},
	Delivery14LS = {
			Pos   = {x = -19.49, y = -1106.68, z = 25.32},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 220
		},
	Delivery15LS = {
			Pos   = {x = 709.83, y = -1077.47, z = 21.02},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 410
		},
------------------------------------------- Blaine County
	Delivery1BC = {
			Pos   = {x = -243.9425, y = 6228.9345, z = 30.5},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 360
		},
	Delivery2BC = {
			Pos   = {x = -1573.8420, y = 413.6582, z = 108.1},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 325
		},
	Delivery3BC = {
			Pos   = {x = 1597.6204, y = 3610.9853, z = 34.1},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 254
		},
	Delivery4BC = {
			Pos   = {x = -1117.36, y = 298.24, z = 65.62},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 526
		},
	Delivery5BC = {
			Pos   = {x =-1574.5502, y = 418.6790, z = 108.1},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 165
		},
	Delivery6BC = {
			Pos   = {x = -231.3688, y = 6242.4750, z = 30.2},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 460
		},
	Delivery7BC = {
			Pos   = {x = 1606.8367, y = 3596.0295, z = 34.1},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 247
		},
	Delivery8BC = {
			Pos   = {x = 491.70, y = -1025.42, z = 26.80},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 256
		},
	Delivery9BC = {
			Pos   = {x = 305.10, y = -1451.77, z = 28.62},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 310
		},
	Delivery10BC = {
			Pos   = {x = -251.85, y = 6135.86, z = 29.81},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 240
		},
	Delivery11BC = {
			Pos   = {x = -355.09, y = -114.71, z = 37.35},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 1000
		},
	Delivery12BC = {
			Pos   = {x = 902.18, y = -186.29, z = 72.51},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 450
		},
	Delivery13BC = {
			Pos   = {x = -912.39, y = -799.27, z = 29.53},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 754
		},
	Delivery14BC = {
			Pos   = {x = -19.49, y = -1106.68, z = 25.32},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 200
		},
	Delivery15BC = {
			Pos   = {x = 709.83, y = -1077.47, z = 21.02},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 150
		},


	
	RetourCamion = {
			Pos   = {x = -1327.67, y = -214.56, z = 43.32},
			Color = {r = 0, g = 0, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 0, g = 0, b = 0},
			Type  = 1,
			Paye = 0
		},
	
	AnnulerMission = {
			Pos   = {x = -1308.92, y = -253.71, z = 40.52},
			Color = {r = 0, g = 0, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 0, g = 0, b = 0},
			Type  = 1,
			Paye = 0
		},
}
