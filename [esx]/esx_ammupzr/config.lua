Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 21
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerColor                = { r = 0, g = 255, b = 255 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = true
Config.MaxInService               = -1
Config.Locale = 'fr'

Config.AmmuStations = {

  Ammu = {

    Blip = {
      Pos     = { x = 1694.17, y = 3758.37, z = 34.71 },
      Sprite  = 150,
      Display = 4,
      Scale   = 1.4,
      Colour  = 75,
    },

    AuthorizedWeapons = {
-- Define prices of the weapons.
      { name = 'WEAPON_KNIFE',                     price = 5000 },
      { name = 'WEAPON_PISTOL',                    price = 10000 },
      { name = 'WEAPON_PISTOL50',                  price = 15000 },
      { name = 'WEAPON_APPISTOL',                  price = 20000 },
      { name = 'WEAPON_BAT',                       price = 1000 },
      { name = 'WEAPON_COMPACTRIFLE',              price = 50000 },
      { name = 'WEAPON_FIREEXTINGUISHER',          price = 200 },
      { name = 'WEAPON_FLARE',                     price = 200 },
    },

	  AuthorizedVehicles = {
		  
		  { name = 'r50',    label = 'R50' },
		  { name = 's65amg', label = 'Mercedes Classe S' },
		  { name = 'kx450f', label = '450 KXF' },
		  { name = 'techi', label = 'Toyota Armée' },
		  
	  },

    Armories = {
      { x = 1695.62, y = 3761.21, z = 34.71 },
    },

    Cloakrooms = {
      { x = 1698.97, y = 3758.62, z = 34.71},
    },
  
  
    Vehicles = {
      {
        Spawner    = { x = 1704.42, y = 3764.62, z = 34.36 },
        SpawnPoint = { x = 1708.01, y = 3752.59, z = 33.8 },
        Heading    = 228.88,
      }
    },

    Helicopters = {
      {
        Spawner    = { x = 113.30500793457, y = -3109.3337402344, z = 6.0060696601868 },
        SpawnPoint = { x = 112.94457244873, y = -3102.5942382813, z = 6.0050659179688 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = 1685.47, y = 3753.08, z = 34.14 },
      
    },

    BossActions = {
      { x = 1694.68, y = 3753.68, z = 34.71 },
    },

  },

}
