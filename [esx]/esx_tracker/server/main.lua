ESX = nil
steamIdentifiers = {}

TriggerEvent('esx:getSharedObject', function(obj)
	ESX = obj
end)

AddEventHandler('esx:onAddInventoryItem', function(source, item, count)
	local source_ = source
	if item.name == 'tracker' then
		if #steamIdentifiers > 0 then
			for i=#steamIdentifiers,1,-1 do
			    if steamIdentifiers[i].src == source_ then
			        table.remove(steamIdentifiers, i)
			    end
			end
		end
    	local identifier = GetPlayerIdentifiers(source_)[1]
		local xPlayer = ESX.GetPlayerFromIdentifier(identifier)
		local jobname = xPlayer.getJob().name
		local gradename = xPlayer.getJob().grade_label
		local elem = {}
		elem['src'] = source_
		elem['job'] = jobname
		elem['grade'] = gradename
		elem['name'] = xPlayer.getName()
		table.insert(steamIdentifiers, elem)
		TriggerClientEvent('esx_tracker:update', -1)
	elseif item.name == 'sniffer' then
		TriggerClientEvent('esx_tracker:addSniffer', source_)
	end
end)

ESX.RegisterUsableItem('tracker', function(source)
	local source_ = source
	local xPlayer = ESX.GetPlayerFromId(source_)
	local jobname = xPlayer.getJob().name

	if jobname == "police" or jobname == "sherif" then
		TriggerClientEvent('esx_tracker:give', source_)
	else
		TriggerClientEvent('esx:showNotification', source_, 'La puce ne peut pas être enlevé')
	end

end)

AddEventHandler('esx:onRemoveInventoryItem', function(source, item, count)
	local source_ = source
	if item.name == 'tracker' and item.count < 1 then
		if #steamIdentifiers > 0 then
			for i=#steamIdentifiers,1,-1 do
			    if steamIdentifiers[i].src == source_ then
			        table.remove(steamIdentifiers, i)
			    end
			end
		end

		TriggerClientEvent('esx_tracker:update', -1)
	elseif item.name == 'sniffer' and item.count < 1 then
		TriggerClientEvent('esx_tracker:removeSniffer', source_)
	end
end)

RegisterServerEvent('esx_tracker:putTrackerOnPlayer')
AddEventHandler('esx_tracker:putTrackerOnPlayer', function(player)
	local xPlayer = ESX.GetPlayerFromId(source)
	local xPlayer2 = ESX.GetPlayerFromId(player)

	xPlayer.removeInventoryItem('tracker', 1)
	xPlayer2.addInventoryItem('tracker', 1)
end)

RegisterServerEvent('esx_tracker:get')
AddEventHandler('esx_tracker:get', function()
	TriggerClientEvent('esx_tracker:getCallback', -1, steamIdentifiers)
end)

RegisterServerEvent('esx_tracker:addTracker')
AddEventHandler('esx_tracker:addTracker', function()
	local source_ = source
	if #steamIdentifiers > 0 then
		for i=#steamIdentifiers,1,-1 do
		    if steamIdentifiers[i].src == source_ then
		        table.remove(steamIdentifiers, i)
		    end
		end
	end

	local identifier = GetPlayerIdentifiers(source_)[1]
	local xPlayer = ESX.GetPlayerFromIdentifier(identifier)
	local jobname = xPlayer.getJob().name
	local gradename = xPlayer.getJob().grade_label
	local elem = {}
	elem['src'] = source_
	elem['job'] = jobname
	elem['grade'] = gradename
	elem['name'] = xPlayer.getName()
	table.insert(steamIdentifiers, elem)
	TriggerClientEvent('esx_tracker:update', -1)
end)
