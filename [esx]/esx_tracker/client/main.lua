ESX = nil
local hasSniffer = false
local blipList = {}

local loaded = false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer
	local isHavingTracker = false
	local isHavingSniffer = false

	for i=1, #PlayerData.inventory, 1 do
		if PlayerData.inventory[i].name == 'sniffer' then
			if PlayerData.inventory[i].count > 0 then
				isHavingSniffer = true
			end
		elseif PlayerData.inventory[i].name == 'tracker' then
			if PlayerData.inventory[i].count > 0 then
				isHavingTracker = true
			end
		end
	end

	if isHavingTracker then
		TriggerServerEvent('esx_tracker:addTracker')
		if isHavingSniffer then
			hasSniffer = true
		end
	elseif isHavingSniffer then
		hasSniffer = true
	  	TriggerServerEvent('esx_tracker:get')
	end
end)

RegisterNetEvent('esx_tracker:addSniffer')
AddEventHandler('esx_tracker:addSniffer', function()
	hasSniffer = true
	TriggerServerEvent('esx_tracker:get')
end)

RegisterNetEvent('esx_tracker:removeSniffer')
AddEventHandler('esx_tracker:removeSniffer', function()
	hasSniffer = false
  	TriggerServerEvent('esx_tracker:get')
end)

RegisterNetEvent('esx_tracker:update')
AddEventHandler('esx_tracker:update', function()
  	TriggerServerEvent('esx_tracker:get')
end)


RegisterNetEvent('esx_tracker:give')
AddEventHandler('esx_tracker:give', function()
    local t, distance = GetClosestPlayer()
    if(t ~= -1 and distance < 3) then
        TriggerServerEvent('esx_tracker:putTrackerOnPlayer', GetPlayerServerId(t))
    end
end)


RegisterNetEvent('esx_tracker:getCallback')
AddEventHandler('esx_tracker:getCallback', function(list)
	Citizen.CreateThread(function()
		-- Fuck that, blip seems to don't want to stick to player if inited to early
	    Citizen.Wait(5000)
		if #blipList > 0 then
			for i=#blipList,1,-1 do
			    if DoesBlipExist(blipList[i]) then
			    	RemoveBlip(blipList[i])
			    end
			end
		end
			blipList = {}

		if hasSniffer and #list > 0 then
		  local players = ESX.Game.GetPlayers()
	      for i = 1, #players, 1 do
	          local ped    = GetPlayerPed(players[i])
	          local find = -1

	      	  for j = 1, #list, 1 do
		          if GetPlayerServerId(players[i]) == list[j].src then
		          	find = j
		          end
	      	  end

	          if find > -1 and DoesEntityExist(ped) and list[find] ~= nil then
	          	local myBlip = AddBlipForEntity(ped)
	          	if list[find].job == 'police' then
					SetBlipSprite(myBlip, 1)
					SetBlipColour(myBlip, 3)
					SetBlipScale(myBlip, 1.0)
					BeginTextCommandSetBlipName("STRING")
					AddTextComponentString(tostring("LSPD " .. list[find].grade))
					EndTextCommandSetBlipName(myBlip)
	          	elseif list[find].job == 'sherif' then
					SetBlipSprite(myBlip, 1)
					SetBlipColour(myBlip, 38)
					SetBlipScale(myBlip, 1.0)
					BeginTextCommandSetBlipName("STRING")
					AddTextComponentString(tostring("LSPD " .. list[find].grade))
					EndTextCommandSetBlipName(myBlip)
	          	else
					SetBlipSprite(myBlip, 188)
					SetBlipColour(myBlip, 1)
					SetBlipScale(myBlip, 1.0)
					BeginTextCommandSetBlipName("STRING")
					AddTextComponentString(tostring(list[find].name))
					EndTextCommandSetBlipName(myBlip)
	          	end
				SetBlipDisplay(myBlip, 3)
				SetBlipFlashes(myBlip, true)
			  	table.insert(blipList, myBlip)
	    		Citizen.Wait(1000)
	          end
	        end
	      end
	end)
end)

function GetPlayers()
    local players = {}

    for i = 0, 31 do
        if NetworkIsPlayerActive(i) then
            table.insert(players, i)
        end
    end

    return players
end

function GetClosestPlayer()
  local players = GetPlayers()
  local closestDistance = -1
  local closestPlayer = -1
  local ply = GetPlayerPed(-1)
  local plyCoords = GetEntityCoords(ply, 0)
  
  for index,value in ipairs(players) do
    local target = GetPlayerPed(value)
    if(target ~= ply) then
      local targetCoords = GetEntityCoords(GetPlayerPed(value), 0)
      local distance = GetDistanceBetweenCoords(targetCoords["x"], targetCoords["y"], targetCoords["z"], plyCoords["x"], plyCoords["y"], plyCoords["z"], true)
      if(closestDistance == -1 or closestDistance > distance) then
        closestPlayer = value
        closestDistance = distance
      end
    end
  end
  
  return closestPlayer, closestDistance
end
