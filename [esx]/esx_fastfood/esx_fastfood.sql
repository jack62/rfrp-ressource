USE `essentialmode`;

CREATE TABLE `fastfood` (
  
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  
  PRIMARY KEY (`id`)
);

INSERT INTO `fastfood` (name, item, price) VALUES
	('fastfood','burger',150),
	('fastfood','burgerxl',175),
	('fastfood','frite',90),
	('fastfood','fritexl',110),
	('fastfood','soda',70),
	('fastfood','sodaxl',80)
;