Config = {}

Config.DrawDistance = 100
Config.Size         = {x = 1.5, y = 1.5, z = 1.5}
Config.Color        = {r = 0, g = 128, b = 255}
Config.Type         = 1
Config.Locale = 'fr'

Config.Zones = {

    fastfood = {
        Items = {},
        Pos = {
            {x = -1182.809, y = -883.820, z = 12.500},
    		{x = -1540.525, y = -454.356, z = 39.250},
    		{x = 81.312, y = 274.131, z = 109.000},
			{x = -122.972, y = 6389.864, z = 31.177},
			{x = 133.171, y = -1462.823, z = 28.350}
        }
    },

}
