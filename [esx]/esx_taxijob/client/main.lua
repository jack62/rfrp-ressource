local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData                = {}
local GUI                       = {}
local HasAlreadyEnteredMarker   = false
local LastZone                  = nil
local CurrentAction             = nil
local CurrentActionMsg          = ''
local CurrentActionData         = {}
local OnJob                     = false
local CurrentCustomer           = nil
local CurrentCustomerBlip       = nil
local DestinationBlip           = nil
local IsNearCustomer            = false
local CustomerIsEnteringVehicle = false
local CustomerEnteredVehicle    = false
local TargetCoords              = nil

ESX                             = nil
GUI.Time                        = 0

Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end
end)

function DrawSub(msg, time)
  ClearPrints()
  SetTextEntry_2("STRING")
  AddTextComponentString(msg)
  DrawSubtitleTimed(time, 1)
end

function ShowLoadingPromt(msg, time, type)
  Citizen.CreateThread(function()
    Citizen.Wait(0)
    N_0xaba17d7ce615adbf("STRING")
    AddTextComponentString(msg)
    N_0xbd12f8228410d9b4(type)
    Citizen.Wait(time)
    N_0x10d373323e5b9c0d()
  end)
end

function GetRandomWalkingNPC()

  local search = {}
  local peds = ESX.Game.GetPeds()
  --local pos = GetEntityCoords(GetPlayerPed(-1))

  for i=1, #peds, 1 do
    if IsPedHuman(peds[i]) and IsPedWalking(peds[i]) and not IsPedAPlayer(peds[i]) then
      table.insert(search, peds[i])
    end
  end

  if #search > 0 then
    return search[GetRandomIntInRange(1, #search)]
  end

  --print('Using fallback code to find walking ped')

  for i=1, 1000, 1 do
    local ped = GetRandomPedAtCoord(0.0,  0.0,  0.0,  math.huge + 0.0,  math.huge + 0.0,  math.huge + 0.0,  26)
    --local ped = GetClosestPed(pos['x'], pos['y'], pos['z'], 100.05, 1, 0, 0, 0, 26)

    if DoesEntityExist(ped) and IsPedHuman(ped) and IsPedWalking(ped) and not IsPedAPlayer(ped) then
      table.insert(search, ped)
    end

  end

  if #search > 0 then
    return search[GetRandomIntInRange(1, #search)]
  end

end

function ClearCurrentMission()

  if DoesBlipExist(CurrentCustomerBlip) then
    RemoveBlip(CurrentCustomerBlip)
  end

  if DoesBlipExist(DestinationBlip) then
    RemoveBlip(DestinationBlip)
  end

  CurrentCustomer           = nil
  CurrentCustomerBlip       = nil
  DestinationBlip           = nil
  IsNearCustomer            = false
  CustomerIsEnteringVehicle = false
  CustomerEnteredVehicle    = false
  TargetCoords              = nil

end

function StartTaxiJob()

  ShowLoadingPromt(_U('taking_service') .. 'Taxi/Uber', 5000, 3)
  ClearCurrentMission()

  OnJob = true

end

function StopTaxiJob()

  local playerPed = GetPlayerPed(-1)

  if IsPedInAnyVehicle(playerPed, false) and CurrentCustomer ~= nil then
    local vehicle = GetVehiclePedIsIn(playerPed,  false)
    TaskLeaveVehicle(CurrentCustomer,  vehicle,  0)

    if CustomerEnteredVehicle then
      TaskGoStraightToCoord(CurrentCustomer,  TargetCoords.x,  TargetCoords.y,  TargetCoords.z,  1.0,  -1,  0.0,  0.0)
    end

  end

  ClearCurrentMission()

  OnJob = false

  DrawSub(_U('mission_complete'), 5000)

end

function OpenTaxiActionsMenu()

  local elements = {
    {label = 'Prendre son service (recevoir appels)', value = 'cloakroom'},
	  {label = 'Fin de service (appels off)', value = 'cloakroom2'}
  }

  if PlayerData.job.grade >= 4 then
    table.insert(elements, {label = _U('deposit_stock'), value = 'put_stock'})
    table.insert(elements, {label = _U('take_stock'), value = 'get_stock'})
  end
  
  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'taxi', GetCurrentResourceName(), 'taxi_actions',
    {
      title    = '',
      elements = elements
    },
    function(data, menu)

      if data.current.value == 'put_stock' then
        OpenPutStocksMenu()
      end

      if data.current.value == 'get_stock' then
        OpenGetStocksMenu()
      end

			
	  if data.current.value == 'cloakroom' then

	        TriggerServerEvent("player:serviceOn", "taxi")	

		ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)

    		if skin.sex == 0 then
        		TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
    		else
        		TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
    		end
    
		end)
	  end
	  
	  if data.current.value == 'cloakroom2' then
		--menu.close()
		ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
    		TriggerEvent('skinchanger:loadSkin', skin)    
		end)
	  end		

    end,
	
    function(data, menu)

      menu.close()

      CurrentAction     = 'taxi_actions_menu'
      CurrentActionMsg  = _U('press_to_open')
      CurrentActionData = {}

    end
  )

end

function OpenTaxiBossMenu()

local elements = {
    {label = _U('boss_actions'), value = 'boss_actions'}
  }

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'taxi', GetCurrentResourceName(), 'taxi_boss',
    {
      title    = '',
      elements = elements
    },
    function(data, menu)

if data.current.value == 'boss_actions' then
        TriggerEvent('esx_society:openBossMenu', 'taxi', function(data, menu)
          menu.close()
        end)
      end

    end,
	
    function(data, menu)

      menu.close()

      CurrentAction     = 'taxi_boss_menu'
      CurrentActionMsg  = _U('press_to_open')
      CurrentActionData = {}

    end
  )

end

function OpenTaxiVehiclesMenu()

local elements = {
	{
    label = _U('spawn_veh'), value = 'spawn_vehicle_0'}
  }
  
  if PlayerData.job.grade >= 1 then
  table.insert(elements, {label = 'Sortir RS7', value = 'spawn_vehicle_1'})
  end

  if PlayerData.job.grade >= 3 then
	table.insert(elements, {label = 'Sortir le bus', value = 'spawn_vehicle_32'})
  table.insert(elements, {label = 'Sortir Limousine', value = 'spawn_vehicle_3'})
  table.insert(elements, {label = 'Sortir Hummer Limousine', value = 'spawn_vehicle_31'})
  end

  if PlayerData.job.grade >= 5 then
	table.insert(elements, {label = 'Sortir I8', value = 'spawn_vehicle_5'})
  end

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'taxi', GetCurrentResourceName(), 'taxi_vehicles',
    {
      title    = '',
      elements = elements
    },
    function(data, menu)

    if data.current.value == 'spawn_vehicle_0' then

        if Config.EnableSocietyOwnedVehicles then

          local elements = {}

          ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(vehicles)

            for i=1, #vehicles, 1 do
              table.insert(elements, {label = GetDisplayNameFromVehicleModel(vehicles[i].model) .. ' [' .. vehicles[i].plate .. ']', value = vehicles[i]})
            end

            ESX.UI.Menu.Open(
              'taxi', GetCurrentResourceName(), 'vehicle_spawner',
              {
                title    = '',
                align    = 'top-left',
                elements = elements,
              },
              function(data, menu)

                menu.close()

                local vehicleProps = data.current.value

                ESX.Game.SpawnVehicle(vehicleProps.model, Config.Zones.VehicleSpawnPoint.Pos, 270.0, function(vehicle)
                  ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
                  local playerPed = GetPlayerPed(-1)
          SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
                  TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
                end)

                TriggerServerEvent('esx_society:removeVehicleFromGarage', 'taxi', vehicleProps)

              end,
              function(data, menu)
                menu.close()
              end
            )

          end, 'taxi')

        else

          menu.close()

          if Config.MaxInService == -1 then

            local playerPed = GetPlayerPed(-1)
            local coords    = Config.Zones.VehicleSpawnPoint.Pos

            ESX.Game.SpawnVehicle('taxi', coords, 225.0, function(vehicle)
        SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
              TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
            end)

          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                local playerPed = GetPlayerPed(-1)
                local coords    = Config.Zones.VehicleSpawnPoint.Pos

                ESX.Game.SpawnVehicle('taxi', coords, 225.0, function(vehicle)
          SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
                  TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
                end)

              else

                ESX.ShowNotification(_U('full_service') .. inServiceCount .. '/' .. maxInService)
        
              end

            end, 'taxi')

          end

        end

      end
    
    if data.current.value == 'spawn_vehicle_1' then

        if Config.EnableSocietyOwnedVehicles then

          local elements = {}

          ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(vehicles)

            for i=1, #vehicles, 1 do
              table.insert(elements, {label = GetDisplayNameFromVehicleModel(vehicles[i].model) .. ' [' .. vehicles[i].plate .. ']', value = vehicles[i]})
            end

            ESX.UI.Menu.Open(
              'taxi', GetCurrentResourceName(), 'vehicle_spawner',
              {
                title    = '',
                align    = 'top-left',
                elements = elements,
              },
              function(data, menu)

                menu.close()

                local vehicleProps = data.current.value

                ESX.Game.SpawnVehicle(vehicleProps.model, Config.Zones.VehicleSpawnPoint.Pos, 270.0, function(vehicle)
                  ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
                  local playerPed = GetPlayerPed(-1)
          SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
                  TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
                end)

                TriggerServerEvent('esx_society:removeVehicleFromGarage', '2016rs7', vehicleProps)

              end,
              function(data, menu)
                menu.close()
              end
            )

          end, 'taxi')

        else

          menu.close()

          if Config.MaxInService == -1 then

            local playerPed = GetPlayerPed(-1)
            local coords    = Config.Zones.VehicleSpawnPoint.Pos

            ESX.Game.SpawnVehicle('2016rs7', coords, 225.0, function(vehicle)
        SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
              TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
            end)

          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                local playerPed = GetPlayerPed(-1)
                local coords    = Config.Zones.VehicleSpawnPoint.Pos

                ESX.Game.SpawnVehicle('2016rs7', coords, 225.0, function(vehicle)
          SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
                  TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
                end)

              else

                ESX.ShowNotification(_U('full_service') .. inServiceCount .. '/' .. maxInService)
        
              end

            end, 'taxi')

          end

        end

      end
    
    if data.current.value == 'spawn_vehicle_3' then

        if Config.EnableSocietyOwnedVehicles then

          local elements = {}

          ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(vehicles)

            for i=1, #vehicles, 1 do
              table.insert(elements, {label = GetDisplayNameFromVehicleModel(vehicles[i].model) .. ' [' .. vehicles[i].plate .. ']', value = vehicles[i]})
            end

            ESX.UI.Menu.Open(
              'taxi', GetCurrentResourceName(), 'vehicle_spawner',
              {
                title    = '',
                align    = 'top-left',
                elements = elements,
              },
              function(data, menu)

                menu.close()

                local vehicleProps = data.current.value

                ESX.Game.SpawnVehicle(vehicleProps.model, Config.Zones.VehicleSpawnPoint.Pos, 270.0, function(vehicle)
                  ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
                  local playerPed = GetPlayerPed(-1)
                  SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
                  TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
                end)

                TriggerServerEvent('esx_society:removeVehicleFromGarage', 'Stretch', vehicleProps)

              end,
              function(data, menu)
                menu.close()
              end
            )

          end, 'taxi')

        else

          menu.close()

          if Config.MaxInService == -1 then

            local playerPed = GetPlayerPed(-1)
            local coords    = Config.Zones.VehicleSpawnPoint.Pos

            ESX.Game.SpawnVehicle('Stretch', coords, 225.0, function(vehicle)
              SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
              TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
            end)

          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                local playerPed = GetPlayerPed(-1)
                local coords    = Config.Zones.VehicleSpawnPoint.Pos

                ESX.Game.SpawnVehicle('Stretch', coords, 225.0, function(vehicle)
                  SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
                  TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
                end)

              else

                ESX.ShowNotification(_U('full_service') .. inServiceCount .. '/' .. maxInService)

              end

            end, 'taxi')

          end

        end

    end
    
    if data.current.value == 'spawn_vehicle_31' then

        if Config.EnableSocietyOwnedVehicles then

          local elements = {}

          ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(vehicles)

            for i=1, #vehicles, 1 do
              table.insert(elements, {label = GetDisplayNameFromVehicleModel(vehicles[i].model) .. ' [' .. vehicles[i].plate .. ']', value = vehicles[i]})
            end

            ESX.UI.Menu.Open(
              'taxi', GetCurrentResourceName(), 'vehicle_spawner',
              {
                title    = '',
                align    = 'top-left',
                elements = elements,
              },
              function(data, menu)

                menu.close()

                local vehicleProps = data.current.value

                ESX.Game.SpawnVehicle(vehicleProps.model, Config.Zones.VehicleSpawnPoint.Pos, 270.0, function(vehicle)
                  ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
                  local playerPed = GetPlayerPed(-1)
                  SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
                  TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
                end)

                TriggerServerEvent('esx_society:removeVehicleFromGarage', 'patriot2', vehicleProps)

              end,
              function(data, menu)
                menu.close()
              end
            )

          end, 'taxi')

        else

          menu.close()

          if Config.MaxInService == -1 then

            local playerPed = GetPlayerPed(-1)
            local coords    = Config.Zones.VehicleSpawnPoint.Pos

            ESX.Game.SpawnVehicle('patriot2', coords, 225.0, function(vehicle)
              SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
              TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
            end)

          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                local playerPed = GetPlayerPed(-1)
                local coords    = Config.Zones.VehicleSpawnPoint.Pos

                ESX.Game.SpawnVehicle('patriot2', coords, 225.0, function(vehicle)
                  SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
                  TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
                end)

              else

                ESX.ShowNotification(_U('full_service') .. inServiceCount .. '/' .. maxInService)

              end

            end, 'taxi')

          end

        end

    end
    
    if data.current.value == 'spawn_vehicle_32' then

        if Config.EnableSocietyOwnedVehicles then

          local elements = {}

          ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(vehicles)

            for i=1, #vehicles, 1 do
              table.insert(elements, {label = GetDisplayNameFromVehicleModel(vehicles[i].model) .. ' [' .. vehicles[i].plate .. ']', value = vehicles[i]})
            end

            ESX.UI.Menu.Open(
              'taxi', GetCurrentResourceName(), 'vehicle_spawner',
              {
                title    = '',
                align    = 'top-left',
                elements = elements,
              },
              function(data, menu)

                menu.close()

                local vehicleProps = data.current.value

                ESX.Game.SpawnVehicle(vehicleProps.model, Config.Zones.VehicleSpawnPoint.Pos, 270.0, function(vehicle)
                  ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
                  local playerPed = GetPlayerPed(-1)
                  SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
                  TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
                end)

                TriggerServerEvent('esx_society:removeVehicleFromGarage', 'bus', vehicleProps)

              end,
              function(data, menu)
                menu.close()
              end
            )

          end, 'taxi')

        else

          menu.close()

          if Config.MaxInService == -1 then

            local playerPed = GetPlayerPed(-1)
            local coords    = Config.Zones.VehicleSpawnPoint.Pos

            ESX.Game.SpawnVehicle('bus', coords, 225.0, function(vehicle)
              SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
              TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
            end)

          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                local playerPed = GetPlayerPed(-1)
                local coords    = Config.Zones.VehicleSpawnPoint.Pos

                ESX.Game.SpawnVehicle('bus', coords, 225.0, function(vehicle)
                  SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
                  TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
                end)

              else

                ESX.ShowNotification(_U('full_service') .. inServiceCount .. '/' .. maxInService)

              end

            end, 'taxi')

          end

        end

    end
    
	  if data.current.value == 'spawn_vehicle_5' then

				if Config.EnableSocietyOwnedVehicles then

					local elements = {}

					ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(vehicles)

						for i=1, #vehicles, 1 do
							table.insert(elements, {label = GetDisplayNameFromVehicleModel(vehicles[i].model) .. ' [' .. vehicles[i].plate .. ']', value = vehicles[i]})
						end

						ESX.UI.Menu.Open(
							'taxi', GetCurrentResourceName(), 'vehicle_spawner',
							{
								title    = '',
								align    = 'top-left',
								elements = elements,
							},
							function(data, menu)

								menu.close()

								local vehicleProps = data.current.value

								ESX.Game.SpawnVehicle(vehicleProps.model, Config.Zones.VehicleSpawnPoint.Pos, 270.0, function(vehicle)
									ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
									local playerPed = GetPlayerPed(-1)
									SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
									TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
								end)

								TriggerServerEvent('esx_society:removeVehicleFromGarage', 'i8', vehicleProps)

							end,
							function(data, menu)
								menu.close()
							end
						)

					end, 'taxi')

				else

					menu.close()

					if Config.MaxInService == -1 then

						local playerPed = GetPlayerPed(-1)
						local coords    = Config.Zones.VehicleSpawnPoint.Pos

						ESX.Game.SpawnVehicle('i8', coords, 225.0, function(vehicle)
							SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
							TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
						end)

					else

						ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

							if canTakeService then

								local playerPed = GetPlayerPed(-1)
								local coords    = Config.Zones.VehicleSpawnPoint.Pos

								ESX.Game.SpawnVehicle('i8', coords, 225.0, function(vehicle)
									SetVehicleNumberPlateText(vehicle, 'TAXI ' .. ESX.GetRandomString(3))
									TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
								end)

							else

								ESX.ShowNotification(_U('full_service') .. inServiceCount .. '/' .. maxInService)

							end

						end, 'taxi')

					end

				end

	  end 

 end,
	
    function(data, menu)

      menu.close()

      CurrentAction     = 'taxi_vehicles_menu'
      CurrentActionMsg  = _U('press_to_open')
      CurrentActionData = {}

    end
  )

end

function OpenMobileTaxiActionsMenu()

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'mobile_taxi_actions',
    {
      title    = 'Taxi',
      elements = {
        {label = _U('billing'), value = 'billing'}
      }
    },
    function(data, menu)

      if data.current.value == 'billing' then

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'billing',
          {
            title = _U('invoice_amount')
          },
          function(data, menu)

            local amount = tonumber(data.value)

            if amount == nil then
              ESX.ShowNotification(_U('amount_invalid'))
            else

              menu.close()

              local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

              if closestPlayer == -1 or closestDistance > 3.0 then
                ESX.ShowNotification(_U('no_players_near'))
              else
                TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_taxi', 'Taxi', amount)
              end

            end

          end,
          function(data, menu)
            menu.close()
          end
        )

      end

    end,
    function(data, menu)
      menu.close()
    end
  )

end

function OpenGetStocksMenu()

  ESX.TriggerServerCallback('esx_taxijob:getStockItems', function(items)

    print(json.encode(items))

    local elements = {}

    for i=1, #items, 1 do
      table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
    end

    ESX.UI.Menu.Open(
      'taxi', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = '',
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('quantity_invalid'))
            else
              menu2.close()
              menu.close()
              OpenGetStocksMenu()

              TriggerServerEvent('esx_taxijob:getStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutStocksMenu()

  ESX.TriggerServerCallback('esx_taxijob:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'taxi', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = '',
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('quantity_invalid'))
            else
              menu2.close()
              menu.close()
              OpenPutStocksMenu()

              TriggerServerEvent('esx_taxijob:putStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end


RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

AddEventHandler('esx_taxijob:hasEnteredMarker', function(zone)

  if zone == 'TaxiActions' then
    CurrentAction     = 'taxi_actions_menu'
    CurrentActionMsg  = _U('press_to_open')
    CurrentActionData = {}
  end
  
  if zone == 'TaxiBossMenu' then
    CurrentAction     = 'taxi_boss_menu'
    CurrentActionMsg  = _U('press_to_open')
    CurrentActionData = {}
  end
  
  if zone == 'TaxiVehiclesMenu' then
    CurrentAction     = 'taxi_vehicles_menu'
    CurrentActionMsg  = _U('press_to_open')
    CurrentActionData = {}
  end

  if zone == 'VehicleDeleter' then

    local playerPed = GetPlayerPed(-1)
    local vehicle = GetVehiclePedIsIn(playerPed, false)

    if IsPedInAnyVehicle(playerPed,  false) then
      CurrentAction     = 'delete_vehicle'
      CurrentActionMsg  = _U('store_veh')
      CurrentActionData = { vehicle = vehicle }
    end

  end

end)

AddEventHandler('esx_taxijob:hasExitedMarker', function(zone)
  ESX.UI.Menu.CloseAll()
  CurrentAction = nil
end)

-- Create Blips
Citizen.CreateThread(function()

  local blip = AddBlipForCoord(-348.91,-113.04,39.43)

  SetBlipSprite (blip, 198)
  SetBlipDisplay(blip, 4)
  SetBlipScale  (blip, 1.0)
  SetBlipColour (blip, 28)
  SetBlipAsShortRange(blip, true)

  BeginTextCommandSetBlipName("STRING")
  AddTextComponentString("Taxi")
  EndTextCommandSetBlipName(blip)

end)



-- Display markers
Citizen.CreateThread(function()
  while true do

    Wait(0)

    if PlayerData.job ~= nil and PlayerData.job.name == 'taxi' then

      local coords = GetEntityCoords(GetPlayerPed(-1))

      for k,v in pairs(Config.Zones) do
        if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
          DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
        end
      end

    end	
	
    if PlayerData.job ~= nil and PlayerData.job.name == 'taxi' and PlayerData.job.grade_name == 'boss' then

      local coords = GetEntityCoords(GetPlayerPed(-1))

      for k,v in pairs(Config.Boss) do
        if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
          DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
        end
      end

    end

  end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do

    Wait(0)

    if PlayerData.job ~= nil and PlayerData.job.name == 'taxi' then

      local coords      = GetEntityCoords(GetPlayerPed(-1))
      local isInMarker  = false
      local currentZone = nil

      for k,v in pairs(Config.Zones) do
        if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
          isInMarker  = true
          currentZone = k
        end
      end
	  
	  if PlayerData.job ~= nil and PlayerData.job.name == 'taxi' and PlayerData.job.grade_name == 'boss' then
		for k,v in pairs(Config.Boss) do
			if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
				isInMarker  = true
				currentZone = k
			end
		end
	  end	

      if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
        HasAlreadyEnteredMarker = true
        LastZone                = currentZone
        TriggerEvent('esx_taxijob:hasEnteredMarker', currentZone)
      end

      if not isInMarker and HasAlreadyEnteredMarker then
        HasAlreadyEnteredMarker = false
        TriggerEvent('esx_taxijob:hasExitedMarker', LastZone)
      end

    end

  end
end)

-- Taxi Job
Citizen.CreateThread(function()

  while true do

    Citizen.Wait(0)

    local playerPed = GetPlayerPed(-1)

    if OnJob then

      if CurrentCustomer == nil then

        DrawSub(_U('drive_search_pass'), 5000)

        if IsPedInAnyVehicle(playerPed,  false) and GetEntitySpeed(playerPed) > 0 then

          local waitUntil = GetGameTimer() + GetRandomIntInRange(30000,  45000)

          while OnJob and waitUntil > GetGameTimer() do
            Citizen.Wait(0)
          end

          if OnJob and IsPedInAnyVehicle(playerPed,  false) and GetEntitySpeed(playerPed) > 0 then

            CurrentCustomer = GetRandomWalkingNPC()

            if CurrentCustomer ~= nil then

              CurrentCustomerBlip = AddBlipForEntity(CurrentCustomer)

              SetBlipAsFriendly(CurrentCustomerBlip, 1)
              SetBlipColour(CurrentCustomerBlip, 2)
              SetBlipCategory(CurrentCustomerBlip, 3)
              SetBlipRoute(CurrentCustomerBlip,  true)

              SetEntityAsMissionEntity(CurrentCustomer,  true, false)
              ClearPedTasksImmediately(CurrentCustomer)
              SetBlockingOfNonTemporaryEvents(CurrentCustomer, 1)

              local standTime = GetRandomIntInRange(60000,  180000)

              TaskStandStill(CurrentCustomer, standTime)

              ESX.ShowNotification(_U('customer_found'))

            end

          end

        end

      else

        if IsPedFatallyInjured(CurrentCustomer) then

          ESX.ShowNotification(_U('client_unconcious'))

          if DoesBlipExist(CurrentCustomerBlip) then
            RemoveBlip(CurrentCustomerBlip)
          end

          if DoesBlipExist(DestinationBlip) then
            RemoveBlip(DestinationBlip)
          end

          SetEntityAsMissionEntity(CurrentCustomer,  false, true)

          CurrentCustomer           = nil
          CurrentCustomerBlip       = nil
          DestinationBlip           = nil
          IsNearCustomer            = false
          CustomerIsEnteringVehicle = false
          CustomerEnteredVehicle    = false
          TargetCoords              = nil

        end

        if IsPedInAnyVehicle(playerPed,  false) then

          local vehicle          = GetVehiclePedIsIn(playerPed,  false)
          local playerCoords     = GetEntityCoords(playerPed)
          local customerCoords   = GetEntityCoords(CurrentCustomer)
          local customerDistance = GetDistanceBetweenCoords(playerCoords.x,  playerCoords.y,  playerCoords.z,  customerCoords.x,  customerCoords.y,  customerCoords.z)

          if IsPedSittingInVehicle(CurrentCustomer,  vehicle) then

            if CustomerEnteredVehicle then

              local targetDistance = GetDistanceBetweenCoords(playerCoords.x,  playerCoords.y,  playerCoords.z,  TargetCoords.x,  TargetCoords.y,  TargetCoords.z)

              if targetDistance <= 10.0 then

                TaskLeaveVehicle(CurrentCustomer,  vehicle,  0)

                ESX.ShowNotification(_U('arrive_dest'))

                TaskGoStraightToCoord(CurrentCustomer,  TargetCoords.x,  TargetCoords.y,  TargetCoords.z,  1.0,  -1,  0.0,  0.0)
                SetEntityAsMissionEntity(CurrentCustomer,  false, true)

                TriggerServerEvent('esx_taxijob:success')

                RemoveBlip(DestinationBlip)

                local scope = function(customer)
                  ESX.SetTimeout(60000, function()
                    DeletePed(customer)
                  end)
                end

                scope(CurrentCustomer)

                CurrentCustomer           = nil
                CurrentCustomerBlip       = nil
                DestinationBlip           = nil
                IsNearCustomer            = false
                CustomerIsEnteringVehicle = false
                CustomerEnteredVehicle    = false
                TargetCoords              = nil

              end

              if TargetCoords ~= nil then
                DrawMarker(1, TargetCoords.x, TargetCoords.y, TargetCoords.z - 1.0, 0, 0, 0, 0, 0, 0, 4.0, 4.0, 2.0, 178, 236, 93, 155, 0, 0, 2, 0, 0, 0, 0)
              end

            else

              RemoveBlip(CurrentCustomerBlip)

              CurrentCustomerBlip = nil

              TargetCoords = Config.JobLocations[GetRandomIntInRange(1,  #Config.JobLocations)]

              local street = table.pack(GetStreetNameAtCoord(TargetCoords.x, TargetCoords.y, TargetCoords.z))
              local msg    = nil

              if street[2] ~= 0 and street[2] ~= nil then
                msg = string.format(_U('take_me_to_near', GetStreetNameFromHashKey(street[1]),GetStreetNameFromHashKey(street[2])))
              else
                msg = string.format(_U('take_me_to', GetStreetNameFromHashKey(street[1])))
              end

              ESX.ShowNotification(msg)

              DestinationBlip = AddBlipForCoord(TargetCoords.x, TargetCoords.y, TargetCoords.z)

              BeginTextCommandSetBlipName("STRING")
              AddTextComponentString("Destination")
              EndTextCommandSetBlipName(blip)

              SetBlipRoute(DestinationBlip,  true)

              CustomerEnteredVehicle = true

            end

          else

            DrawMarker(1, customerCoords.x, customerCoords.y, customerCoords.z - 1.0, 0, 0, 0, 0, 0, 0, 4.0, 4.0, 2.0, 178, 236, 93, 155, 0, 0, 2, 0, 0, 0, 0)

            if not CustomerEnteredVehicle then

              if customerDistance <= 30.0 then

                if not IsNearCustomer then
                  ESX.ShowNotification(_U('close_to_client'))
                  IsNearCustomer = true
                end

              end

              if customerDistance <= 100.0 then

                if not CustomerIsEnteringVehicle then

                  ClearPedTasksImmediately(CurrentCustomer)

                  local seat = 0

                  for i=4, 0, 1 do
                    if IsVehicleSeatFree(vehicle,  seat) then
                      seat = i
                      break
                    end
                  end

                  TaskEnterVehicle(CurrentCustomer,  vehicle,  -1,  seat,  2.0,  1)

                  CustomerIsEnteringVehicle = true

                end

              end

            end

          end

        else

          DrawSub(_U('return_to_veh'), 5000)

        end

      end

    end

  end
end)

-- Key Controls
Citizen.CreateThread(function()
  while true do

    Citizen.Wait(0)

    if CurrentAction ~= nil then

      SetTextComponentFormat('STRING')
      AddTextComponentString(CurrentActionMsg)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)

      if IsControlPressed(0,  Keys['E']) and PlayerData.job ~= nil and PlayerData.job.name == 'taxi' and (GetGameTimer() - GUI.Time) > 300 then

        if CurrentAction == 'taxi_actions_menu' then
          OpenTaxiActionsMenu()
        end
		
		if CurrentAction == 'taxi_boss_menu' then
		  OpenTaxiBossMenu()
		end 

		if CurrentAction == 'taxi_vehicles_menu' then
		  OpenTaxiVehiclesMenu()
		end		

        if CurrentAction == 'delete_vehicle' then

          local playerPed = GetPlayerPed(-1)

          if Config.EnableSocietyOwnedVehicles then
            local vehicleProps = ESX.Game.GetVehicleProperties(CurrentActionData.vehicle)
            TriggerServerEvent('esx_society:putVehicleInGarage', 'taxi', vehicleProps)
          else
            if GetEntityModel(CurrentActionData.vehicle) == GetHashKey('taxi') then
              if Config.MaxInService ~= -1 then
                TriggerServerEvent('esx_service:disableService', 'taxi')
              end
            end
          end

          ESX.Game.DeleteVehicle(CurrentActionData.vehicle)

        end

        CurrentAction = nil
        GUI.Time      = GetGameTimer()

      end

    end

    if IsControlPressed(0,  Keys['F6']) and Config.EnablePlayerManagement and PlayerData.job ~= nil and PlayerData.job.name == 'taxi' and (GetGameTimer() - GUI.Time) > 150 then
      OpenMobileTaxiActionsMenu()
      GUI.Time = GetGameTimer()
    end

    if IsControlPressed(0,  Keys['F9']) and (GetGameTimer() - GUI.Time) > 150 then

      if OnJob then
        StopTaxiJob()
      else

        if PlayerData.job ~= nil and PlayerData.job.name == 'taxi' then

          local playerPed = GetPlayerPed(-1)

          if IsPedInAnyVehicle(playerPed,  false) then

            local vehicle = GetVehiclePedIsIn(playerPed,  false)

            if PlayerData.job.grade >= 5 then
              StartTaxiJob()
            else
              if GetEntityModel(vehicle) == GetHashKey('taxi') or GetEntityModel(vehicle) == GetHashKey('2016rs7') then
                StartTaxiJob()
              else
                ESX.ShowNotification(_U('must_in_taxi'))
              end
            end

          else

            if PlayerData.job.grade >= 5 then
              ESX.ShowNotification(_U('must_in_vehicle'))
            else
              ESX.ShowNotification(_U('must_in_taxi'))
            end

          end

        end

      end

      GUI.Time = GetGameTimer()

    end

  end
end)

------------------------------------------
----------TP VEHICULE + JOUEUR------------
------------------------------------------
local TeleportFromTo = {

	["Garage"] = {
		positionFrom = { ['x'] = 201.49046225684, ['y'] = -1004.4910888672, ['z'] = -98.999999999999, nom = "Sortir du garage"},
		positionTo = { ['x'] = -384.17, ['y'] = -106.47, ['z'] = 38.7, nom = "Entrer au garage"},
	},
}

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(2)
		local pos = GetEntityCoords(GetVehiclePedIsIn(GetPlayerPed(-1), 0), true)

		for k, j in pairs(TeleportFromTo) do

			if(Vdist(pos.x, pos.y, pos.z, j.positionFrom.x, j.positionFrom.y, j.positionFrom.z) < 150.0) then
				DrawMarker(1, j.positionFrom.x, j.positionFrom.y, j.positionFrom.z - 0.99, 0, 0, 0, 0, 0, 0, 3.5, 3.5, 0.5001, 0, 255, 0, 105, 0, 0, 2, 2, 0, 0, 0)
				if(Vdist(pos.x, pos.y, pos.z, j.positionFrom.x, j.positionFrom.y, j.positionFrom.z) < 5.0)then
					if(Vdist(pos.x, pos.y, pos.z, j.positionFrom.x, j.positionFrom.y, j.positionFrom.z) < 1.0)then
						ClearPrints()
						DisplayHelpText("Appuyez sur ~INPUT_PICKUP~ pour ~b~".. j.positionFrom.nom,1, 1, 0.5, 0.8, 0.9, 255, 255, 255, 255)
						DrawSubtitleTimed(2000, 1)
						if IsControlJustPressed(1, 38) then
							DoScreenFadeOut(1000)
							Citizen.Wait(2000)
							SetEntityCoords(GetVehiclePedIsIn(GetPlayerPed(-1), 0), j.positionTo.x, j.positionTo.y, j.positionTo.z - 1)
							DoScreenFadeIn(1000)
						end
					end
				end
			end

			if(Vdist(pos.x, pos.y, pos.z, j.positionTo.x, j.positionTo.y, j.positionTo.z) < 150.0) then
				DrawMarker(1, j.positionTo.x, j.positionTo.y, j.positionTo.z - 0.99, 0, 0, 0, 0, 0, 0, 3.5, 3.5, 0.5001, 0, 255, 0, 105, 0, 0, 2, 2, 0, 0, 0)
				if(Vdist(pos.x, pos.y, pos.z, j.positionTo.x, j.positionTo.y, j.positionTo.z) < 5.0)then
					if(Vdist(pos.x, pos.y, pos.z, j.positionTo.x, j.positionTo.y, j.positionTo.z) < 1.0)then
						ClearPrints()
						DisplayHelpText("Appuyez sur ~INPUT_PICKUP~ pour ~r~".. j.positionTo.nom,1, 1, 0.5, 0.8, 0.9, 255, 255, 255, 255)
						DrawSubtitleTimed(2000, 1)
						if IsControlJustPressed(1, 38) then
							DoScreenFadeOut(1000)
							Citizen.Wait(2000)
							SetEntityCoords(GetVehiclePedIsIn(GetPlayerPed(-1), 0), j.positionFrom.x, j.positionFrom.y, j.positionFrom.z - 1)
							DoScreenFadeIn(1000)
						end
					end
				end
			end
		end
	end
end)
------------------------------------------
----------TP VEHICULE + JOUEUR------------
------------------------------------------

--------------------------------------------------- Loboratoire Traitement Weed ---------------------------------------------------------------------------------------------------------

local TeleportFromTo = {
	
	["Taxi"] = {
		positionFrom = { ['x'] = -349.26, ['y'] = -112.96, ['z'] = 39.43, nom = "Entrer dans Smith & Co'Taxi."},
		positionTo = { ['x'] = 206.3239440918, ['y'] = -1006.6431884766, ['z'] = -98.999999999999, nom = "Sortir de Smith & Co'Taxi."},
	},	
}

Drawing = setmetatable({}, Drawing)
Drawing.__index = Drawing


function Drawing.draw3DText(x,y,z,textInput,fontId,scaleX,scaleY,r, g, b, a)
    local px,py,pz=table.unpack(GetGameplayCamCoords())
    local dist = GetDistanceBetweenCoords(px,py,pz, x,y,z, 1)

    local scale = (1/dist)*20
    local fov = (1/GetGameplayCamFov())*100
    local scale = scale*fov

    SetTextScale(scaleX*scale, scaleY*scale)
    SetTextFont(fontId)
    SetTextProportional(1)
    SetTextColour(r, g, b, a)
    SetTextDropshadow(0, 0, 0, 0, 255)
    SetTextEdge(2, 0, 0, 0, 150)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    SetTextCentre(1)
    AddTextComponentString(textInput)
    SetDrawOrigin(x,y,z+2, 0)
    DrawText(0.0, 0.0)
    ClearDrawOrigin()
end

function Drawing.drawMissionText(m_text, showtime)
    ClearPrints()
    SetTextEntry_2("STRING")
    AddTextComponentString(m_text)
    DrawSubtitleTimed(showtime, 1)
end

------------------------------------------
------------------------------------------
------Modifier text-----------------------
------------------------------------------
------------------------------------------
function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
	--DisplayHelpTextFromStringLabel(0, 0, 0, -1)			---A 0 Enleve le son de la notif
end
------------------------------------------
------------------------------------------
------Modifier text-----------------------
------------------------------------------
------------------------------------------

function msginf(msg, duree)
    duree = duree or 500
    ClearPrints()
    SetTextEntry_2("STRING")
    AddTextComponentString(msg)
    DrawSubtitleTimed(duree, 1)
end

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(2)
		local pos = GetEntityCoords(GetPlayerPed(-1), true)
		
		for k, j in pairs(TeleportFromTo) do
		
			--msginf(k .. " " .. tostring(j.positionFrom.x), 15000)
			if(Vdist(pos.x, pos.y, pos.z, j.positionFrom.x, j.positionFrom.y, j.positionFrom.z) < 150.0) then
				DrawMarker(1, j.positionFrom.x, j.positionFrom.y, j.positionFrom.z - 1, 0, 0, 0, 0, 0, 0, 1.5001, 1.5001, 0.501, 117,202,93,255, 0, 0, 0,0)
				if(Vdist(pos.x, pos.y, pos.z, j.positionFrom.x, j.positionFrom.y, j.positionFrom.z) < 5.0)then
					--Drawing.draw3DText(j.positionFrom.x, j.positionFrom.y, j.positionFrom.z - 1.100, j.positionFrom.nom, 1, 0.2, 0.1, 255, 255, 255, 215)
					if(Vdist(pos.x, pos.y, pos.z, j.positionFrom.x, j.positionFrom.y, j.positionFrom.z) < 1.0)then
						ClearPrints()
						DisplayHelpText("Appuyez sur ~INPUT_PICKUP~ pour ~b~".. j.positionFrom.nom,1, 1, 0.5, 0.8, 0.9, 255, 255, 255, 255)
						DrawSubtitleTimed(2000, 1)
						if IsControlJustPressed(1, 38) then
							DoScreenFadeOut(1000)
							Citizen.Wait(2000)
							SetEntityCoords(GetPlayerPed(-1), j.positionTo.x, j.positionTo.y, j.positionTo.z - 1)
							DoScreenFadeIn(1000)
						end
					end
				end
			end
			
			if(Vdist(pos.x, pos.y, pos.z, j.positionTo.x, j.positionTo.y, j.positionTo.z) < 150.0) then
				DrawMarker(1, j.positionTo.x, j.positionTo.y, j.positionTo.z - 1, 0, 0, 0, 0, 0, 0, 1.5001, 1.5001, 0.501, 117,202,93,255, 0, 0, 0,0)
				if(Vdist(pos.x, pos.y, pos.z, j.positionTo.x, j.positionTo.y, j.positionTo.z) < 5.0)then
					--Drawing.draw3DText(j.positionTo.x, j.positionTo.y, j.positionTo.z - 1.100, j.positionTo.nom, 1, 0.2, 0.1, 255, 255, 255, 215)
					if(Vdist(pos.x, pos.y, pos.z, j.positionTo.x, j.positionTo.y, j.positionTo.z) < 1.0)then
						ClearPrints()
						DisplayHelpText("Appuyez sur ~INPUT_PICKUP~ pour ~r~".. j.positionTo.nom,1, 1, 0.5, 0.8, 0.9, 255, 255, 255, 255)
						DrawSubtitleTimed(2000, 1)
						if IsControlJustPressed(1, 38) then
							DoScreenFadeOut(1000)
							Citizen.Wait(2000)
							SetEntityCoords(GetPlayerPed(-1), j.positionFrom.x, j.positionFrom.y, j.positionFrom.z - 1)
							DoScreenFadeIn(1000)
						end
					end
				end
			end
		end
	end
end)

RegisterNetEvent('NB:openMenuTaxi')
AddEventHandler('NB:openMenuTaxi', function()
	OpenMobileTaxiActionsMenu()
end)
