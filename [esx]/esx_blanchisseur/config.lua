Config					= {}
Config.Locale			= 'fr'
Config.Slice			= 2500
Config.Percentage		= 80
Config.MarkerSize   	= {x = 3.0, y = 3.0, z = 3.0}
Config.openHours		= 21
Config.closeHours		= 1
Config.Blip 			= false
Config.Hours 			= false
Config.Menu				= false
Config.Bonus			= {min = 0, max = 05}

Config.Zones = {
	{x = -448.710, y = -2176.266, z = 10.541,percent=80},
}
