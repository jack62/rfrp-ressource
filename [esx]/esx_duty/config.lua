Config                            = {}
Config.DrawDistance               = 100.0
--language currently available EN and SV
Config.Locale                     = 'sv'

Config.Zones = {

  PoliceDuty = {
    Pos   = { x = 458.78, y = -990.94, z = 30.691 },
    Size  = { x = 2.5, y = 2.5, z = 1.5 },
    Color = { r = 0, g = 255, b = 0 },  
    Type  = 27,
  },

  AmbulanceDuty = {
    Pos = { x = 268.5, y = -1366.47, z = 23.65 },
    Size = { x = 2.5, y = 2.5, z = 1.5 },
    Color = { r = 0, g = 255, b = 0 },
    Type = 27,
  },
}