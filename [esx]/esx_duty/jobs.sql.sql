INSERT INTO `jobs` (name, label) VALUES
  ('offpolice','Hors Service'),
  ('offambulance','Hors Service')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
  ('offpolice',0,'recruit','Cadet',12,'{}','{}'),
  ('offpolice',1,'officer','Brigadier',24,'{}','{}'),
  ('offpolice',2,'sergeant','Inspecteur',36,'{}','{}'),
  ('offpolice',3,'lieutenant','Commandant',48,'{}','{}'),
  ('offpolice',4,'boss','Comissaire',0,'{}','{}'),
  ('offambulance',0,'ambulance','Ambulance',12,'{}','{}'),
  ('offambulance',1,'doctor','Ambulance',24,'{}','{}'),
  ('offambulance',2,'chief_doctor','Ambulance',36,'{}','{}'),
  ('offambulance',3,'boss','Ambulance',48,'{}','{}')
;