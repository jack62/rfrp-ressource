Config              = {}
Config.MarkerType   = 27
Config.DrawDistance = 0.7
Config.ZoneSize     = {x = 2.5, y = 2.5, z = 2.0}
Config.MarkerColor  = {r = 100, g = 204, b = 100}
Config.MarkerColors  = {
	[0] = {r = 100, g = 204, b = 100},   -- vert
	[1] = {r = 255, g = 255, b = 102},   -- jaune
	[2] = {r = 255, g = 100, b = 0  },   -- orange
	[3] = {r = 255, g = 26 , b = 26 },   -- rouge
}

Config.RequiredCopsCoke  = 3
Config.RequiredCopsMeth  = 3
Config.RequiredCopsWeed  = 2
Config.RequiredCopsOpium = 4

Config.TimeToFarm    = 1 * 2000
Config.TimeToProcess = 1 * 2000
Config.TimeToSell    = 1 * 2000

Config.Locale = 'fr'

Config.Zones = {
	CokeField =       {x=2683.512,  y=-762.100,   z=33.76,   name = _U('coke_field'),       sprite = 501, color = 40, drugType = "Coke"},
	CokeProcessing =  {x=-111.823,  y=-13.767,  z=69.619,  name = _U('coke_processing'),  sprite = 478, color = 40, drugType = "Coke"},
	CokeDealer =      {x=-925.012, y=-1307.040,    z=8.8,    name = _U('coke_dealer'),      sprite = 500, color = 75, drugType = "Coke"},
	MethField =       {x=-1798.99,  y=1916.25,   z=135.56,    name = _U('meth_field'),       sprite = 499, color = 26, drugType = "Meth"},
	MethProcessing =  {x=3602.31,  y=3660.29,  z=33.87,  name = _U('meth_processing'),  sprite = 499, color = 26, drugType = "Meth"},
	MethDealer =      {x=-1798.14,   y=409.29,   z=113.67,   name = _U('meth_dealer'),      sprite = 500, color = 75, drugType = "Meth"},
	WeedField =       {x=1057.68,   y=-3196.309,  z=-99.999,  name = _U('weed_field'),       sprite = 496, color = 52, drugType = "Weed"},
	WeedProcessing =  {x=-1117.206,  y=-502.907,  z=34.906,  name = _U('weed_processing'),  sprite = 496, color = 52, drugType = "Weed"},
	WeedDealer =      {x=189.81,    y=-1071.55,   z=2,   name = _U('weed_dealer'),      sprite = 500, color = 75, drugType = "Weed"},
	OpiumField =      {x=-1900.29,  y=2103.39,  z=135.47,   name = _U('opium_field'),      sprite = 51,  color = 60, drugType = "Opium"},
	OpiumProcessing = {x=1343.5,  y=4383.49,   z=44.34,   name = _U('opium_processing'), sprite = 51,  color = 60, drugType = "Opium"},
	OpiumDealer =     {x=569.79, y=-312.53,   z=18.77,   name = _U('opium_dealer'),     sprite = 500, color = 75, drugType = "Opium"}
	}




