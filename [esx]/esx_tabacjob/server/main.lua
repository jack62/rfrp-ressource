-----------------------------------------
-- Edited by Island Paradise RP
-- LambdaBe / Eden Mertens
-----------------------------------------

ESX = nil
local PlayersTransforming  = {}
local PlayersSelling       = {}
local PlayersHarvesting = {}
local vine = 1
local jus = 1
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
	TriggerEvent('esx_service:activateService', 'tabac', Config.MaxInService)
end

TriggerEvent('esx_phone:registerNumber', 'tabac', _U('tabac_client'), true, true)
TriggerEvent('esx_society:registerSociety', 'tabac', 'TABAC', 'society_tabac', 'society_tabac', 'society_tabac', {type = 'private'})
local function Harvest(source, zone)

	if PlayersHarvesting[source] == true then

		local xPlayer  = ESX.GetPlayerFromId(source)

		if zone == "RecolteTabacBrun" then
			local itemQuantity = xPlayer.getInventoryItem('tabacbrun').count
			if itemQuantity >= 100 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_place'))
				return
			else
				SetTimeout(1800, function()
					xPlayer.addInventoryItem('tabacbrun', 4)
					Harvest(source, zone)
				end)
			end
		end

		if zone == "RecolteTabacBlond" then
			local itemQuantity = xPlayer.getInventoryItem('tabacblond').count
			if itemQuantity >= 100 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_place'))
				return
			else
				SetTimeout(1800, function()
					xPlayer.addInventoryItem('tabacblond', 4)
					Harvest(source, zone)
				end)
			end
		end
	end
end

RegisterServerEvent('esx_tabacjob:startHarvest')
AddEventHandler('esx_tabacjob:startHarvest', function(zone)
	local _source = source

	--if PlayersHarvesting[_source] == false then
	--	TriggerClientEvent('esx:showNotification', _source, '~r~C\'est pas bien de glitch ~w~')
	--	PlayersHarvesting[_source]=false
	--else
		PlayersHarvesting[_source]=true
		TriggerClientEvent('esx:showNotification', _source, _U('recolte_notif'))
		Harvest(_source, zone)
	--end
end)


RegisterServerEvent('esx_tabacjob:stopHarvest')
AddEventHandler('esx_tabacjob:stopHarvest', function()
	local _source = source

	PlayersHarvesting[_source] = false
	TriggerClientEvent('esx:showNotification', _source, 'Vous sortez du ~r~Champs de Tabac')
end)


local function Transform(source, zone)

	if PlayersTransforming[source] == true then

		local xPlayer  = ESX.GetPlayerFromId(source)

		if zone == "TraitementBlond" then
			local itemQuantity = xPlayer.getInventoryItem('tabacblond').count

			if itemQuantity <= 0 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_tabac'))
				return
			else
				SetTimeout(1800, function()
					xPlayer.removeInventoryItem('tabacblond', 1)
					xPlayer.addInventoryItem('tabacblondsec', 1)

					Transform(source, zone)
				end)
			end
		elseif zone == "TraitementBrun" then
			local itemQuantity = xPlayer.getInventoryItem('tabacbrun').count
			if itemQuantity <= 0 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_tabac'))
				return
			else
				SetTimeout(1800, function()
					xPlayer.removeInventoryItem('tabacbrun', 1)
					xPlayer.addInventoryItem('tabacbrunsec', 1)

					Transform(source, zone)
				end)
			end

		elseif zone == "TraitementMalboro" then
			local itemQuantity = xPlayer.getInventoryItem('tabacblondsec').count
			if itemQuantity <= 0 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_tabac_sec'))
				return
			else
				SetTimeout(1800, function()
					xPlayer.removeInventoryItem('tabacblondsec', 1)
					xPlayer.addInventoryItem('cigarette', 1)

					Transform(source, zone)
				end)
			end

		elseif zone == "TraitementGitanes" then
			local itemQuantity = xPlayer.getInventoryItem('tabacbrunsec').count
			if itemQuantity <= 0 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_tabac_sec'))
				return
			else
				SetTimeout(1800, function()
					xPlayer.removeInventoryItem('tabacbrunsec', 1)
					xPlayer.addInventoryItem('gitanes', 1)

					Transform(source, zone)
				end)
			end

		elseif zone == "TraitementSplif" then

			local tabacQuantity = xPlayer.getInventoryItem('tabacbrunsec').count
	      	local WeedQuantity  = xPlayer.getInventoryItem('weed').count

	      	if (tabacQuantity <= 0 or WeedQuantity <= 0) then

	        	TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez pas assez de Tabac Brun Sec et/ou de Weed.')
	        	return
	      	else
	      		SetTimeout(1800, function()

	                xPlayer.removeInventoryItem('tabacbrunsec', 2)
	                xPlayer.removeInventoryItem('weed', 2)
	                xPlayer.addInventoryItem('splif', 1)

					Transform(source, zone)
				end)
			end
		end
	end
end

RegisterServerEvent('esx_tabacjob:startTransform')
AddEventHandler('esx_tabacjob:startTransform', function(zone)
	local _source = source

	--if PlayersTransforming[_source] == false then
	--	TriggerClientEvent('esx:showNotification', _source, '~r~C\'est pas bien de glitch ~w~')
	--	PlayersTransforming[_source]=false
	--else
		PlayersTransforming[_source]=true
		TriggerClientEvent('esx:showNotification', _source, _U('treating_in_progress'))
		Transform(_source,zone)
	--end
end)

RegisterServerEvent('esx_tabacjob:stopTransform')
AddEventHandler('esx_tabacjob:stopTransform', function()

	local _source = source

	--if PlayersTransforming[_source] == true then
		PlayersTransforming[_source]=false
		TriggerClientEvent('esx:showNotification', _source, 'Vous êtes trop loin de l\'~r~établi')

	--else
	--	TriggerClientEvent('esx:showNotification', _source, 'Vous pouvez ~g~transformer votre Tabac')
	--	PlayersTransforming[_source]=true

	--end
end)

local function Sell(source, zone)

	if PlayersSelling[source] == true then
		local xPlayer  = ESX.GetPlayerFromId(source)

		if (zone == 'SellMalbora' or zone == 'SellGitanes') then

			if xPlayer.getInventoryItem('malbora').count <= 0 then
				vine = 0
			else
				vine = 1
			end

			if xPlayer.getInventoryItem('gitanes').count <= 0 then
				jus = 0
			else
				jus = 1
			end

			if vine == 0 and jus == 0 then

				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez pas de Cigarettes à vendre.')
				return

			elseif xPlayer.getInventoryItem('malbora').count <= 0 and jus == 0 then

				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez pas de Malboro à vendre.')
				vine = 0
				return
			elseif xPlayer.getInventoryItem('gitanes').count <= 0 and vine == 0 then

				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez pas de Lucky Strike à vendre.')
				jus = 0
				return
			else
				if (jus == 1 and zone == 'SellGitanes') then

					SetTimeout(275, function()
						local money = 25
						xPlayer.removeInventoryItem('gitanes', 1)
						local societyAccount = nil

						TriggerEvent('esx_addonaccount:getSharedAccount', 'society_tabac', function(account)
							societyAccount = account
						end)

						if societyAccount ~= nil then

							societyAccount.addMoney(money)
							xPlayer.addMoney(7)
							TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. money)
						end
						Sell(source,zone)
					end)
				elseif (vine == 1 and zone == 'SellMalbora') then

					SetTimeout(275, function()
						local money = 25
						xPlayer.removeInventoryItem('malbora', 1)
						local societyAccount = nil

						TriggerEvent('esx_addonaccount:getSharedAccount', 'society_tabac', function(account)
							societyAccount = account
						end)
						if societyAccount ~= nil then
							societyAccount.addMoney(money)
							TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. money)
						end
						Sell(source,zone)
					end)
				end
			end
		end
	end
end

RegisterServerEvent('esx_tabacjob:startSell')
AddEventHandler('esx_tabacjob:startSell', function(zone)

	local _source = source

	--if PlayersSelling[_source] == false then
	--	TriggerClientEvent('esx:showNotification', _source, '~r~C\'est pas bien de glitch ~w~')
	--	PlayersSelling[_source]=false
	--else
		PlayersSelling[_source]=true
		TriggerClientEvent('esx:showNotification', _source, 'Vous êtes entrain de vendre vos Cigarettes au commeçant.')
		Sell(_source, zone)
	--end

end)

RegisterServerEvent('esx_tabacjob:stopSell')
AddEventHandler('esx_tabacjob:stopSell', function()

	local _source = source

	--if PlayersSelling[_source] == true then
		PlayersSelling[_source]=false
		TriggerClientEvent('esx:showNotification', _source, 'Vous êtes ne vendez plus de Cigarettes au commeçant.')

	--else
	--	TriggerClientEvent('esx:showNotification', _source, 'Vous pouvez à nouveau ~g~vendre.')
	--	PlayersSelling[_source]=true
	--end

end)

RegisterServerEvent('esx_tabacjob:getStockItem')
AddEventHandler('esx_tabacjob:getStockItem', function(itemName, count)

	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_tabac', function(inventory)

		local item = inventory.getItem(itemName)

		if item.count >= count then
			inventory.removeItem(itemName, count)
			xPlayer.addInventoryItem(itemName, count)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
		end

		TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_withdrawn') .. count .. ' ' .. item.label)

	end)

end)

ESX.RegisterServerCallback('esx_tabacjob:getStockItems', function(source, cb)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_tabac', function(inventory)
		cb(inventory.items)
	end)

end)

RegisterServerEvent('esx_tabacjob:putStockItems')
AddEventHandler('esx_tabacjob:putStockItems', function(itemName, count)

	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_tabac', function(inventory)

		local item = inventory.getItem(itemName)

		if item.count >= 0 then
			xPlayer.removeInventoryItem(itemName, count)
			inventory.addItem(itemName, count)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
		end

		TriggerClientEvent('esx:showNotification', xPlayer.source, _U('added') .. count .. ' ' .. item.label)

	end)
end)

ESX.RegisterServerCallback('esx_tabacjob:getPlayerInventory', function(source, cb)

	local xPlayer    = ESX.GetPlayerFromId(source)
	local items      = xPlayer.inventory

	cb({
		items      = items
	})

end)

RegisterServerEvent('esx_tabacjob:annonce')
AddEventHandler('esx_tabacjob:annonce', function(result)
  local _source  = source
  local xPlayer  = ESX.GetPlayerFromId(_source)
  local xPlayers = ESX.GetPlayers()
  local text     = result
  print(text)
  for i=1, #xPlayers, 1 do
    local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
    TriggerClientEvent('esx_tabacjob:annonce', xPlayers[i],text)
  end

  Wait(8000)

  local xPlayers = ESX.GetPlayers()
  for i=1, #xPlayers, 1 do
    local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
    TriggerClientEvent('esx_tabacjob:annoncestop', xPlayers[i])
  end

end)

---------------------------
---- UTILISER CIGARETTE ----
----------------------------
ESX.RegisterUsableItem('malbora', function(source)

    local xPlayer = ESX.GetPlayerFromId(source)

    xPlayer.removeInventoryItem('malbora', 1)

    TriggerClientEvent('esx_tabac:onSmokeCig', source)
    TriggerClientEvent('esx:showNotification', source, 'Vous fumez ~g~1x ~b~Malboro')

end)

ESX.RegisterUsableItem('gitanes', function(source)

    local xPlayer = ESX.GetPlayerFromId(source)

    xPlayer.removeInventoryItem('gitanes', 1)

    TriggerClientEvent('esx_tabac:onSmokeCig', source)
    TriggerClientEvent('esx:showNotification', source, 'Vous fumez ~g~1x ~b~Lucky Strike')

end)

ESX.RegisterUsableItem('splif', function(source)

  local _source  = source
  local xPlayer  = ESX.GetPlayerFromId(_source)

  xPlayer.removeInventoryItem('splif', 1)
  
  TriggerClientEvent('esx_tabac:onSmokeSplif', _source)
  TriggerClientEvent('esx_status:add', source, 'thirst', -80000)
  TriggerClientEvent('esx_status:add', source, 'hunger', -80000)
  TriggerClientEvent('esx:showNotification', source, 'Vous fumez ~g~1x Calumet de la paix')
  TriggerClientEvent('esx_status:add', source, 'drug', 150000)

end)

--ESX.RegisterUsableItem('jus_raisin', function(source)

--	local xPlayer = ESX.GetPlayerFromId(source)

--	xPlayer.removeInventoryItem('jus_raisin', 1)

--	TriggerClientEvent('esx_status:add', source, 'hunger', 40000)
--	TriggerClientEvent('esx_status:add', source, 'thirst', 120000)
--	TriggerClientEvent('esx_basicneeds:onDrink', source)
--	TriggerClientEvent('esx:showNotification', source, _U('used_jus'))

--end)

--ESX.RegisterUsableItem('grand_cru', function(source)

--	local xPlayer = ESX.GetPlayerFromId(source)

--	xPlayer.removeInventoryItem('grand_cru', 1)

--	TriggerClientEvent('esx_status:add', source, 'drunk', 400000)
--	TriggerClientEvent('esx_basicneeds:onDrink', source)
--	TriggerClientEvent('esx:showNotification', source, _U('used_grand_cru'))

--end)