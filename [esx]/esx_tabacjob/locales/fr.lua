-----------------------------------------
-- Created and modify by L'ile Légale RP
-- SenSi and Kaminosekai
-----------------------------------------
Locales['fr'] = {
	-- General
	['cloakroom'] = 'Vestiaire',
	['vine_clothes_civil'] = 'Tenue Civil',
	['vine_clothes_vine'] = 'Tenue de travail',
	['veh_menu'] = 'Garage',
	['spawn_veh'] = 'Appuyez sur ~INPUT_CONTEXT~ pour sortir véhicule',
	['amount_invalid'] = 'montant invalide',
	['press_to_open'] = 'Appuyez sur ~INPUT_CONTEXT~ pour accéder au menu',
	['billing'] = 'facuration',
	['invoice_amount'] = 'montant de la facture',
	['no_players_near'] = 'aucun joueur à proximité',
	['store_veh'] = 'Appuyez sur ~INPUT_CONTEXT~ pour ranger le véhicule',
	['comp_earned'] = 'La société a gagné ~g~$',
	['deposit_stock'] = 'Déposer Stock',
	['take_stock'] = 'Prendre Stock',
	['boss_actions'] = 'action Patron',
	['quantity'] = 'Quantité',
	['quantity_invalid'] = '~r~Quantité invalide~w~',
	['inventory'] = 'Inventaire',
	['have_withdrawn'] = 'Vous avez retiré x',
	['added'] = 'Vous avez ajouté x',
	['not_enough_place'] = 'Vous n\'avez plus de place',
	['sale_in_prog'] = 'Revente en cours...',
	['van'] = 'Camion de travail',
	['open_menu'] = ' ',
  
	-- A modifier selon l'entreprise
	['tabac_client'] = 'Client Tabac',
	['no_tabacsec_sale'] = 'Vous n\'avez plus ou pas assez de tabac sec',
	['no_vin_sale'] = 'Vous n\'avez plus ou pas assez de vin',
	['not_enough_feuilletab'] = 'Vous n\'avez plus de feuilles de tabac',
	['tabacsec'] = '~g~ tabac!~w~',
	['no_product_sale'] = 'Vous n\'avez plus de produits',
	['press_traitement_jus'] = 'appuyez sur ~INPUT_CONTEXT~ pour transformer votre tabac',
	['used_jus'] = 'Vous avez bu du jus de raisin',
	['used_tabacsec'] = 'Vous avez bu une bouteille de grand cru',

	-- NOUVELLES TRADUCTIONS
	['recolte_tabac_brun'] = 'Appuyez sur ~INPUT_CONTEXT~ pour récolter du Tabac Brun',
	['recolte_tabac_blond'] = 'Appuyez sur ~INPUT_CONTEXT~ pour récolter du Tabac Blond',
	['traitement_tabac_brun'] = 'Appuyez sur ~INPUT_CONTEXT~ pour sécher votre Tabac Brun',
	['traitement_tabac_blond'] = 'Appuyez sur ~INPUT_CONTEXT~ pour sécher votre Tabac Blond',
	['roulage_gitanes'] = 'Appuyez sur ~INPUT_CONTEXT~ pour rouler des gitanes',
	['roulage_malbora'] = 'Appuyez sur ~INPUT_CONTEXT~ pour rouler des cigarettes',
	['roulage_splif'] = 'Appuyez sur ~INPUT_CONTEXT~ pour rouler des Splif',
	['vente_malbora'] = 'Appuyez sur ~INPUT_CONTEXT~ pour vendre des Malbora',
	['vente_gitanes'] = 'Appuyez sur ~INPUT_CONTEXT~ pour vendre des Lucky Strike',
	['recolte_notif'] = 'Vous récoltez du Tabac!',
	['not_enough_tabac'] = 'Vous n\'avez pas assez de Tabac',
	['not_enough_tabac_sec'] = 'Vous n\'avez pas assez de Tabac Sec',
	['treating_in_progress'] = 'Vous êtes en train de traiter',
}