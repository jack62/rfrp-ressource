Config                            = {}
Config.DrawDistance               = 100.0
Config.MaxInService               = -1
Config.EnablePlayerManagement     = true
Config.EnableSocietyOwnedVehicles = false
Config.Locale                     = 'fr'

Config.Zones = {

	RecolteTabacBrun = {
		Pos   = {x = 268.4737, y = 6478.4584, z = 29.70},
		Size  = {x = 3.5, y = 3.5, z = 2.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Récolte de Feuilles de Tabac Brun",
		Type  = 27
	},

	RecolteTabacBlond = {
		Pos   = {x = 333.54852294922, y = 6647.189453125, z = 28.808355331421},
		Size  = {x = 3.5, y = 3.5, z = 2.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Récolte de Feuilles de Tabac Blond",
		Type  = 27
	},

	TraitementBlond = {
		Pos   = { x = 2349.6174316406, y = 3137.5869140625, z = 48.2087059021 },
	    Size  = { x = 1.5, y = 1.5, z = 1.0 },
	    Color = { r = 204, g = 204, b = 0 },
	    Name  = "Traitement du Tabac Blond",
	    Type  = 27,
	},

	TraitementBrun = {
		Pos   = { x = 2348.3198242188, y = 3128.3188476563, z = 48.2087059021  },
	    Size  = { x = 1.5, y = 1.5, z = 1.0 },
	    Color = { r = 204, g = 204, b = 0 },
		Name  = "Traitement du Tabac Brun",
		Type  = 27
	},

	TraitementMalboro = {
		Pos   = {x = 2055.0080566406, y = 3186.6091308594, z = 45.186500549316 },
		Size  = {x = 3.5, y = 3.5, z = 2.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Roulage des Malboro",
		Type  = 27
	},

	TraitementGitanes = {
		Pos   = {x = 2058.6774902344, y = 3185.9069824219, z = 45.186492919922},
		Size  = {x = 3.5, y = 3.5, z = 2.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Roulage des Lucky Strike",
		Type  = 27
	},

	tabacActions = {
		Pos   = { x = 2340.6191, y = 3124.6501, z = 48.0087 },
	    Size  = { x = 1.5, y = 1.5, z = 1.0 },
	    Color = { r = 204, g = 204, b = 0 },
		Name  = "Point d'action",
		Type  = 23
	},

	TraitementSplif = {
		Pos   = { x = 2483.3996582031, y = 3446.1877441406, z = 51.068393707275 },
		Size  = { x = 3.5, y = 3.5, z = 2.0 },
		Color = { r = 136, g = 243, b = 216 },
		Name  = "Roulage du Splif",
		Type  = 0
	},
	  
	VehicleSpawner = {
		Pos   = { x = 2364.3635253906, y = 3136.5427246094, z = 48.2112159729  },
	    Size  = { x = 1.5, y = 1.5, z = 1.0 },
	    Color = { r = 204, g = 204, b = 0 },
		Name  = "Garage véhicule",
		Type  = 0
	},

	VehicleSpawnPoint = {
		Pos   = { x = 2371.8051, y = 3127.9262, z = 48.1384 },
    	Size  = { x = 1.5, y = 1.5, z = 1.0 },
		Name  = "Spawn point",
		Type  = -1
	},

	VehicleDeleter = {
		Pos   = { x = 2363.0761, y = 3113.7097, z = 48.2520 },
	    Size  = { x = 3.0, y = 3.0, z = 1.0 },
	    Color = { r = 204, g = 204, b = 0 },
		Name  = "Ranger son véhicule",
		Type  = 0
	},

	SellMalbora = {
		Pos   = {x = -57.956474304199, y = -1747.3608398438, z = 29.314598083496},
		Size  = {x = 3.5, y = 3.5, z = 2.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Vente des Marlboro",
		Type  = 27
	},
		
	SellGitanes = {
		Pos   = {x = 371.9794921875, y = 337.72451782227, z = 103.35970306396},
		Size  = {x = 3.5, y = 3.5, z = 2.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Vente des Lucky Strike",
		Type  = 27
	}
}