Locales['en'] = {
	['gunrange'] = " Stand de Tir ",
	['actionMessage'] = "Appuis sur ~INPUT_PICKUP~ pour ouvrir le menu.",
	['difficulty'] = " Niveau ",
	['easy'] = " Noob ",
	['normal'] = " Test PPA  ",
	['hard'] = " Mac Cree ",
	['harder'] = " Lucky Luck ",
	['impossible'] = " Mac Giver ",
	['targets'] = " Cibles ",
	['points'] = " Points ",
	['you_got'] = "Vous avez ",
	['wait_for_turn'] = "~o~ Attends ton tour!",
	['point'] = " Points",

	--Scores
	['show_board'] = "Appuie sur ~INPUT_PICKUP~",
	['last_10'] = "10 Derniers",
	['name'] = "Nom : ",
	['difficulty_2'] = "Niveau : ",
	['targets_2'] = "Cibles : ",
	["points_2"] = "Score : ",
}
