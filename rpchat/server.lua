function getIdentity(source)
    local identifier = GetPlayerIdentifiers(source)[1]
    local result = MySQL.Sync.fetchAll("SELECT * FROM characters WHERE identifier = @identifier", {
        ['@identifier'] = identifier
    })
  if result[1] ~= nil then
    local identity = result[1]

    return {
      firstname   = identity['firstname'],
      lastname  = identity['lastname'],
      dateofbirth = identity['dateofbirth'],
      sex   = identity['sex'],
      height    = identity['height']
    }
  else
    return {
      firstname   = '',
      lastname  = '',
      dateofbirth = '',
      sex   = '',
      height    = ''
    }
    end
end

AddEventHandler('chatMessage', function(source, name, message)
    if string.sub(message,1,string.len("/"))=="/" then
        --ne doit rien se passer c'est une commande
    else
    local name = getIdentity(source)
        TriggerClientEvent("sendProximityMessage", -1, source, name.firstname, message)
		PerformHttpRequest('https://discordapp.com/api/webhooks/505739776603717636/nH1HSfGEto8uBympAcmJAPAIlWW-HcGhvNMlwpAra0TNtlOghUF5Ez1ZwwV9g9EZfXqc', function(err, text, headers) end, 'POST', json.encode({username = name, content = message}), { ['Content-Type'] = 'application/json' })

    end
    CancelEvent()
end)

TriggerEvent('es:addCommand', 'me', function(source, args, user)
  table.remove(args, 1)
  --local pname = GetPlayerName(source)
  local name = getIdentity(source)
  TriggerClientEvent("sendProximityMessageMe", -1, source , name.firstname, table.concat(args, " "))
end)

TriggerEvent('es:addCommand', 'do', function(source, args, user)
  table.remove(args, 1)
  local name = getIdentity(source)
  TriggerClientEvent("sendProximityMessageDo", -1, source, name.firstname, table.concat(args, " "))
end)

AddEventHandler('chatMessage', function(source, name, msg)
  if msg:sub(1, 1) == "/" then
    sm = stringsplit(msg, " ");
     if sm[1] == "/tweet" then
      local identity = getIdentity(source)
         CancelEvent()
  TriggerClientEvent('chatMessage', -1, "^0[^4Twitter^0]", {30, 144, 255}, " ^3" .. identity.firstname .. " " .. identity.lastname .."^0: " .. string.sub(msg,7))
     end
  end
end)

AddEventHandler('chatMessage', function(source, name, msg)
  sm = stringsplit(msg, " ");
  if sm[1] == "/ooc" then
    CancelEvent()
    TriggerClientEvent('chatMessage', -1, "OOC | " .. name, { 128, 128, 128 }, string.sub(msg,5))
  end
end)

function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

function makeArgs(cmd)
    args = {}
    for i = 2, #cmd, 1 do
        table.insert(args, cmd[i])
    end
    return args
end

