resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

description "Add-On Ped Skinchange Menu" -- Resource Descrption

dependencies {
    'NativeUI',
}

client_script {
	'@NativeUI/NativeUI.lua',
	'client/preload.lua',
	'config.lua',
	'client/client.lua',
}

server_script {
	'server/server.lua',
}

-- Add-On Ped Skinchange Menu Made By: FlatracerMOD (aka Flatracer)
