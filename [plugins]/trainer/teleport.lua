RegisterNetEvent("TeleportMe") --Just Don't Edit!
RegisterNetEvent("Effect") --Just Don't Edit!
local nv, wp, player, op, optome, metoadmin
local entity = 0
local text = "placeholder"
local zHeigt = 0.0
local pos = 0.0, 0.0, 0.0
local height = 1000.0
local scale = 0.0
local Admin = 666

RegisterNUICallback("teleport", function(data, cb) --Teleportation Options
	local action = data.action
	local playerPed = GetPlayerPed(-1)
	local playerVeh = GetVehiclePedIsIn(playerPed, 0)
	local playerPedPos = GetEntityCoords(playerPed, true)
	local playerVehPos = GetEntityCoords(playerVeh, true)
	
	if action == "nearestcar" then --Teleport Into Nearest Car
		nv = true
	elseif action == "waypoint" then --Teleport To Waypoint
		if DoesBlipExist(GetFirstBlipInfoId(8)) then
		
			local blipIterator = GetBlipInfoIdIterator(8)
			local blip = GetFirstBlipInfoId(8, blipIterator)
			local coords = Citizen.InvokeNative(0xFA7C7F0AADF25D09, blip, Citizen.ResultAsVector()) --Thanks To Briglair [forum.FiveM.net]
			
			if IsPedInAnyVehicle(playerPed, 0) and (GetPedInVehicleSeat(playerVeh, -1) == playerPed) then
				entity = playerVeh
				text = "With Vehicle"
			else
				entity = playerPed
				text = "On Foot"
			end
			zHeigt = 0.0
			height = 1000.0
			SetEntityCoords(entity, coords.x, coords.y, height)
			FreezeEntityPosition(entity, true)
			drawNotification("~y~Wait...")
			wp = true
			
		else
			drawNotification("~r~No Waypoint Set!")
		end
	elseif action == "forward" then --Teleport Forward
		if IsPedInAnyVehicle(playerPed, 0) and (GetPedInVehicleSeat(playerVeh, -1) == playerPed) then
			SetEntityCoords(playerVeh, playerVehPos.x + (3 * GetEntityForwardX(playerVeh)), playerVehPos.y + (3 * GetEntityForwardY(playerVeh)), playerVehPos.z - 0.75)
		else
			SetEntityCoords(playerPed, playerPedPos.x + (3 * GetEntityForwardX(playerPed)), playerPedPos.y + (3 * GetEntityForwardY(playerPed)), playerPedPos.z - 0.75)
		end
		drawNotification("~g~Teleported Forward")
	end
	cb("ok")
end)

RegisterNUICallback("onlineplayertele", function(data, cb) --Online Player Teleportation Options
	player = tonumber(data.action)
	local localplayerCoords = GetEntityCoords(GetPlayerPed(-1), true)
	local playerCoords = GetEntityCoords(GetPlayerPed(player), true)
	
	if GetPlayerPed(player) ~= 0 then
		if GetPlayerPed(player) ~= GetPlayerPed(-1) then
			if IsPedInAnyVehicle(GetPlayerPed(-1), false) and (GetPedInVehicleSeat(GetVehiclePedIsIn(GetPlayerPed(-1), 0), -1) == GetPlayerPed(-1)) then
				SetEntityCoords(GetVehiclePedIsIn(GetPlayerPed(-1), 0), 0.0, 0.0, 0.0)
				FreezeEntityPosition(GetVehiclePedIsIn(GetPlayerPed(-1), true), true)
			else
				SetEntityCoords(GetPlayerPed(-1), 0.0, 0.0, 0.0)
				FreezeEntityPosition(GetPlayerPed(-1), true)
			end
			op = true
		else
			drawNotification("~r~Can't Teleport To Yourself!")
		end
	else
		drawNotification("~r~Player " .. player + 1 .. " Doesn't Exist!")
	end
		
		
	cb("ok")
end)

RegisterNUICallback("onlineplayerteletome", function(data, cb) --Online Player To Me Teleportation Options
	player = tonumber(data.action)
	local PlayerPedPos = GetEntityCoords(GetPlayerPed(-1), true)
	
	if admin == true then
		if GetPlayerPed(player) ~= 0 then
			if GetPlayerPed(player) ~= GetPlayerPed(-1) then
				optome = true
			else
				drawNotification("~r~Can't Teleport Yourself!")
			end
		else
			drawNotification("~r~Player " .. player + 1 .. " Doesn't Exist!")
		end
	else
		drawNotification("~r~Only Works For Admins!")
	end
		
	cb("ok")
end)

Citizen.CreateThread(function() --Teleport Online Player To Me
	while true do
		Citizen.Wait(0)
		
		if (optome == true) then
			local sender = GetPlayerIndex()
			TriggerServerEvent("TeleOtherPlayer", sender, player)
			drawNotification("~g~Teleported Player To You!")
			optome = false
		end
	end
end)

AddEventHandler("TeleportMe", function(Player, OtherPlayer) --Just Don't Edit!
	local playerCoords = GetEntityCoords(GetPlayerPed(Player), true)
	if GetPlayerPed(-1) == GetPlayerPed(OtherPlayer) then
		Admin = Player
		metoadmin = true
	end
end)

AddEventHandler("Effect", function(Entitiy) --Just Don't Edit!
	if IsEntityAPed(Entitiy) then
		scale = 4.0
	else
		scale = 15.0
	end
	if not HasNamedPtfxAssetLoaded("scr_rcbarry1") then
		RequestNamedPtfxAsset("scr_rcbarry1")
	end
	while not HasNamedPtfxAssetLoaded("scr_rcbarry1") do
		Citizen.Wait(0)
	end
	SetPtfxAssetNextCall("scr_rcbarry1")
	StartParticleFxLoopedOnEntity("scr_alien_teleport", Entitiy, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, scale, false, false, false)
end)

Citizen.CreateThread(function() --Teleport To Online Player
	while true do
		Citizen.Wait(0)
		local playerCoords = GetEntityCoords(GetPlayerPed(player), true)
		local localplayerCoords = GetEntityCoords(GetPlayerPed(-1), true)
		local localvehCoords = GetEntityCoords(GetVehiclePedIsIn(GetPlayerPed(-1), 1), true)
		
		if (op == true) then
			if (localvehCoords.z == 0.0) or (localplayerCoords.z == 0.0) then
				while playerCoords.z == 0.0 do
					playerCoords = GetEntityCoords(GetPlayerPed(player), true)
				end
				if IsPedInAnyVehicle(GetPlayerPed(-1), false) and (GetPedInVehicleSeat(GetVehiclePedIsIn(GetPlayerPed(-1), false), -1) == GetPlayerPed(-1)) then
					FreezeEntityPosition(GetVehiclePedIsIn(GetPlayerPed(-1), false))
					SetEntityCoords(GetVehiclePedIsIn(GetPlayerPed(-1), false), playerCoords)
				else
					if IsPedInAnyVehicle(GetPlayerPed(player), true) and IsAnyVehicleSeatEmpty(GetVehiclePedIsIn(GetPlayerPed(player), false)) then
						local playerVeh = GetVehiclePedIsIn(GetPlayerPed(player), false)
						local seat = GetVehicleModelMaxNumberOfPassengers(GetEntityModel(playerVeh)) - 2
						local seatindex = -1
						while seatindex <= seat and ((IsVehicleSeatFree(playerVeh, seatindex)) == false) do
							Citizen.Wait(0)
							seatindex = seatindex + 1
						end
						FreezeEntityPosition(GetPlayerPed(-1), false)
						SetPedIntoVehicle(GetPlayerPed(-1), playerVeh, seatindex)
						drawNotification("~g~Teleported Into " .. player + 1 .. ". Players Vehicle!")
					else
						FreezeEntityPosition(GetPlayerPed(-1), false)
						SetEntityCoords(GetPlayerPed(-1), playerCoords)
						drawNotification("~g~Teleported To " .. player + 1 .. ". Player!")
					end
				end
			end
			op = false
		end
	end
end)

Citizen.CreateThread(function() --Teleport Player To Admin
	while true do
		Citizen.Wait(0)
		local playerCoords = GetEntityCoords(GetPlayerPed(player), true)
		local localplayerCoords = GetEntityCoords(GetPlayerPed(-1), true)
		local localvehCoords = GetEntityCoords(GetVehiclePedIsIn(GetPlayerPed(-1), 1), true)
		
		if (metoadmin == true) then
			if IsPedInAnyVehicle(GetPlayerPed(Admin), true) and IsAnyVehicleSeatEmpty(GetVehiclePedIsIn(GetPlayerPed(Admin), false)) then
				local playerVeh = GetVehiclePedIsIn(GetPlayerPed(Admin), false)
				local seat = GetVehicleModelMaxNumberOfPassengers(GetEntityModel(playerVeh)) - 2
				local seatindex = -1
				while seatindex <= seat and ((IsVehicleSeatFree(playerVeh, seatindex)) == false) do
					Citizen.Wait(0)
					seatindex = seatindex + 1
				end
				SetPedIntoVehicle(GetPlayerPed(-1), playerVeh, seatindex)
			else
				SetEntityCoords(GetPlayerPed(-1), playerCoords)
			end
			drawNotification("~y~You Got Teleporten To An Admin!")
			metoadmin = false
		end
	end
end)

Citizen.CreateThread(function() --Teleport Into Nearest Car
	while true do
		Citizen.Wait(0)
		
		local playerPed = GetPlayerPed(-1)
		local playerPedPos = GetEntityCoords(playerPed, true)
		local NearestVehicle = GetClosestVehicle(GetEntityCoords(playerPed, true), 1000.0, 0, 4)
		local NearestVehiclePos = GetEntityCoords(NearestVehicle, true)
		local NearestPlane = GetClosestVehicle(GetEntityCoords(playerPed, true), 1000.0, 0, 16384)
		local NearestPlanePos = GetEntityCoords(NearestPlane, true)
		if (nv == true) then
			drawNotification("~y~Wait...")
			Citizen.Wait(1000)
			if (NearestVehicle == 0) and (NearestPlane == 0) then
				drawNotification("~r~No Vehicle Found")
			elseif (NearestVehicle == 0) and (NearestPlane ~= 0) then
				if IsVehicleSeatFree(NearestPlane, -1) then
					SetPedIntoVehicle(playerPed, NearestPlane, -1)
					SetVehicleAlarm(NearestPlane, false)
					SetVehicleDoorsLocked(NearestPlane, 1)
					SetVehicleNeedsToBeHotwired(NearestPlane, false)
				else
					local driverPed = GetPedInVehicleSeat(NearestPlane, -1)
					ClearPedTasksImmediately(driverPed)
					SetEntityAsMissionEntity(driverPed, 1, 1)
					DeleteEntity(driverPed)
					SetPedIntoVehicle(playerPed, NearestPlane, -1)
					SetVehicleAlarm(NearestPlane, false)
					SetVehicleDoorsLocked(NearestPlane, 1)
					SetVehicleNeedsToBeHotwired(NearestPlane, false)
				end
				drawNotification("~g~Teleported Into Nearest Vehicle!")
			elseif (NearestVehicle ~= 0) and (NearestPlane == 0) then
				if IsVehicleSeatFree(NearestVehicle, -1) then
					SetPedIntoVehicle(playerPed, NearestVehicle, -1)
					SetVehicleAlarm(NearestVehicle, false)
					SetVehicleDoorsLocked(NearestVehicle, 1)
					SetVehicleNeedsToBeHotwired(NearestVehicle, false)
				else
					local driverPed = GetPedInVehicleSeat(NearestVehicle, -1)
					ClearPedTasksImmediately(driverPed)
					SetEntityAsMissionEntity(driverPed, 1, 1)
					DeleteEntity(driverPed)
					SetPedIntoVehicle(playerPed, NearestVehicle, -1)
					SetVehicleAlarm(NearestVehicle, false)
					SetVehicleDoorsLocked(NearestVehicle, 1)
					SetVehicleNeedsToBeHotwired(NearestVehicle, false)
				end
				drawNotification("~g~Teleported Into Nearest Vehicle!")
			elseif (NearestVehicle ~= 0) and (NearestPlane ~= 0) then
				if Vdist(NearestVehiclePos.x, NearestVehiclePos.y, NearestVehiclePos.z, playerPedPos.x, playerPedPos.y, playerPedPos.z) < Vdist(NearestPlanePos.x, NearestPlanePos.y, NearestPlanePos.z, playerPedPos.x, playerPedPos.y, playerPedPos.z) then
					if IsVehicleSeatFree(NearestVehicle, -1) then
						SetPedIntoVehicle(playerPed, NearestVehicle, -1)
						SetVehicleAlarm(NearestVehicle, false)
						SetVehicleDoorsLocked(NearestVehicle, 1)
						SetVehicleNeedsToBeHotwired(NearestVehicle, false)
					else
						local driverPed = GetPedInVehicleSeat(NearestVehicle, -1)
						ClearPedTasksImmediately(driverPed)
						SetEntityAsMissionEntity(driverPed, 1, 1)
						DeleteEntity(driverPed)
						SetPedIntoVehicle(playerPed, NearestVehicle, -1)
						SetVehicleAlarm(NearestVehicle, false)
						SetVehicleDoorsLocked(NearestVehicle, 1)
						SetVehicleNeedsToBeHotwired(NearestVehicle, false)
					end
				elseif Vdist(NearestVehiclePos.x, NearestVehiclePos.y, NearestVehiclePos.z, playerPedPos.x, playerPedPos.y, playerPedPos.z) > Vdist(NearestPlanePos.x, NearestPlanePos.y, NearestPlanePos.z, playerPedPos.x, playerPedPos.y, playerPedPos.z) then
					if IsVehicleSeatFree(NearestPlane, -1) then
						SetPedIntoVehicle(playerPed, NearestPlane, -1)
						SetVehicleAlarm(NearestPlane, false)
						SetVehicleDoorsLocked(NearestPlane, 1)
						SetVehicleNeedsToBeHotwired(NearestPlane, false)
					else
						local driverPed = GetPedInVehicleSeat(NearestPlane, -1)
						ClearPedTasksImmediately(driverPed)
						SetEntityAsMissionEntity(driverPed, 1, 1)
						DeleteEntity(driverPed)
						SetPedIntoVehicle(playerPed, NearestPlane, -1)
						SetVehicleAlarm(NearestPlane, false)
						SetVehicleDoorsLocked(NearestPlane, 1)
						SetVehicleNeedsToBeHotwired(NearestPlane, false)
					end
				end
				drawNotification("~g~Teleported Into Nearest Vehicle!")
			end
			nv = false
		end
	end
end)

Citizen.CreateThread(function() --Teleport To Waypoint
	while true do
		Citizen.Wait(0)
		
		if (wp == true) then
			if zHeigt == 0.0 then
				Citizen.Wait(0)
				Pos = GetEntityCoords(entity, true)
				height = height - 50.0
				TriggerServerEvent("EffectForAll", entity)
				SetEntityCoords(entity, Pos.x, Pos.y, height)
				zHeigt = getGroundZ(Pos.x, Pos.y, Pos.z)
			else
				TriggerServerEvent("EffectForAll", entity)
				SetEntityCoords(entity, Pos.x, Pos.y, zHeigt)
				FreezeEntityPosition(entity, false)
				drawNotification("~g~Teleported To Waypoint " .. text .."!")
				wp = false
			end
		end
	end
end)

function getGroundZ(x, y, z)
	local result, groundZ = GetGroundZFor_3dCoord(x+0.0, y+0.0, z+0.0, Citizen.ReturnResultAnyway())
	return groundZ
end