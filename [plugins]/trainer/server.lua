RegisterServerEvent("ID") --Just Don't Edit!
RegisterServerEvent("GetID") --Just Don't Edit!
RegisterServerEvent("TeleOtherPlayer") --Just Don't Edit!
RegisterServerEvent("EffectForAll") --Just Don't Edit!

local admins = { --Add Steam Identifiers In The Given Format For Admins
	"steam:110000106cd0cc7",
	"steam:11000011877065b",
	"steam:110000108c6c763",
	"steam:110000102c35b16",
	"steam:11000010f4260d8",
	"steam:110000106bb6ef7",
	"steam:11000010dd4b413",
	"steam:1100001016c3b4c",
}

AddEventHandler("ID", function()
	local ID = GetPlayerIdentifiers(source)[1]
	for k, steamidadmin in pairs(admins) do
		if steamidadmin == ID then
			print("Admin (" .. ID .. ") spawned. Access to Scorpion Trainer granted!")
			TriggerClientEvent("admin", source)
		end
	end
end)

AddEventHandler("GetID", function() --Just Don't Edit!
	local ID = GetPlayerIdentifiers(source)[1]
	print("" .. ID .. "")
end)

--[[	To get your Identifier:

				Add the Trainer to your Server Resources, run FiveM and join YOUR Server. Once your Ped Spawned, press the following Buttoncombination:

				--->>>		Button for VEHICLE AIM + Button for PED COVER + Button for PED SPRINT.		<<<---

				It Outputs the Identifier in your RCON Log. Edit this File (Replace steam:110000XXXXXXXXX with your Identifier)

]]

AddEventHandler("TeleOtherPlayer", function(localPlayer, OtherPlayer)
	local LPlayer = localPlayer
	local OPlayer = OtherPlayer
	TriggerClientEvent("TeleportMe", -1, LPlayer, OPlayer)
end)

AddEventHandler("EffectForAll", function(Entity)
	local entity = Entity
	TriggerClientEvent("Effect", -1, entity)
end)