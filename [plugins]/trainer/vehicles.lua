local despawnable, autodelete, mapblip, vehgodmode,
	  vehvisibiledamage, flyingvehicle, reducevehgrip,
	  setvisible, setgravity, boostcar, seatbelt,
	  Object, showextras, frontleft, frontright,
	  rearleft, rearright, hood, trunk, needforspeed, 
	  bunnyhop
	  
function SpawnVehicle(model, x, y, z) --Vehicle Spawning Function
	local playerPed = GetPlayerPed(-1)
	local playerHeading = GetEntityHeading(playerPed)
	local playerVeh = GetVehiclePedIsIn(playerPed, true)
	local playerPedPos = GetEntityCoords(playerPed, true)

	if autodelete then
		if IsPedInAnyVehicle(playerPed, true) then
			SetEntityAsMissionEntity(Object, 1, 1)
			SetEntityAsMissionEntity(playerVeh, 1, 1)
			DeleteEntity(Object)
			DeleteVehicle(playerVeh)
			drawNotification("~g~Old Vehicle Deleted!")
		end
	end
	if IsModelValid(model) then
		RequestModel(model)
		while not HasModelLoaded(model) do
			Citizen.Wait(0)
			blockinput = true
		end
		local veh = CreateVehicle(model, x, y, z + 1, playerHeading, true, true)
		SetPedIntoVehicle(playerPed, veh, -1)
		if mapblip then
			local vehblip = AddBlipForEntity(veh)
			if IsPedInAnySub(playerPed) then  --Submersible Blips (Submersible & Kraken)
				SetBlipSprite(vehblip, 308)
			elseif IsPedInAnyTaxi(playerPed) then  --Taxi Blip
				SetBlipSprite(vehblip, 198)
				SetBlipColour(vehblip, 5)
			elseif (GetVehicleClass(veh) == 8) or (GetVehicleClass(veh) == 13) then --Motorcycle & Bike Blips
				if (model == GetHashKey("POLICEB")) then --Police Bike Blip
					SetBlipSprite(vehblip, 226)
					SetBlipColour(vehblip, 63)
				else
					SetBlipSprite(vehblip, 226) --Motorcycle Blips
				end
			elseif (GetVehicleClass(veh) == 10) or (GetVehicleClass(veh) == 11) or (GetVehicleClass(veh) == 17) or (GetVehicleClass(veh) == 20) then  --Industrial, Utility, Service & Commercial Blips
				if (model == GetHashKey("TRASH")) or (model == GetHashKey("TRASH2")) then --Trash Blips
					SetBlipSprite(vehblip, 318)
				elseif (model == GetHashKey("SADLER")) or (model == GetHashKey("SADLER2")) or (model == GetHashKey("GUARDIAN")) then --Sadler & Guardian Blips
					SetBlipSprite(vehblip, 227)
				else
					SetBlipSprite(vehblip, 67) --Industrial, Utility, Service & Commercial Blips
				end
			elseif (GetVehicleClass(veh) == 14) then --Boat Blips
				if (model == GetHashKey("MARQUIS")) then --Marquis Blip
					SetBlipSprite(vehblip, 410)
				else
					SetBlipSprite(vehblip, 427) --Boat Blips
				end
			elseif (GetVehicleClass(veh) == 16) then --Plane Blips
				if (model == GetHashKey("BESRA")) or (model == GetHashKey("HYDRA")) or (model == GetHashKey("LAZER")) then --Jet Blips
					SetBlipSprite(vehblip, 424)
				elseif (model == GetHashKey("BLIMP")) or (model == GetHashKey("BLIMP2")) then --Blimp Blips
					SetBlipSprite(vehblip, 401)
				elseif (model == GetHashKey("CUBAN800")) or (model == GetHashKey("DODO"))or (model == GetHashKey("DUSTER"))or (model == GetHashKey("STUNT"))or (model == GetHashKey("MAMMATUS"))or (model == GetHashKey("VELUM"))or (model == GetHashKey("VELUM2"))or (model == GetHashKey("VESTRA")) then --Small Plane Blip
					SetBlipSprite(vehblip, 251)
				else
					SetBlipSprite(vehblip, 423) --Large Plane Blips
				end
			elseif (GetVehicleClass(veh) == 18) then --Emergency Blips
				if (model == GetHashKey("LGUARD")) or (model == GetHashKey("PRANGER")) then --Lifeguard & Park Ranger - Normal Blip
					SetBlipSprite(vehblip, 227)
				else
					SetBlipSprite(vehblip, 56) --Emergency Blips
					if (model == GetHashKey("PBUS")) then
						SetBlipColour(vehblip, 63)
					end
				end
			else --Everything Else - Normal Blip
				if (GetVehicleClass(playerVeh) ~= 15) then
					if (model == GetHashKey("RHINO")) then --Rhino Blip
						SetBlipSprite(vehblip, 421)
					elseif (model == GetHashKey("DUNE")) or (model == GetHashKey("DUNE2")) or (model == GetHashKey("DUNE4")) or (model == GetHashKey("DUNE5")) then --Dune Blip (Dune, Spacedocker 6 Ramp Buggys)
						SetBlipSprite(vehblip, 147)
					elseif (model == GetHashKey("BOXVILLE")) or (model == GetHashKey("BOXVILLE2")) or (model == GetHashKey("BOXVILLE3")) or (model == GetHashKey("BOXVILLE4")) or (model == GetHashKey("CAMPER")) or (model == GetHashKey("JOURNEY")) or (model == GetHashKey("TACO")) then --Boxville, Camper, Journey & Taco Van Blips
						SetBlipSprite(vehblip, 67)
					elseif (model == GetHashKey("INSURGENT")) or (model == GetHashKey("TECHNICAL")) or (model == GetHashKey("LIMO2")) then --Insurgent Pickup, Technical & Turreted Limo Blips
						SetBlipSprite(vehblip, 426)
					else
						SetBlipSprite(vehblip, 227) --Everything Else - Normal Blip
					end
				end
			end
		end
		if despawnable then
			SetEntityAsNoLongerNeeded(veh)
		else
			SetEntityAsMissionEntity(veh, 1, 1)
		end
		blockinput = false
		drawNotification("~g~Vehicle Spawned!")
	else
		drawNotification("~r~Invalid Model!")
	end
end

RegisterNUICallback("veh", function(data, cb) --Vehicle Options
	local action = data.action
	local newstate = data.newstate
	local playerPed = GetPlayerPed(-1)
	local playerVeh = GetVehiclePedIsIn(playerPed, true)

	if action == "fixclean" then --Fixing And Cleaning
		if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(GetVehiclePedIsIn(GetPlayerPed(-1), 0), -1) == GetPlayerPed(-1)) then
			SetVehicleFixed(playerVeh)
			SetVehicleDirtLevel(playerVeh, 0.0)
			SetVehicleLights(playerVeh, 0)
			SetVehicleBurnout(playerVeh, false)
			Citizen.InvokeNative(0x1FD09E7390A74D54, playerVeh, 0)
			drawNotification("~g~Vehicle repaired & cleaned!")
		else
			drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
		end
	elseif action == "upgrade" then --Maximum Upgrade
		if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(GetVehiclePedIsIn(GetPlayerPed(-1), 0), -1) == GetPlayerPed(-1)) then
			ClearVehicleCustomPrimaryColour(playerVeh)
			ClearVehicleCustomSecondaryColour(playerVeh)
			SetVehicleModKit(playerVeh, 0)
			SetVehicleWheelType(playerVeh, 7)
			SetVehicleMod(playerVeh, 0, GetNumVehicleMods(playerVeh, 0) - 1, true)
			SetVehicleMod(playerVeh, 1, GetNumVehicleMods(playerVeh, 1) - 1, true)
			SetVehicleMod(playerVeh, 2, GetNumVehicleMods(playerVeh, 2) - 1, true)
			SetVehicleMod(playerVeh, 3, GetNumVehicleMods(playerVeh, 3) - 1, true)
			SetVehicleMod(playerVeh, 4, GetNumVehicleMods(playerVeh, 4) - 1, true)
			SetVehicleMod(playerVeh, 5, GetNumVehicleMods(playerVeh, 5) - 1, true)
			SetVehicleMod(playerVeh, 6, GetNumVehicleMods(playerVeh, 6) - 1, true)
			SetVehicleMod(playerVeh, 7, GetNumVehicleMods(playerVeh, 7) - 1, true)
			SetVehicleMod(playerVeh, 8, GetNumVehicleMods(playerVeh, 8) - 1, true)
			SetVehicleMod(playerVeh, 9, GetNumVehicleMods(playerVeh, 9) - 1, true)
			SetVehicleMod(playerVeh, 10, GetNumVehicleMods(playerVeh, 10) - 1, true)
			SetVehicleMod(playerVeh, 11, GetNumVehicleMods(playerVeh, 11) - 1, true)
			SetVehicleMod(playerVeh, 12, GetNumVehicleMods(playerVeh, 12) - 1, true)
			SetVehicleMod(playerVeh, 13, GetNumVehicleMods(playerVeh, 13) - 1, true)
			SetVehicleMod(playerVeh, 14, 51, false)
			SetVehicleMod(playerVeh, 15, GetNumVehicleMods(playerVeh, 15) - 1, true)
			SetVehicleMod(playerVeh, 16, GetNumVehicleMods(playerVeh, 16) - 1, true)
			ToggleVehicleMod(playerVeh, 17, true)
			ToggleVehicleMod(playerVeh, 18, true)
			ToggleVehicleMod(playerVeh, 19, true)
			ToggleVehicleMod(playerVeh, 20, true)
			ToggleVehicleMod(playerVeh, 21, true)
			ToggleVehicleMod(playerVeh, 22, true)
			SetVehicleMod(playerVeh, 23, 1, true)
			SetVehicleMod(playerVeh, 24, 1, true)
			SetVehicleMod(playerVeh, 25, GetNumVehicleMods(playerVeh, 25) - 1, true)
			SetVehicleMod(playerVeh, 27, GetNumVehicleMods(playerVeh, 27) - 1, true)
			SetVehicleMod(playerVeh, 28, GetNumVehicleMods(playerVeh, 28) - 1, true)
			SetVehicleMod(playerVeh, 30, GetNumVehicleMods(playerVeh, 30) - 1, true)
			SetVehicleMod(playerVeh, 33, GetNumVehicleMods(playerVeh, 33) - 1, true)
			SetVehicleMod(playerVeh, 34, GetNumVehicleMods(playerVeh, 34) - 1, true)
			SetVehicleMod(playerVeh, 35, GetNumVehicleMods(playerVeh, 35) - 1, true)
			SetVehicleMod(playerVeh, 38, GetNumVehicleMods(playerVeh, 38) - 1, true)
			SetVehicleTyreSmokeColor(playerVeh, 0, 0, 0)
			SetVehicleWindowTint(playerVeh, 1)
			SetVehicleTyresCanBurst(playerVeh, false)
			SetVehicleNumberPlateText(playerVeh, "F-MOD")
			SetVehicleNumberPlateTextIndex(playerVeh, 5)
			SetVehicleModColor_1(playerVeh, 4, 12, 0)
			SetVehicleModColor_2(playerVeh, 4, 12)
			SetVehicleColours(playerVeh, 12, 12)
			SetVehicleExtraColours(playerVeh, 70, 141)
			drawNotification("~g~Vehicle Upgraded!")
		else
			drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
		end
	elseif action == "downgrade" then --Downgrade
		if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(GetVehiclePedIsIn(GetPlayerPed(-1), 0), -1) == GetPlayerPed(-1)) then
			SetVehicleModKit(playerVeh, 0)
			SetVehicleWheelType(playerVeh, 4)
			SetVehicleMod(playerVeh, 0, -1, 0)
			SetVehicleMod(playerVeh, 1, -1, 0)
			SetVehicleMod(playerVeh, 2, -1, 0)
			SetVehicleMod(playerVeh, 3, -1, 0)
			SetVehicleMod(playerVeh, 4, -1, 0)
			SetVehicleMod(playerVeh, 5, -1, 0)
			SetVehicleMod(playerVeh, 6, -1, 0)
			SetVehicleMod(playerVeh, 7, -1, 0)
			SetVehicleMod(playerVeh, 8, -1, 0)
			SetVehicleMod(playerVeh, 9, -1, 0)
			SetVehicleMod(playerVeh, 10, -1, 0)
			SetVehicleMod(playerVeh, 11, -1, 0)
			SetVehicleMod(playerVeh, 12, -1, 0)
			SetVehicleMod(playerVeh, 13, -1, 0)
			SetVehicleMod(playerVeh, 14, -1, 0)
			SetVehicleMod(playerVeh, 15, -1, 0)
			SetVehicleMod(playerVeh, 16, -1, 0)
			ToggleVehicleMod(playerVeh, 17, false)
			ToggleVehicleMod(playerVeh, 18, false)
			ToggleVehicleMod(playerVeh, 19, false)
			ToggleVehicleMod(playerVeh, 20, false)
			ToggleVehicleMod(playerVeh, 21, false)
			ToggleVehicleMod(playerVeh, 22, false)
			SetVehicleMod(playerVeh, 23, 9, true)
			SetVehicleMod(playerVeh, 24, 9, true)
			SetVehicleMod(playerVeh, 25, -1, true)
			SetVehicleMod(playerVeh, 27, -1, true)
			SetVehicleMod(playerVeh, 28, -1, true)
			SetVehicleMod(playerVeh, 30, -1, true)
			SetVehicleMod(playerVeh, 33, -1, true)
			SetVehicleMod(playerVeh, 34, -1, true)
			SetVehicleMod(playerVeh, 35, -1, true)
			SetVehicleMod(playerVeh, 38, -1, true)
			SetVehicleTyreSmokeColor(playerVeh, 0, 0, 0)
			SetVehicleWindowTint(playerVeh, -1)
			SetVehicleTyresCanBurst(playerVeh, true)
			SetVehicleNumberPlateText(playerVeh, "F-MOD")
			SetVehicleNumberPlateTextIndex(playerVeh, 5)
			SetVehicleModColor_1(playerVeh, 5, 1, 0)
			SetVehicleModColor_2(playerVeh, 5, 1)
			SetVehicleCustomPrimaryColour(playerVeh, 255, 51, 255)
			SetVehicleCustomSecondaryColour(playerVeh, 255, 51, 255)
			drawNotification("~g~Vehicle Downgraded!")
		else
			drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
		end	
	elseif action == "flipvehicle" then --Flip
		if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(GetVehiclePedIsIn(GetPlayerPed(-1), 0), -1) == GetPlayerPed(-1)) then
			SetVehicleOnGroundProperly(playerVeh)
			drawNotification("~g~Vehicle Flipped!")
		else
			drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
		end
	elseif action == "fuckup" then --Fuck Up Current Vehicle
		if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(GetVehiclePedIsIn(GetPlayerPed(-1), 0), -1) == GetPlayerPed(-1)) then
			StartVehicleAlarm(playerVeh)
			DetachVehicleWindscreen(playerVeh)
			SmashVehicleWindow(playerVeh, 0)
			SmashVehicleWindow(playerVeh, 1)
			SmashVehicleWindow(playerVeh, 2)
			SmashVehicleWindow(playerVeh, 3)
			SetVehicleTyreBurst(playerVeh, 0, true, 1000.0)
			SetVehicleTyreBurst(playerVeh, 1, true, 1000.0)
			SetVehicleTyreBurst(playerVeh, 2, true, 1000.0)
			SetVehicleTyreBurst(playerVeh, 3, true, 1000.0)
			SetVehicleTyreBurst(playerVeh, 4, true, 1000.0)
			SetVehicleTyreBurst(playerVeh, 5, true, 1000.0)
			SetVehicleTyreBurst(playerVeh, 4, true, 1000.0)
			SetVehicleTyreBurst(playerVeh, 7, true, 1000.0)
			SetVehicleDoorBroken(playerVeh, 0, true)
			SetVehicleDoorBroken(playerVeh, 1, true)
			SetVehicleDoorBroken(playerVeh, 2, true)
			SetVehicleDoorBroken(playerVeh, 3, true)
			SetVehicleDoorBroken(playerVeh, 4, true)
			SetVehicleDoorBroken(playerVeh, 5, true)
			SetVehicleDoorBroken(playerVeh, 6, true)
			SetVehicleDoorBroken(playerVeh, 7, true)
			SetVehicleLights(playerVeh, 1)
			Citizen.InvokeNative(0x1FD09E7390A74D54, playerVeh, 1)
			SetVehicleNumberPlateTextIndex(playerVeh, 5)
			SetVehicleNumberPlateText(playerVeh, "ANALSWAG")
			SetVehicleDirtLevel(playerVeh, 10.0)
			SetVehicleModColor_1(playerVeh, 1)
			SetVehicleModColor_2(playerVeh, 1)
			SetVehicleCustomPrimaryColour(playerVeh, 255, 51, 255)
			SetVehicleCustomSecondaryColour(playerVeh, 255, 51, 255)
			SetVehicleBurnout(playerVeh, true)
			drawNotification("~g~Vehicle Fucked Up!")
		else
			drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
		end
	elseif action == "delete" then --Delete
		if IsPedInAnyVehicle(GetPlayerPed(-1), 0) and (GetPedInVehicleSeat(GetVehiclePedIsIn(GetPlayerPed(-1), 0), -1) == GetPlayerPed(-1)) then
			SetEntityAsMissionEntity(Object, 1, 1)
			DeleteEntity(Object)
			SetEntityAsMissionEntity(playerVeh, 1, 1)
			DeleteVehicle(playerVeh)
			drawNotification("~g~Vehicle Deleted!")
		else
			drawNotification("~r~You Aren't In The Driverseat Of A Vehicle!")
		end
	elseif action == "vehiclegodmode" then --Godmode
		vehgodmode = newstate
		SetVehicleFixed(playerVeh)
		SetVehicleDirtLevel(playerVeh, 0.0)
		SetVehicleEngineHealth(playerVeh, 1000.0)
		if (vehgodmode == true) then
			drawNotification("~g~Vehicle Godmode Enabled!")
		else
			drawNotification("~r~Vehicle Godmode Disabled!")
		end
	elseif action == "fly" then --Flying Vehicle
		flyingvehicle = newstate
		if (flyingvehicle == true) then
			drawNotification("~g~Flying Vehicle Enabled!")
		else
			drawNotification("~r~Flying Vehicle Disabled!")
		end
	elseif action == "reducevehiclegrip" then --Reducing Grip
		reducevehgrip = newstate
		if (reducevehgrip == true) then
			drawNotification("~g~Vehicle Grip Disabled!")
		else
			drawNotification("~r~Vehicle Grip Enabled!")
		end
	elseif action == "visible" then --Visibility
		setvisible = newstate
		if (setvisible == true) then
			drawNotification("~g~Vehicle Visible!")
		else
			drawNotification("~r~Vehicle Invisible!")
		end
	elseif action == "gravity" then --Gravity
		setgravity = newstate
		if (setgravity == true) then
			drawNotification("~g~Vehicle Gravity Enabled!")
		else
			drawNotification("~r~Vehicle Gravity Disabled!")
		end
	elseif action == "hornboost" then  --Boost On Horn
		boostcar = newstate
		if (boostcar == true) then
			drawNotification("~g~Boost Car Enabled!")
		else
			drawNotification("~r~Boost Car Disabled!")
		end
	elseif action == "sb" then --Bike Seatbelt
		seatbelt = newstate
		if (seatbelt == true) then
			drawNotification("~g~Seatbelt Enabled!")
		else
			drawNotification("~r~Seatbelt Disabled!")
		end
	elseif action == "bunny" then --Bike Seatbelt
		bunnyhop = newstate
		if (bunnyhop == true) then
			drawNotification("~g~Bunny Hop Enabled!~n~ Jump With The Ped Sprint Button ~INPUT_SPRINT~")
		else
			drawNotification("~r~Bunny Hop Disabled!")
		end
	end

	cb("ok")
end)

RegisterNUICallback("fancy", function(data, cb) --Fancy Vehicle Spawning
	local playerPed = GetPlayerPed(-1)
	local x, y, z = table.unpack(GetEntityCoords(playerPed, true))
	local action = data.action

	if action == "fzufo" then
		if autodelete then
			if IsPedInAnyVehicle(playerPed, true) then
				SetEntityAsMissionEntity(Object, 1, 1)
				DeleteEntity(Object)
				SetEntityAsMissionEntity(playerVeh, 1, 1)
				DeleteVehicle(playerVeh)
				drawNotification("~g~Old Vehicle Deleted!")
			end
		end

		local coords = GetEntityCoords(playerPed, true)
		SetEntityCoords(playerPed, -2051.9, 3237.0, 1456.9)
		RequestModel(GetHashKey("dt1_tc_dufo_core"))
		while not HasModelLoaded(GetHashKey("dt1_tc_dufo_core")) do
			Citizen.Wait(0)
		end
		SetEntityCoords(playerPed, coords)
		Object = CreateObject(GetHashKey("dt1_tc_dufo_core"), -2051.9, 3237.0, 1456.9, true, true, true)
		RequestModel(GetHashKey("SKYLIFT"))
		while not HasModelLoaded(GetHashKey("SKYLIFT")) do
			Citizen.Wait(0)
		end
		local veh = CreateVehicle(GetHashKey("SKYLIFT"), x, y, z + 1, GetEntityHeading(playerPed), true, true)
		SetPedIntoVehicle(playerPed, veh, -1)
		SetEntityAsNoLongerNeeded(veh)
		AttachEntityToEntity(Object, veh, 0, 0.0, -4.0, 4.0, 0.0, 0.0, 0.0, true, true, true, false, 1, true)
		SetEntityNoCollisionEntity(veh, Object, false)
		SetEntityCollision(Object, true, true)
	elseif action == "acufo" then
		if autodelete then
			if IsPedInAnyVehicle(playerPed, true) then
				SetEntityAsMissionEntity(Object, 1, 1)
				DeleteEntity(Object)
				SetEntityAsMissionEntity(playerVeh, 1, 1)
				DeleteVehicle(playerVeh)
				drawNotification("~g~Old Vehicle Deleted!")
			end
		end

		local coords = GetEntityCoords(playerPed, true)
		SetEntityCoords(playerPed, 2490.4, 3774.8, 2414.0)
		RequestModel(GetHashKey("imp_prop_ship_01a"))
		while not HasModelLoaded(GetHashKey("imp_prop_ship_01a")) do
			Citizen.Wait(0)
		end
		SetEntityCoords(playerPed, coords)
		Object = CreateObject(GetHashKey("imp_prop_ship_01a"), 2490.4, 3774.8, 2414.0, true, true, true)
		RequestModel(GetHashKey("SKYLIFT"))
		while not HasModelLoaded(GetHashKey("SKYLIFT")) do
			Citizen.Wait(0)
		end
		local veh = CreateVehicle(GetHashKey("SKYLIFT"), x, y, z + 1, GetEntityHeading(playerPed), true, true)
		SetPedIntoVehicle(playerPed, veh, -1)
		SetEntityAsNoLongerNeeded(veh)
		AttachEntityToEntity(Object, veh, 0, 0.0, -4.0, 2.5, 0.0, 0.0, 0.0, true, true, true, false, 1, true)
		SetEntityNoCollisionEntity(veh, Object, false)
		SetEntityCollision(Object, true, true)
	elseif action == "damufo" then
		if autodelete then
			if IsPedInAnyVehicle(playerPed, true) then
				SetEntityAsMissionEntity(Object, 1, 1)
				DeleteEntity(Object)
				SetEntityAsMissionEntity(playerVeh, 1, 1)
				DeleteVehicle(playerVeh)
				drawNotification("~g~Old Vehicle Deleted!")
			end
		end

		RequestModel(GetHashKey("gr_prop_damship_01a"))
		while not HasModelLoaded(GetHashKey("gr_prop_damship_01a")) do
			Citizen.Wait(0)
		end
		Object = CreateObject(GetHashKey("gr_prop_damship_01a"), x, y, z, true, true, true)
		RequestModel(GetHashKey("SKYLIFT"))
		while not HasModelLoaded(GetHashKey("SKYLIFT")) do
			Citizen.Wait(0)
		end
		local veh = CreateVehicle(GetHashKey("SKYLIFT"), x, y, z + 1, GetEntityHeading(playerPed), true, true)
		SetPedIntoVehicle(playerPed, veh, -1)
		SetEntityAsNoLongerNeeded(veh)
		AttachEntityToEntity(Object, veh, 0, 0.0, -4.0, 2.5, 0.0, 0.0, 0.0, true, true, true, false, 1, true)
		SetEntityNoCollisionEntity(veh, Object, false)
		SetEntityCollision(Object, true, true)
	elseif action == "monsurgent" then
		RequestModel(GetHashKey("INSURGENT2"))
		while not HasModelLoaded(GetHashKey("INSURGENT2")) do
			Citizen.Wait(0)
		end
		local vehProp = CreateVehicle(GetHashKey("INSURGENT2"), x, y, z + 1, GetEntityHeading(playerPed), true, true)
		DetachVehicleWindscreen(vehProp)
		SpawnVehicle(GetHashKey("MONSTER"), x, y, z)
		local playerVeh = GetVehiclePedIsIn(playerPed, false)
		AttachEntityToEntity(vehProp, playerVeh, 0, 0.0, 0.05, 0.4, 0.0, 0.0, 0.0, true, true, true, false, 1, true)
		SetVehicleOnGroundProperly(vehProp)
		SetEntityInvincible(vehProp, true)
		SetVehicleExplodesOnHighExplosionDamage(vehProp, false)
	elseif action == "monsterbus" then
		RequestModel(GetHashKey("bus"))
		while not HasModelLoaded(GetHashKey("bus")) do
			Citizen.Wait(0)
		end
		local vehProp = CreateVehicle(GetHashKey("bus"), x, y, z + 1, GetEntityHeading(playerPed), true, true)
		DetachVehicleWindscreen(vehProp)
		SpawnVehicle(GetHashKey("MARSHALL"), x, y, z)
		local playerVeh = GetVehiclePedIsIn(playerPed, false)
		AttachEntityToEntity(vehProp, playerVeh, 0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, true, true, true, false, 1, true)
		SetVehicleOnGroundProperly(vehProp)
		SetEntityInvincible(vehProp, true)
		SetVehicleExplodesOnHighExplosionDamage(vehProp, false)
	elseif action == "dozurgent" then
		RequestModel(GetHashKey("BULLDOZER"))
		while not HasModelLoaded(GetHashKey("BULLDOZER")) do
			Citizen.Wait(0)
		end
		local vehProp = CreateVehicle(GetHashKey("BULLDOZER"), x, y, z + 1, GetEntityHeading(playerPed), true, true)
		DetachVehicleWindscreen(vehProp)
		SpawnVehicle(GetHashKey("INSURGENT2"), x, y, z)
		local playerVeh = GetVehiclePedIsIn(playerPed, false)
		AttachEntityToEntity(vehProp, playerVeh, 0, 0.0, 0.0, -0.5, 0.0, 0.0, 0.0, true, true, true, false, 1, true)
		SetVehicleOnGroundProperly(vehProp)
		SetEntityInvincible(vehProp, true)
		SetVehicleExplodesOnHighExplosionDamage(vehProp, false)
	end
	drawNotification("~g~Vehicle Spawned!")

	cb("ok")
end)

RegisterNUICallback("vehspawn", function(data, cb) --Vehicle Spawning
	local playerPed = GetPlayerPed(-1)
	local x, y, z = table.unpack(GetEntityCoords(playerPed, true))

	if data.action == "despawn" then
		despawnable = data.newstate
		if (despawnable == true) then
			drawNotification("~g~Vehicle Despawn Enabled!")
		else
			drawNotification("~r~Vehicle Despawn Disabled!")
		end
		return
	elseif data.action == "delete" then
		autodelete = data.newstate
		if (autodelete == true) then
			drawNotification("~g~Replace Vehicle When Spawning Enabled!")
		else
			drawNotification("~r~Replace Vehicle When Spawning Disabled!")
		end
		return
	elseif data.action == "mark" then
		mapblip = data.newstate
		if (mapblip == true) then
			drawNotification("~g~Mark Vehicle On Map Enabled!")
		else
			drawNotification("~r~Mark Vehicle On Map Disabled!")
		end
		return
	elseif data.action == "input" then
		DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8", "", "", "", "", "", 25)
		blockinput = true

		while UpdateOnscreenKeyboard() ~= 1 and UpdateOnscreenKeyboard() ~= 2 do
			Citizen.Wait(0)
		end

		local result = GetOnscreenKeyboardResult()
		if result then
			SpawnVehicle(GetHashKey(string.upper(result)), x, y, z)
		end

		blockinput = false
		return
	end

	local playerVeh = GetVehiclePedIsIn(playerPed, false)
	local vehhash = GetHashKey(data.action)

	SpawnVehicle(vehhash, x, y, z)

	cb("ok")
end)

RegisterNUICallback("openclosedoors", function(data, cb) --Door Menu
	local playerPed = GetPlayerPed(-1)
	local playerVeh = GetVehiclePedIsIn(playerPed, false)
	local action = data.action
	
	if action == "0" then
		if IsPedInAnyVehicle(playerPed, true) then
			if GetVehicleDoorAngleRatio(playerVeh, 0) > 0.0 then 
				SetVehicleDoorShut(playerVeh, 0, false)
				drawNotification("~r~Front Left Door Closed!")
			else
				SetVehicleDoorOpen(playerVeh, 0, false)
				frontleft = true
			end
		else
			drawNotification("~r~Not in a vehicle!")
		end
	elseif action == "1" then 
		if IsPedInAnyVehicle(playerPed, true) then
			if GetVehicleDoorAngleRatio(playerVeh, 1) > 0.0 then 
				SetVehicleDoorShut(playerVeh, 1, false)
				drawNotification("~r~Front Right Door Closed!")
			else
				SetVehicleDoorOpen(playerVeh, 1, false)
				frontright = true
			end
		else
			drawNotification("~r~Not in a vehicle!")
		end
	elseif action == "2" then 
		if IsPedInAnyVehicle(playerPed, true) then
			if GetVehicleDoorAngleRatio(playerVeh, 2) > 0.0 then 
				SetVehicleDoorShut(playerVeh, 2, false)
				drawNotification("~r~Rear Left Door Closed!")
			else
				SetVehicleDoorOpen(playerVeh, 2, false)
				rearleft = true
			end
		else
			drawNotification("~r~Not in a vehicle!")
		end
	elseif action == "3" then 
		if IsPedInAnyVehicle(playerPed, true) then
			if GetVehicleDoorAngleRatio(playerVeh, 3) > 0.0 then 
				SetVehicleDoorShut(playerVeh, 3, false)
				drawNotification("~r~Rear Right Door Closed!")
			else
				SetVehicleDoorOpen(playerVeh, 3, false)
				rearright = true
			end
		else
			drawNotification("~r~Not in a vehicle!")
		end
	elseif action == "4" then 
		if IsPedInAnyVehicle(playerPed, true) then
			if GetVehicleDoorAngleRatio(playerVeh, 4) > 0.0 then 
				SetVehicleDoorShut(playerVeh, 4, false)
				drawNotification("~r~Hood Closed!")
			else
				SetVehicleDoorOpen(playerVeh, 4, false)
				hood = true
			end
		else
			drawNotification("~r~Not in a vehicle!")
		end
	elseif action == "5" then 
		if IsPedInAnyVehicle(playerPed, true) then
			if GetVehicleDoorAngleRatio(playerVeh, 5) > 0.0 then 
				SetVehicleDoorShut(playerVeh, 5, false)
				drawNotification("~r~Trunk Closed!")
			else
				SetVehicleDoorOpen(playerVeh, 5, false)
				trunk = true
			end
		else
			drawNotification("~r~Not in a vehicle!")
		end
	end
end)

RegisterNUICallback("extra", function(data, cb) --Extra Menu
	local playerPed = GetPlayerPed(-1)
	local playerVeh = GetVehiclePedIsIn(playerPed, false)
	local action = data.action
	local extra = tonumber(action)
	local newstate = data.newstate
	
	if action == "show" then
		showextras = newstate
		drawNotification("~g~Showing Extras States!")
		drawNotification("~y~Disappears In 10 Seconds!")
	else
		if IsPedInAnyVehicle(playerPed, true) then
			if (DoesExtraExist(playerVeh, extra) == 1) then
				if (IsVehicleExtraTurnedOn(playerVeh, extra) == 1) then
					SetVehicleExtra(playerVeh, extra, true)
					drawNotification("~r~Extra " .. action .. " Disabled!")
				else
					SetVehicleExtra(playerVeh, extra, false)
					drawNotification("~g~Extra " .. action .. " Enabled!")
				end
			else
				drawNotification("~r~Extra " .. action .. " Not Existing!")
			end
		else
			drawNotification("~r~Not in a vehicle!")
		end
	end
end)

Citizen.CreateThread(function() --Door Messages
	while true do
		Citizen.Wait(0)

		if (frontleft == true) then --Front Left Door Message
			Citizen.Wait(50)
			if GetVehicleDoorAngleRatio(GetVehiclePedIsIn(GetPlayerPed(-1), false), 0) == 0.0 then
				drawNotification("~r~No Front Left Door!")
			else
				drawNotification("~g~Front Left Door Opened!")
			end
			frontleft = false
		elseif (frontright == true) then --Front Right Door Message
			Citizen.Wait(50)
			if GetVehicleDoorAngleRatio(GetVehiclePedIsIn(GetPlayerPed(-1), false), 1) == 0.0 then
				drawNotification("~r~No Front Right Door!")
			else
				drawNotification("~g~Front Right Door Opened!")
			end
			frontright = false
		elseif (rearleft == true) then --Rear Left Door Message
			Citizen.Wait(50)
			if GetVehicleDoorAngleRatio(GetVehiclePedIsIn(GetPlayerPed(-1), false), 2) == 0.0 then
				drawNotification("~r~No Rear Left Door!")
			else
				drawNotification("~g~Rear Left Door Opened!")
			end
			rearleft = false
		elseif (rearright == true) then --Rear Right Door Message
			Citizen.Wait(50)
			if GetVehicleDoorAngleRatio(GetVehiclePedIsIn(GetPlayerPed(-1), false), 3) == 0.0 then
				drawNotification("~r~No Rear Right Door!")
			else
				drawNotification("~g~Rear Right Door Opened!")
			end
			rearright = false
		elseif (hood == true) then --Hood Message
			Citizen.Wait(50)
			if GetVehicleDoorAngleRatio(GetVehiclePedIsIn(GetPlayerPed(-1), false), 4) == 0.0 then
				drawNotification("~r~No Hood!")
			else
				drawNotification("~g~Hood Opened!")
			end
			hood = false
		elseif (trunk == true) then --Trunk Message
			Citizen.Wait(50)
			if GetVehicleDoorAngleRatio(GetVehiclePedIsIn(GetPlayerPed(-1), false), 5) == 0.0 then
				drawNotification("~r~No Trunk!")
			else
				drawNotification("~g~Trunk Opened!")
			end
			trunk = false
		end
	end
end)

Citizen.CreateThread(function() --Godmode
	while true do
		Citizen.Wait(0)
		local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1))
		if (vehgodmode == false) then
			if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
				SetVehicleCanBeVisiblyDamaged(playerVeh, true)
				SetVehicleTyresCanBurst(playerVeh, true)
				SetEntityInvincible(playerVeh, false)
				SetEntityProofs(playerVeh, false, false, false, false, false, false, false, false)
				SetVehicleWheelsCanBreak(playerVeh, true)
				SetVehicleExplodesOnHighExplosionDamage(playerVeh, true)
				SetEntityOnlyDamagedByPlayer(playerVeh, true)
				SetEntityCanBeDamaged(playerVeh, true)
			end
		elseif (vehgodmode == true) then
			if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
				SetVehicleCanBeVisiblyDamaged(playerVeh, false)
				SetVehicleTyresCanBurst(playerVeh, false)
				SetEntityInvincible(playerVeh, true)
				SetEntityProofs(playerVeh, true, true, true, true, true, true, true, true)
				SetVehicleWheelsCanBreak(playerVeh, false)
				SetVehicleExplodesOnHighExplosionDamage(playerVeh, false)
				SetEntityOnlyDamagedByPlayer(playerVeh, false)
				SetEntityCanBeDamaged(playerVeh, false)
			end
		end
	end
end)

Citizen.CreateThread(function() --Flying Vehicle
	while true do
		Citizen.Wait(0)

		local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
		local Speed = GetEntitySpeed(playerVeh)
		local Rot = GetEntityRotation(playerVeh, 2)
		
		if Speed < 10.0 then
			Speed = Speed + 15.0
		end
		if (flyingvehicle == true) then
			if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
				SetCurrentPedWeapon(GetPlayerPed(-1), GetHashKey("WEAPON_UNARMED"), true)
				if IsControlPressed(1, 21) then --Instant Stop
					SetVehicleForwardSpeed(playerVeh, 0.0)
				elseif IsControlPressed(1, 71) then --Forward
					SetVehicleForwardSpeed(playerVeh, Speed * 1.01)
				elseif IsControlPressed(1, 72) then --Backward
					SetVehicleForwardSpeed(playerVeh, -70.0)
				end
				if (IsPedInAnyHeli(GetPlayerPed(-1)) == false) and (IsPedInAnyPlane(GetPlayerPed(-1)) == false) then
					if IsControlPressed(1, 89) then --Left (Everything Else than Helicopters and Planes)
						SetEntityRotation(playerVeh, Rot.x, Rot.y, Rot.z + 0.5, 2, 1)
					elseif IsControlPressed(1, 90) then --Right (Everything Else than Helicopters and Planes)
						SetEntityRotation(playerVeh, Rot.x, Rot.y, Rot.z - 0.5, 2, 1)
					elseif IsControlPressed(1, 108) then --Left Roll (Everything Else than Helicopters and Planes)
						SetEntityRotation(playerVeh, Rot.x, Rot.y - 0.75, Rot.z, 2, 1)
					elseif IsControlPressed(1, 109) then --Right Roll (Everything Else than Helicopters and Planes)
						SetEntityRotation(playerVeh, Rot.x, Rot.y + 0.75, Rot.z, 2, 1)
					end
				end
			end
		end
	end
end)

Citizen.CreateThread(function() --Reducing Grip
	while true do
		Citizen.Wait(0)

		local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
		if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
			SetVehicleReduceGrip(playerVeh, reducevehgrip)
		end
	end
end)

Citizen.CreateThread(function() --Visibility
	while true do
		Citizen.Wait(0)

		local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
		if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
			SetEntityVisible(playerVeh, setvisible, 0)
		end
	end
end)

Citizen.CreateThread(function() --Gravity
	while true do
		Citizen.Wait(0)

		local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
		if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
			SetVehicleGravity(playerVeh, setgravity)
		end
	end
end)

Citizen.CreateThread(function() --Boost On Horn
	while true do
		Citizen.Wait(0)

		local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)

		if (boostcar == true) then
			if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
				if IsControlPressed(1, 71) and IsControlPressed(1, 86) then
					SetVehicleBoostActive(playerVeh, 1, 0)
					SetVehicleForwardSpeed(playerVeh, 75.0)
					StartScreenEffect("RaceTurbo", 0, 0)
				elseif IsControlPressed(1, 72) and IsControlPressed(1, 86) then
					SetVehicleBoostActive(playerVeh, 1, 0)
					SetVehicleForwardSpeed(playerVeh, -75.0)
				end
				SetVehicleBoostActive(playerVeh, 0, 0)
			end
		end
	end
end)

Citizen.CreateThread(function() --Seatbelt
	while true do
		Citizen.Wait(0)

		local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)

		if (seatbelt == true) then
			if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
				SetPedCanBeKnockedOffVehicle(GetPlayerPed(-1), 1)
				SetPedConfigFlag(GetPlayerPed(-1), 32, false)
				if (IsPedRagdoll(GetPlayerPed(-1)) == true) then
					SetPedIntoVehicle(GetPlayerPed(-1), playerVeh, -1)
				end
			end
		end
	end
end)

Citizen.CreateThread(function() --Bunny Hop
	while true do
		Citizen.Wait(0)

		if (bunnyhop == true) then
			if IsPedInAnyVehicle(GetPlayerPed(-1), true) and IsVehicleOnAllWheels(GetVehiclePedIsIn(GetPlayerPed(-1), false)) and IsControlJustReleased(1, 21) and not IsControlPressed(1, 289) then
				ApplyForceToEntity(GetVehiclePedIsIn(GetPlayerPed(-1), false), 1, 0.0, 0.0, 12.5, 0.0, 0.0, 0.0, 1, true, true, true, true, true)
			end
		end
	end
end)

Citizen.CreateThread(function() --Vehicle Blip Rotation
	while true do
		Citizen.Wait(0)

		local vehicleblip = GetBlipFromEntity(GetVehiclePedIsIn(GetPlayerPed(-1), true))
		
		if vehicleblip then
			if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
				if (GetVehicleClass(GetVehiclePedIsIn(GetPlayerPed(-1), true)) == 8) or (GetVehicleClass(GetVehiclePedIsIn(GetPlayerPed(-1), true)) == 13) then
					SetBlipRotation(vehicleblip, math.ceil(GetEntityHeading(GetVehiclePedIsIn(GetPlayerPed(-1), true))) - 90)
				else
					SetBlipRotation(vehicleblip, math.ceil(GetEntityHeading(GetVehiclePedIsIn(GetPlayerPed(-1), true))))
				end
			end
		end
	end
end)

Citizen.CreateThread(function() --Helicopter Blip
	while true do
		Citizen.Wait(0)

		local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), true)
		
		
		if IsPedInAnyVehicle(GetPlayerPed(-1)) then
			if (GetVehicleClass(playerVeh) == 15) and (IsVehicleModel(playerVeh, GetHashKey("SKYLIFT")) == false) then --Helicopter Blips
				if IsPedInAnyVehicle(GetPlayerPed(-1)) then
					if GetBlipSprite(GetBlipFromEntity(playerVeh)) ~= 422 then
						RemoveBlip(GetBlipFromEntity(playerVeh))
						AddBlipForEntity(playerVeh)
						SetBlipSprite(GetBlipFromEntity(playerVeh), 422)
					end
				else
					if GetBlipSprite(GetBlipFromEntity(playerVeh)) ~= 64 then
						RemoveBlip(GetBlipFromEntity(playerVeh))
						AddBlipForEntity(playerVeh)
						SetBlipSprite(GetBlipFromEntity(playerVeh), 64)
					end
				end
			end
		end
	end
end)

Citizen.CreateThread(function() --Emergency Blip Color Flash
	while true do
		Citizen.Wait(0)

		local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), true)
		
		
		if IsPedInAnyVehicle(GetPlayerPed(-1)) then
			if (GetVehicleClass(playerVeh) == 18) then --Emergency Blips
				if GetBlipSprite(GetBlipFromEntity(playerVeh)) == 198 then
					if IsVehicleSirenOn(playerVeh) then
						if GetBlipColour(GetBlipFromEntity(playerVeh)) ~= 76 then
							Citizen.Wait(250)
							SetBlipColour(GetBlipFromEntity(playerVeh), 76)
						elseif GetBlipColour(GetBlipFromEntity(playerVeh)) ~= 63 then
							Citizen.Wait(250)
							SetBlipColour(GetBlipFromEntity(playerVeh), 63)
						end
					else
						SetBlipColour(GetBlipFromEntity(playerVeh), 0)
					end
				end
			end
		end
	end
end)

Citizen.CreateThread(function() --Hide Extras States
	while true do
		Citizen.Wait(0)
		
		if (showextras == true) then
			Citizen.Wait(10000)
			drawNotification("~r~Extras States Hidden Again")
			showextras = false
		end
	end
end)

Citizen.CreateThread(function() --Show Extras States
	while true do
		Citizen.Wait(0)
		
		local playerVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
		local extra = {}

		for i = 1, 14 do
			if DoesExtraExist(playerVeh, i) then
				if (IsVehicleExtraTurnedOn(playerVeh, i) == 1) then
					extra[i] = "ON"
				else
					extra[i] = "OFF"
				end
			else
				extra[i] = "N/A"
			end
		end
		
		if (showextras == true) then
			CustomDraw("~r~" .. extra[1] .. "", 0.900, 0.1550 + 0.0365)
			CustomDraw("~r~" .. extra[2] .. "", 0.900, 0.1915 + 0.0365)
			CustomDraw("~r~" .. extra[3] .. "", 0.900, 0.2280 + 0.0365)
			CustomDraw("~r~" .. extra[4] .. "", 0.900, 0.2645 + 0.0365)
			CustomDraw("~r~" .. extra[5] .. "", 0.900, 0.3010 + 0.0365)
			CustomDraw("~r~" .. extra[6] .. "", 0.900, 0.3375 + 0.0365)
			CustomDraw("~r~" .. extra[7] .. "", 0.900, 0.3740 + 0.0365)
			CustomDraw("~r~" .. extra[8] .. "", 0.900, 0.4105 + 0.0365)
			CustomDraw("~r~" .. extra[9] .. "", 0.900, 0.4470 + 0.0365)
			CustomDraw("~r~" .. extra[10] .. "", 0.900, 0.4835 + 0.0365)
			CustomDraw("~r~" .. extra[11] .. "", 0.900, 0.5200 + 0.0365)
			CustomDraw("~r~" .. extra[12] .. "", 0.900, 0.5565 + 0.0365)
			CustomDraw("~r~" .. extra[13] .. "", 0.900, 0.5930 + 0.0365)
			CustomDraw("~r~" .. extra[14] .. "", 0.900, 0.6295 + 0.0365)
		end
	end
end)

Citizen.CreateThread(function() --Get Current Vehicle Colors (Vehicle Color Menu)
	while true do
		Citizen.Wait(0)

		if IsPedInAnyVehicle(GetPlayerPed(-1), true) then
			vehiclecol = table.pack(GetVehicleColours(GetVehiclePedIsIn(GetPlayerPed(-1), false)))
			extracol = table.pack(GetVehicleExtraColours(GetVehiclePedIsIn(GetPlayerPed(-1), false)))
		end
	end
end)

