resource_manifest_version "44febabe-d386-4d18-afbe-5e627f4af937"

-- Example custom radios
supersede_radio "RADIO_01_CLASS_ROCK" { url = "http://icecast.skyrock.net/s/natio_mp3_64k?tvr_name=frequenceradio&tvr_section1=64mp3", volume = 0.1, name = "Skyrock" }
supersede_radio "RADIO_02_POP" { url = "http://cdn.nrjaudio.fm/audio1/fr/30001/mp3_128.mp3?origine=fluxradios", volume = 0.1, name = "NRJ" }	   
supersede_radio "RADIO_03_HIPHOP_NEW" { url = "http://radiofg.impek.com/fg.mp3", volume = 0.1, name = "Radio FG" }
supersede_radio "RADIO_04_PUNK" { url = "http://streaming.radio.funradio.fr:80/fun-1-44-128.mp3", volume = 0.1, name = "Fun Radio" }
supersede_radio "RADIO_05_TALK_01" { url = "http://gene-wr05.ice.infomaniak.ch/gene-wr05.mp3", volume = 0.1, name = "Generation" }
supersede_radio "RADIO_06_COUNTRY" { url = "http://chai5she.lb.vip.cdn.dvmr.fr/rmcinfo", volume = 0.1, name = "RMC Sport" }
supersede_radio "RADIO_16_SILVERLAKE" { url = "http://mp3lg4.tdf-cdn.com/9243/lag_164753.mp3", volume = 0.1, name = "Virgin Radio" }
supersede_radio "RADIO_07_DANCE_01" { url = "http://cdn.nrjaudio.fm/audio1/fr/30601/mp3_128.mp3?origine=fluxradios", volume = 0.1, name = "Nostalgie" }
supersede_radio "RADIO_08_MEXICAN" { url = "http://cdn.nrjaudio.fm/audio1/fr/30401/mp3_128.mp3?origine=fluxradios", volume = 0.1, name = "Rire et Chansons" }
supersede_radio "RADIO_09_HIPHOP_OLD" { url = "http://broadcast.infomaniak.net/radioclassique-low.mp3", volume = 0.2, name = "Radio Classique" }
supersede_radio "RADIO_12_REGGAE" { url = "http://streamingads.hotmixradio.fm/hotmixradio-metal-128.mp3", volume = 0.1, name = "Hotmix Metal" }
supersede_radio "RADIO_13_JAZZ" { url = "https://listen.radioking.com/radio/34319/stream/70508", volume = 0.1, name = "Radiomad" }




files {
	"index.html"
}

ui_page "index.html"

client_scripts {
	"data.js",
	"client.js"
}
