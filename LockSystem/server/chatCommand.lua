if(globalConf["SERVER"].enableGiveKey)then
    RegisterCommand('givekey', function(source, args, rawCommand)
        local src = source
        local identifier = GetPlayerIdentifiers(src)[1]

        if(args[1])then
            local targetId = args[1]
            local targetIdentifier = GetPlayerIdentifiers(targetId)[1]
            if(targetIdentifier)then
                if(targetIdentifier ~= identifier)then
                    if(args[2])then
                        local plate = string.lower(args[2])
                        if(owners[plate])then
                            if(owners[plate] == identifier)then
                                alreadyHas = false
                                for k, v in pairs(secondOwners) do
                                    if(k == plate)then
                                        for _, val in ipairs(v) do
                                            if(val == targetIdentifier)then
                                                alreadyHas = true
                                            end
                                        end
                                    end
                                end

                                if(not alreadyHas)then
                                    TriggerClientEvent("ls:giveKeys", targetIdentifier, plate)
                                    TriggerEvent("ls:addSecondOwner", targetIdentifier, plate)

                                    TriggerClientEvent("ls:notify", targetId, "Tu as reçu les clefs du vehicule " .. plate .. " par " .. GetPlayerName(src))
                                    TriggerClientEvent("ls:notify", src, "Tu as donner les clefs du vehicule " .. plate .. " à " .. GetPlayerName(targetId))
                                else
                                    TriggerClientEvent("ls:notify", src, "La cible à deja les clefs")
                                    TriggerClientEvent("ls:notify", targetId, GetPlayerName(src) .. " a essayer de te donner les clefs mais tu les possedais deja")
                                end
                            else
                                TriggerClientEvent("ls:notify", src, "Ce n'est pas ton vehicule")
                            end
                        else
                            TriggerClientEvent("ls:notify", src, "Le vehicule avec cette plaque n'existe pas")
                        end
                    else
                        TriggerClientEvent("ls:notify", src, "Second missing argument : /givekey <id> <plate>")
                    end
                else
                    TriggerClientEvent("ls:notify", src, "Tu ne peux pas te donner tes propres clefs")
                end
            else
                TriggerClientEvent("ls:notify", src, "Player not found")
            end
        else
            TriggerClientEvent("ls:notify", src, 'First missing argument : /givekey <id> <plate>')
        end

        CancelEvent()
    end)
end
